"use strict";
var uglify = require('gulp-uglify');
var gulp = require('gulp');
var pump = require('pump');
var rename = require('gulp-rename');
var cleanCSS = require('gulp-clean-css');
var minifyHtml = require('gulp-htmlmin');

//======================================================================================================

//======================================================================================================
(function(){

    module.exports = function(destination, version){

        var source_js = destination+'/js/finalApp'+version+'.js',
            dest_js = destination+'/js/',
            source_html = destination+'/tpl/finalTemplate.html',
            dest_html = destination+'/tpl/',
            source_css = destination+'/css/finalStyles'+version+'.css',
            dest_css = destination+'/css/';

        //-----------   Scripts --------
        gulp.task('uglify_js', function(cb){

            pump([
                    gulp.src(source_js),
                    uglify(),
                    gulp.dest(dest_js)
                ],
                cb
            );
        });

        //=========================
        //-----------   HTML --------
        gulp.task('minify_html', function() {
            var opts = {
                collapseWhitespace: true
            };

            gulp.src(source_html)
                .pipe(minifyHtml(opts))
                .pipe(gulp.dest(dest_html))
        });
        //=========================
        //-----------   CSS --------
        gulp.task('minify_css', function(){
            gulp.src(source_css)
                .pipe(cleanCSS({
                    processImport : false,
                    keepSpecialComments : 0
                }))
                .pipe(gulp.dest(dest_css));
        });


        return gulp;

    };
})();
//======================================================================================================
