"use strict";
var uglify = require('gulp-uglify');
var browserify = require('browserify');
var gulp = require('gulp');
var source = require('vinyl-source-stream');
var buffer = require('vinyl-buffer');
var gutil = require('gulp-util');
var sourcemaps = require('gulp-sourcemaps');
var rename = require('gulp-rename');
//======================================================================================================
var replace = require('gulp-replace');
var concat = require('gulp-concat');
var less = require('gulp-less');
//======================================================================================================

//======================================================================================================
(function(){

    module.exports = function(src, destination, appName, shouldMinify, version, indexTextToReplace){

        var app_source = ""+src+"/"+appName+"";
        //-----------   Scripts --------
        gulp.task('scripts', function(){


            var b = browserify({
                entries: app_source+'_js/app/app.js',
                debug: false
            });

            console.log(destination+'/js/');

            return b.bundle()
                .pipe(source(app_source+'_js/app/app.js'))
                .pipe(buffer())
                //.pipe(sourcemaps.init({loadMaps: true}))
                // Add transformation tasks to the pipeline here.
                //.pipe(uglify())
                .on('error', gutil.log)
                //.pipe(sourcemaps.write('./'))
                .pipe(rename('finalApp'+version+'.js'))
                .pipe(gulp.dest(destination+'/js/'));

        });

        //-----------   Templates --------
        gulp.task('templates', function(){

            gulp.src([''+app_source+'_html/views/first.html', ''+app_source+'_html/views/*/*.html', ''+app_source+'_html/views/*/*/*.html', ''+app_source+'_html/views/*/*/*/*.html', ''+app_source+'_html/views/*/*/*/*/*.html', ''+app_source+'_html/views/last.html'])
                .pipe(concat("finalTemplate.html"))
                .pipe(replace(indexTextToReplace, version))
                .pipe(gulp.dest(''+destination+'/tpl'));

            gulp.src(''+app_source+'_html/index.html')
                .pipe(replace(indexTextToReplace, version))
                .pipe(gulp.dest(''+destination+'/'));

        });

        //-----------   Less --------
        gulp.task('less', function(){

            return gulp.src([''+app_source+'_less/finalStyles.less'])
                .pipe(less({
                    paths: [''+app_source+'_less'] // Search paths for imports
                }))
                .pipe(concat('finalStyles'+version+'.css'))
                .pipe(gulp.dest(''+destination+'/css'));

        });

        return gulp;

    };
})();
//======================================================================================================
