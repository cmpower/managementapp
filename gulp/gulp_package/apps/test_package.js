//================================================================================================
var gulp = require('gulp'),
    replace = require('gulp-replace'),
    concat = require('gulp-concat'),
    shouldMinify = false,
    DEVELOPMENT = false,
    argv = require('yargs').argv;
//================================================================================================
var finalPackageDest = "./public/test";
//================================================================================================

//-----------   Package  --------
gulp.task("TEST_BUILD", function(){
    var src = "./dev/client";
    var newVersion = argv.version;
    var version = require("../../."+finalPackageDest+"/app_version.json");
    var currentVersion = version.AppVersion;
    var indexTextToReplace = version.IndexHTMLFileTextToReplace;

    console.log("Current Version : ", version.AppVersion);
    console.log("New Version : ", argv.version);

    if(typeof newVersion == "undefined"){
        console.log("New Version is undefined");
        return false;
    }
    //-------------------
    if(newVersion == currentVersion){
        console.error("Current Version and New Version are same. Please check you codebase.");
        return false;
    }else if(newVersion < currentVersion){
        console.error("New Version is less than Current Version. Please check you codebase.");
        return false;
    }else if(newVersion > currentVersion){
        console.log("Ok");

        if(DEVELOPMENT){
            finalPackageDest = "./public/dev/client"
        }
        require("../package_modules/bundle_app.js")(src, finalPackageDest, "client", shouldMinify, newVersion, indexTextToReplace);
        return gulp.run('scripts', 'templates', 'less');
        //return gulp.run('scripts');
    }
});
//-----------   Minification  --------
gulp.task("TEST_COMPRESS", function(){
    var newVersion = argv.version;
    var version = require("../../."+finalPackageDest+"/app_version.json");
    var currentVersion = version.AppVersion;

    console.log("Current Version : ", version.AppVersion);
    console.log("New Version : ", argv.version);

    if(typeof newVersion == "undefined"){
        console.log("New Version is undefined");
        return false;
    }
    if(newVersion == currentVersion){
        console.error("Current Version and New Version are same. Please check you codebase.");
        return false;
    }else if(newVersion < currentVersion){
        console.error("New Version is less than Current Version. Please check you codebase.");
        return false;
    }else if(newVersion > currentVersion){
        console.log("Ok");
        require("../package_modules/minify_app.js")(finalPackageDest, newVersion);
        return gulp.run('uglify_js', 'minify_css', 'minify_html');
        //return gulp.run('minify_html');
    }
});


//================================================================================================
//================================================================================================

module.exports = gulp;