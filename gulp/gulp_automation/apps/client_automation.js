//  CLIENT
var APP_NAME = "CLIENT";          // HAS TO BE IN UPPERCASE

//=======================================================================================
require("./create_views/create_view.js")(APP_NAME);
require("./create_views/create_sub_view.js")(APP_NAME);
//=======================================================================================
require("./create_routes_and_components/create_route_activity.js")(APP_NAME);
require("./create_routes_and_components/create_sub_view_access.js")(APP_NAME);
//=======================================================================================
require("./create_response_handler/create_response_handler.js")(APP_NAME);
//=======================================================================================
require("./create_storage/create_storage.js")(APP_NAME);
//=======================================================================================
require("./create_view_utilities/create_view_utility.js")(APP_NAME);
