var gulp = require('gulp');
var fileExists = require('file-exists'); // check if file exists

module.exports = function(APP_NAME){

//-----------   Create New Sub View  --------
    gulp.task(''+APP_NAME+'_CREATE_SUB_VIEW', function(){

        var VIEW_NAME = require("../../../../dev/"+APP_NAME.toLowerCase()+"/"+APP_NAME.toLowerCase()+"_automation/sub_view_config/NEW_Sub_View_Name.js");
        var automation_root = "../../../dev/"+APP_NAME.toLowerCase()+"/"+APP_NAME.toLowerCase()+"_automation";
        var project_root = "../../../dev/"+APP_NAME.toLowerCase()+"/"+APP_NAME.toLowerCase()+"";

        require("../../automation_modules/create_view_tasks")(automation_root, project_root, VIEW_NAME);

        var file_folder = project_root+"_js/views/"+VIEW_NAME.FOLDER_NAME+"/view/init.js";
        console.log("View Already Exists : " + fileExists(file_folder));

        if(!fileExists(file_folder)){
            gulp.run('create_view_js', 'create_view_html', 'create_view_update_language_obj', 'create_view_less', 'create_view_update_style_import');
        }
    });

    return gulp;
};
