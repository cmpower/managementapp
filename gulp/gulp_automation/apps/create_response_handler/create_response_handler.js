var gulp = require('gulp');
var fileExists = require('file-exists'); // check if file exists

module.exports = function(APP_NAME){

    //-----------   Create New Response Handler  --------
    gulp.task(''+APP_NAME+'_CREATE_RESPONSE_HANDLER', function(){
        var NEW_RESPONSE_HANDLER = require("../../../../dev/"+APP_NAME.toLowerCase()+"/"+APP_NAME.toLowerCase()+"_automation/response_handler_config/NEW_Response_Handler.js");
        var automation_root = "../../../dev/"+APP_NAME.toLowerCase()+"/"+APP_NAME.toLowerCase()+"_automation";
        var project_root = "../../../dev/"+APP_NAME.toLowerCase()+"/"+APP_NAME.toLowerCase()+"";
        require("../../automation_modules/create_response_handler_tasks")(automation_root, project_root, NEW_RESPONSE_HANDLER);

        var config_file = project_root+"_js/app/models/configs/"+NEW_RESPONSE_HANDLER.CONFIG_FILE_NAME;
        var command_file = project_root+"_js/app/response_handler/command/"+NEW_RESPONSE_HANDLER.COMMAND_FILE_NAME;
        var query_file = project_root+"_js/app/response_handler/query/"+NEW_RESPONSE_HANDLER.QUERY_FILE_NAME;
        console.log("Config File Exists ? : " + fileExists(config_file));
        console.log("Command File Exists ? : " + fileExists(command_file));
        console.log("Query File Exists ? : " + fileExists(query_file));

        if(!fileExists(config_file) && !fileExists(command_file) && !fileExists(query_file)){
            gulp.run('new_api_url', 'new_model_config', 'new_model_config_name', 'create_config_file');
            gulp.run('new_response_handler', 'create_command_file', 'create_query_file');
        }else{
            console.log("Cannot create new response handle. Already exists.");
        }
    });

    return gulp;
};
