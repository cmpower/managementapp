var gulp = require('gulp');
var findInFiles = require('find-in-files'); // check is a string is found

module.exports = function(APP_NAME){

    //-----------   Create Sub View Access (Routes) --------
    gulp.task(''+APP_NAME+'_CREATE_SUB_VIEW_ACCESS', function(){

        var NEW_SUB_VIEW_ACCESS = require("../../../../dev/"+APP_NAME.toLowerCase()+"/"+APP_NAME.toLowerCase()+"_automation/sub_view_access_config/NEW_Sub_View_Access.js");
        var automation_root = "../../../dev/"+APP_NAME.toLowerCase()+"/"+APP_NAME.toLowerCase()+"_automation";
        var project_root = "../../../dev/"+APP_NAME.toLowerCase()+"/"+APP_NAME.toLowerCase()+"";

        require("../../automation_modules/create_sub_view_access_tasks")(automation_root, project_root, NEW_SUB_VIEW_ACCESS);

        gulp.run('update_view_elements_coll_for_sub_view', 'update_view_module_coll', 'add_view_name_to_coll');

    });

    return gulp;
};
