var gulp = require('gulp');
var findInFiles = require('find-in-files'); // check is a string is found

module.exports = function(APP_NAME){

    //-----------   Create New Route Activity  --------
    gulp.task(''+APP_NAME+'_CREATE_ROUTE_ACTIVITY', function(){
        var NEW_ROUTE_ACTIVITY = require("../../../../dev/"+APP_NAME.toLowerCase()+"/"+APP_NAME.toLowerCase()+"_automation/route_activity_config/NEW_Route_Activity.js");
        var automation_root = "../../../dev/"+APP_NAME.toLowerCase()+"/"+APP_NAME.toLowerCase()+"_automation";
        var project_root = "../../../dev/"+APP_NAME.toLowerCase()+"/"+APP_NAME.toLowerCase()+"";
        require("../../automation_modules/create_route_activity_tasks")(automation_root, project_root, NEW_ROUTE_ACTIVITY);

        var activity_name = NEW_ROUTE_ACTIVITY.ACTIVITY_NAME;
        var activity_file_path = project_root+"_js/app/modules";
        var activity_file = "app_activity_module.js";

        findInFiles.find(activity_name, activity_file_path, activity_file)
            .then(function(results) {
                var objectKey = activity_file_path+"/"+activity_file;
                console.log(results);
                console.log(results[objectKey]);
                if(typeof results[objectKey] == "undefined"){
                    gulp.run('new_activity', 'new_route', 'new_route_controller', 'update_view_components_coll', 'update_view_initializer_coll', 'update_view_elements_coll', 'update_view_module_coll');
                }else{
                    console.log("Activity and Route already exists.");
                }
            });
    });

    return gulp;
};
