var gulp = require('gulp');
var fileExists = require('file-exists'); // check if file exists

module.exports = function(APP_NAME){

    //-----------   Create View Storage  --------
    gulp.task(''+APP_NAME+'_CREATE_VIEW_UTILITY', function(){
        var UTILITY = require("../../../../dev/"+APP_NAME.toLowerCase()+"/"+APP_NAME.toLowerCase()+"_automation/view_utility_config/utility.js");
        var automation_root = "../../../dev/"+APP_NAME.toLowerCase()+"/"+APP_NAME.toLowerCase()+"_automation/view_utility_config/utility_files";
        var project_root = "../../../dev/"+APP_NAME.toLowerCase()+"/"+APP_NAME.toLowerCase()+"";

        var src, destination, fileName;

        var utility_type = UTILITY.UTILITY_TYPE;
        switch (utility_type){
            case 'storage' :
                src = automation_root+"/view_name_storage.js";
                destination = project_root+"_js/views/"+UTILITY.VIEW_FOLDER_PATH+"/utility/";
                fileName = UTILITY.VIEW_NAME+"_storage.js";
                break;
            case 'server_call' :
                src = automation_root+"/view_name_server_calls.js";
                destination = project_root+"_js/views/"+UTILITY.VIEW_FOLDER_PATH+"/utility/";
                fileName = UTILITY.VIEW_NAME+"_server_calls.js";
                break;
            case 'data_manipulation' :
                src = automation_root+"/view_name_data_manipulation.js";
                destination = project_root+"_js/views/"+UTILITY.VIEW_FOLDER_PATH+"/utility/";
                fileName = UTILITY.VIEW_NAME+"_data_manipulation.js";
                break;
            case 'sub_view_instance' :
                src = automation_root+"/view_name_sub_view_instance.js";
                destination = project_root+"_js/views/"+UTILITY.VIEW_FOLDER_PATH+"/utility/";
                fileName = UTILITY.VIEW_NAME+"_sub_view_instance.js";
                break;
            case 'sub_view_render' :
                src = automation_root+"/view_name_sub_view_render.js";
                destination = project_root+"_js/views/"+UTILITY.VIEW_FOLDER_PATH+"/utility/";
                fileName = UTILITY.VIEW_NAME+"_sub_view_render.js";
                break;
            case 'default' :
                console.log("Utility Type is undefined");
                break;
        }


        require("../../automation_modules/create_utility_file_tasks")(src, destination, fileName, UTILITY);

        var file_folder = destination;
        console.log("View Already Exists : " + fileExists(file_folder));

        if(!fileExists(file_folder)){
            gulp.run('create_utility_file');
        }
    });

    return gulp;
};
