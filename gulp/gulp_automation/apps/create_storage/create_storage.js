var gulp = require('gulp');
var findInFiles = require('find-in-files'); // check is a string is found

module.exports = function(APP_NAME){

    //-----------   Create New Storage  --------
    gulp.task(''+APP_NAME+'_CREATE_STORAGE', function(){
        var NEW_STORAGE = require("../../../../dev/"+APP_NAME.toLowerCase()+"/"+APP_NAME.toLowerCase()+"_automation/storage_creator_config/NEW_Storage.js");
        var automation_root = "../../../dev/"+APP_NAME.toLowerCase()+"/"+APP_NAME.toLowerCase()+"_automation";
        var project_root = "../../../dev/"+APP_NAME.toLowerCase()+"/"+APP_NAME.toLowerCase()+"";
        require("../../automation_modules/create_storage_tasks")(automation_root, project_root, NEW_STORAGE);

        var storage_name = NEW_STORAGE.STORAGE_NAME;
        var storage_file_path = project_root+"_js/app/storage";
        var storage_file = "storage.js";

        findInFiles.find(storage_name, storage_file_path, storage_file)
            .then(function(results) {
                var objectKey = storage_file_path+"/"+storage_file;
                console.log(results);
                console.log(results[objectKey]);
                if(typeof results[objectKey] == "undefined"){
                    gulp.run('new_storage');
                }else{
                    console.log("Storage already exists.");
                }
            });
    });

    return gulp;
};
