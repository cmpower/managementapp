var gulp = require('gulp');
var inject = require('gulp-inject-string'); // insert view language object

//======================================================================================================
(function(){

    module.exports = function(automation_root, project_root, NEW_ROUTE_ACTIVITY){

//-----------   Create New Route Activity  --------
        gulp.task('new_activity', function(){
            var prependDestStr = '/*Prepend_Activity_Object*/';
            var str = ",\n\t\t'"+NEW_ROUTE_ACTIVITY.ACTIVITY_NAME+"': function(){\n\t\t\t this.appRouter.navigate('#"+NEW_ROUTE_ACTIVITY.ROUTE_NAME+"', RouteConstants.getRouteConstant()); \n\t\t}";
            gulp.src(project_root+'_js/app/modules/app_activity_module.js')
                .pipe(inject.before(prependDestStr, str))
                .pipe(gulp.dest(project_root+'_js/app/modules'));
        });
        gulp.task('new_route', function(){
            var prependDestStr = '/*Prepend_Route_Object*/';
            var str = ",\n\t\t'"+NEW_ROUTE_ACTIVITY.ROUTE_NAME+"' \t\t\t\t\t\t: '"+NEW_ROUTE_ACTIVITY.ROUTE_CONTROLLER_NAME+"'";
            gulp.src(project_root+'_js/app/router/routes.js')
                .pipe(inject.before(prependDestStr, str))
                .pipe(gulp.dest(project_root+'_js/app/router'));
        });
        gulp.task('new_route_controller', function(){
            var prependDestStr, str;
            if(NEW_ROUTE_ACTIVITY.ROUTE_STATE){
                prependDestStr = '/*Prepend_In_State_Route_Controller_Object*/';
                str = ",\n\t\t\t\t'"+NEW_ROUTE_ACTIVITY.ROUTE_CONTROLLER_NAME+"': function(){\n\t\t\t\t\t var AppInState = App_State.request('app:state:get'); \n\t\t\t\t\t (AppInState)? ViewComponents."+NEW_ROUTE_ACTIVITY.COMPONENT_NAME+"() : AppStateChange.refreshApp(); \n\t\t\t\t}";
            }else{
                prependDestStr = '/*Prepend_Out_State_Route_Controller_Object*/';
                str = ",\n\t\t\t\t'"+NEW_ROUTE_ACTIVITY.ROUTE_CONTROLLER_NAME+"': function(){\n\t\t\t\t\t ViewComponents."+NEW_ROUTE_ACTIVITY.COMPONENT_NAME+"(); \n\t\t\t\t}";
            }

            gulp.src(project_root+'_js/app/router/route_controller.js')
                .pipe(inject.before(prependDestStr, str))
                .pipe(gulp.dest(project_root+'_js/app/router'));
        });
        gulp.task('update_view_components_coll', function(){
            var prependDestStr = '/*Prepend_View_Component_Object*/';
            var str = ",\n\t\t'"+NEW_ROUTE_ACTIVITY.COMPONENT_NAME+"': function(){\n\t\t\t ViewLayout.renderViewOnLayout( \n\t\t\t\t ViewInitializer."+NEW_ROUTE_ACTIVITY.INITIALIZER_NAME+"() \n\t\t\t );  \n\t\t}";
            gulp.src(project_root+'_js/app/components/render_view_components.js')
                .pipe(inject.before(prependDestStr, str))
                .pipe(gulp.dest(project_root+'_js/app/components'));
        });
        gulp.task('update_view_initializer_coll', function(){
            var prependDestStr = '/*Prepend_View_Initializer_Object*/';
            var str = ",\n\t\t'"+NEW_ROUTE_ACTIVITY.INITIALIZER_NAME+"': function(){\n\t\t\t this.CURRENT_CONTENT_VIEW = ViewElements."+NEW_ROUTE_ACTIVITY.ELEMENT_NAME+"();\n\t\t\t return this.CURRENT_CONTENT_VIEW;\n\t\t}";
            gulp.src(project_root+'_js/app/components/view_initializer.js')
                .pipe(inject.before(prependDestStr, str))
                .pipe(gulp.dest(project_root+'_js/app/components'));
        });
        gulp.task('update_view_elements_coll', function(){
            var prependDestStr = '/*Prepend_View_Elements_Object*/';
            var str = ",\n\t\t'"+NEW_ROUTE_ACTIVITY.ELEMENT_NAME+"': function(){\n\t\t\t var view = viewBuilder.buildView(viewModule."+NEW_ROUTE_ACTIVITY.MODULE_NAME+");\n\t\t\t return (view);\n\t\t}";
            gulp.src(project_root+'_js/app/components/view/view_elements.js')
                .pipe(inject.before(prependDestStr, str))
                .pipe(gulp.dest(project_root+'_js/app/components/view'));
        });
        gulp.task('update_view_module_coll', function(){
            var prependDestStr = '/*Prepend_View_Module_Object*/';
            var str = ",\n\t\t\t'"+NEW_ROUTE_ACTIVITY.MODULE_NAME+"': {\n\t\t\t\t 'view_elems' : require( '../../../views/"+NEW_ROUTE_ACTIVITY.FOLDER_NAME+"/view/view_elements'),\n\t\t\t\t 'view_funcs' : require( '../../../views/"+NEW_ROUTE_ACTIVITY.FOLDER_NAME+"/view/view_functions')\n\t\t\t}";
            gulp.src(project_root+'_js/app/components/view/view_modules.js')
                .pipe(inject.before(prependDestStr, str))
                .pipe(gulp.dest(project_root+'_js/app/components/view'));
        });


        return gulp;

    };
})();
//======================================================================================================
