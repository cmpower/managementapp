var gulp = require('gulp');
var inject = require('gulp-inject-string'); // insert view language object

//======================================================================================================
(function(){

    module.exports = function(automation_root, project_root, NEW_ROUTE_ACTIVITY){

        //-----------------
        gulp.task('update_view_elements_coll_for_sub_view', function(){
            var prependDestStr = '/*Prepend_View_Elements_Object*/';
            var str = ",\n\t\t'"+NEW_ROUTE_ACTIVITY.ELEMENT_NAME+"': function(data){\n\t\t\t var model = viewModel(data); \n\t\t\t var view = viewBuilder.buildView(viewModule."+NEW_ROUTE_ACTIVITY.MODULE_NAME+", model);\n\t\t\t return (view);\n\t\t}";
            gulp.src(project_root+'_js/app/components/view/view_elements.js')
                .pipe(inject.before(prependDestStr, str))
                .pipe(gulp.dest(project_root+'_js/app/components/view'));
        });

        //-----------------
        gulp.task('update_view_module_coll', function(){
            var prependDestStr = '/*Prepend_View_Module_Object*/';
            var str = ",\n\t\t\t'"+NEW_ROUTE_ACTIVITY.MODULE_NAME+"': {\n\t\t\t\t 'view_elems' : require( '../../../views/"+NEW_ROUTE_ACTIVITY.FOLDER_NAME+"/view/view_elements'),\n\t\t\t\t 'view_funcs' : require( '../../../views/"+NEW_ROUTE_ACTIVITY.FOLDER_NAME+"/view/view_functions')\n\t\t\t}";
            gulp.src(project_root+'_js/app/components/view/view_modules.js')
                .pipe(inject.before(prependDestStr, str))
                .pipe(gulp.dest(project_root+'_js/app/components/view'));
        });

        //-----------------
        gulp.task('add_view_name_to_coll', function(){
            var prependDestStr = '/*Prepend_Sub_View_Name*/';
            var str = ",\n\t\t\t'"+NEW_ROUTE_ACTIVITY.FILE_NAME+"' : '"+NEW_ROUTE_ACTIVITY.ELEMENT_NAME+"'";
            gulp.src(project_root+'_js/app/components/sub_view_names.js')
                .pipe(inject.before(prependDestStr, str))
                .pipe(gulp.dest(project_root+'_js/app/components'));
        });

        return gulp;

    };
})();
//======================================================================================================
