var gulp = require('gulp');
var replace = require('gulp-replace'); // replace text
var concat = require('gulp-concat');

//======================================================================================================
(function(){

    module.exports = function(src, destination, fileName, UTILITY){

        gulp.task('create_utility_file', function(){

            console.log(src);
            console.log(destination);

            gulp.src(src)
                .pipe(replace(UTILITY.view_name_to_replace_big, (UTILITY.VIEW_NAME).toUpperCase()))
                .pipe(replace(UTILITY.view_name_to_replace_small, UTILITY.VIEW_NAME))
                .pipe(concat(fileName))
                .pipe(gulp.dest(destination));

        });

        return gulp;

    };
})();
//======================================================================================================
