var gulp = require('gulp');
var concat = require('gulp-concat');
var replace = require('gulp-replace'); // replace text
var inject = require('gulp-inject-string'); // insert view language object

var src = "";
var destination = "";

//======================================================================================================
(function(){

    module.exports = function(auto_root, proj_root, VIEW_Obj){
        var automation_root = auto_root;
        var project_root = proj_root;
        var VIEW_NAME = VIEW_Obj;


        gulp.task('create_view_js', function(){

            src = [ automation_root+"/view_config/js/*.js", automation_root+"/view_config/js/*/*.js", automation_root+"/view_config/js/*/*/*.js" ];
            destination = project_root+"_js/views/"+VIEW_NAME.FOLDER_NAME+"/view";

            console.log(src);
            console.log(destination);
            console.log(VIEW_NAME);

            gulp.src(src)
                .pipe(replace(VIEW_NAME.language_str_to_replace, VIEW_NAME.VIEW_LANGUAGE_ID))
                .pipe(replace(VIEW_NAME.template_str_to_replace, VIEW_NAME.VIEW_TEMPLATE_ID))
                .pipe(gulp.dest(destination));

        });


        gulp.task('create_view_html', function(){
            src = automation_root+"/view_config/html/view_content.html";
            destination = project_root+"_html/views/"+VIEW_NAME.FOLDER_NAME+"/";
            gulp.src(src)
                .pipe(replace(VIEW_NAME.template_str_to_replace, VIEW_NAME.VIEW_TEMPLATE_ID))
                .pipe(concat(VIEW_NAME.FILE_NAME+".html"))
                .pipe(gulp.dest(destination));
        });
        gulp.task('create_view_less', function(){
            src = automation_root+"/view_config/less/view_styling.less";
            destination = project_root+"_less/views/"+VIEW_NAME.FOLDER_NAME+"/";
            gulp.src(src)
                .pipe(replace(VIEW_NAME.template_str_to_replace, VIEW_NAME.VIEW_TEMPLATE_ID))
                .pipe(concat(VIEW_NAME.FILE_NAME+".less"))
                .pipe(gulp.dest(destination));
        });
        gulp.task('create_view_update_style_import', function(){
            var prependDestStr = '/*PrependViewStylesImport*/';
            var str = "@import 'views/"+VIEW_NAME.FOLDER_NAME+"/"+VIEW_NAME.FILE_NAME+"';\n";
            src = project_root+"_less/finalStyles.less";
            destination = project_root+"_less";
            gulp.src(src)
                .pipe(inject.before(prependDestStr, str))
                .pipe(gulp.dest(destination));
        });
        gulp.task('create_view_update_language_obj', function(){
            var prependDestStr = '/*PrependViewLanguageObj*/';
            var str = ",\n\t\t\t'"+VIEW_NAME.VIEW_LANGUAGE_ID+"': {\n\t\t\t\t '' : '' \n\t\t\t}";
            src = project_root+"_js/app/utils/language/multi_lingual_module.js";
            destination = project_root+"_js/app/utils/language";
            gulp.src(src)
                .pipe(inject.before(prependDestStr, str))
                .pipe(gulp.dest(destination));
        });

        return gulp;

    };
})();
//======================================================================================================
