var gulp = require('gulp');
var inject = require('gulp-inject-string'); // insert view language object
var concat = require('gulp-concat');
var replace = require('gulp-replace'); // replace text
//======================================================================================================
(function(){

    module.exports = function(automation_root, project_root, NEW_RESPONSE_HANDLER){

        gulp.task('new_api_url', function(){
            var appendDestStr = NEW_RESPONSE_HANDLER.URL_APPEND_STR;
            var str = ",\n\t\t\t'"+NEW_RESPONSE_HANDLER.URL_NAME+"'\t\t\t\t\t\t : "+NEW_RESPONSE_HANDLER.URL;

            gulp.src(project_root+'_js/app/api/api_url.js')
                .pipe(inject.before(appendDestStr, str))
                .pipe(gulp.dest(project_root+'_js/app/api'));
        });

        gulp.task('new_model_config', function(){
            var appendDestStr = NEW_RESPONSE_HANDLER.MODEL_CONFIG_APPEND_STR;
            var str = ",\n\n\t\t'"+NEW_RESPONSE_HANDLER.MODEL_CONFIG_NAME+"' : "+NEW_RESPONSE_HANDLER.MODEL_CONFIG_PATH;

            gulp.src(project_root+'_js/app/models/model_config.js')
                .pipe(inject.before(appendDestStr, str))
                .pipe(gulp.dest(project_root+'_js/app/models'));
        });
        gulp.task('new_model_config_name', function(){
            var appendDestStr = NEW_RESPONSE_HANDLER.MODEL_CONFIG_REF_APPEND_STR;
            var str = ",\n\n\t\t'"+NEW_RESPONSE_HANDLER.MODEL_CONFIG_REFERENCE_NAME+"' : '"+NEW_RESPONSE_HANDLER.MODEL_CONFIG_NAME+"'";

            gulp.src(project_root+'_js/app/models/model_config_names.js')
                .pipe(inject.before(appendDestStr, str))
                .pipe(gulp.dest(project_root+'_js/app/models'));
        });
        gulp.task('create_config_file', function(){
            var src = automation_root+"/response_handler_config/model_config/config.js";
            var destination = project_root+"_js/app/models/configs/";
            gulp.src(src)
                .pipe(replace(NEW_RESPONSE_HANDLER.url_name_replace_str, NEW_RESPONSE_HANDLER.URL_NAME))
                .pipe(replace(NEW_RESPONSE_HANDLER.attributes_replace_str, NEW_RESPONSE_HANDLER.ATTRIBUTES))
                .pipe(replace(NEW_RESPONSE_HANDLER.method_replace_str, NEW_RESPONSE_HANDLER.METHOD))
                .pipe(replace(NEW_RESPONSE_HANDLER.response_handler_name_replace_str , NEW_RESPONSE_HANDLER.RESPONSE_HANDLER_NAME))
                .pipe(replace(NEW_RESPONSE_HANDLER.success_queries_replace_str, NEW_RESPONSE_HANDLER.SUCCESS_QUERIES))
                .pipe(replace(NEW_RESPONSE_HANDLER.error_queries_replace_str, NEW_RESPONSE_HANDLER.ERROR_QUERIES))
                .pipe(replace(NEW_RESPONSE_HANDLER.success_commands_replace_str, NEW_RESPONSE_HANDLER.SUCCESS_COMMANDS))
                .pipe(replace(NEW_RESPONSE_HANDLER.error_commands_replace_str, NEW_RESPONSE_HANDLER.ERROR_COMMANDS))
                .pipe(concat(NEW_RESPONSE_HANDLER.CONFIG_FILE_NAME))
                .pipe(gulp.dest(destination));
        });

        gulp.task('new_response_handler', function(){
            var appendDestStr = NEW_RESPONSE_HANDLER.RESPONSE_HANDLER_APPEND_STR;
            var str = ",\n\t\t'"+NEW_RESPONSE_HANDLER.RESPONSE_HANDLER_NAME+"' : {\n\t\t\t 'command' : require( './command/"+NEW_RESPONSE_HANDLER.COMMAND_FILE_NAME+"'),\n\t\t\t 'query'   : require( './query/"+NEW_RESPONSE_HANDLER.QUERY_FILE_NAME+"')\n\t\t}";

            gulp.src(project_root+'_js/app/response_handler/response_handler.js')
                .pipe(inject.before(appendDestStr, str))
                .pipe(gulp.dest(project_root+'_js/app/response_handler'));
        });
        gulp.task('create_command_file', function(){
            var src = automation_root+"/response_handler_config/model_config/command.js";
            var destination = project_root+"_js/app/response_handler/command/";
            gulp.src(src)
                .pipe(concat(NEW_RESPONSE_HANDLER.COMMAND_FILE_NAME))
                .pipe(gulp.dest(destination));
        });
        gulp.task('create_query_file', function(){
            var src = automation_root+"/response_handler_config/model_config/query.js";
            var destination = project_root+"_js/app/response_handler/query/";
            gulp.src(src)
                .pipe(concat(NEW_RESPONSE_HANDLER.QUERY_FILE_NAME))
                .pipe(gulp.dest(destination));
        });

        return gulp;

    };
})();
//======================================================================================================
