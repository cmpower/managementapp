var gulp = require('gulp');
var inject = require('gulp-inject-string'); // insert view language object

//======================================================================================================
(function(){

    module.exports = function(automation_root, project_root, NEW_STORAGE){

//-----------   Create New Route Activity  --------

        gulp.task('new_storage', function(){

            var prependObjDestStr = '/*Prepend_Storage_Object*/';
            var prependNameDestStr = '/*Prepend_Storage_Name*/';

            var objstr = ",\n\t\t\t'"+NEW_STORAGE.STORAGE_NAME+"' : {}";
            var nameStr = ",\n\t\t\t'"+NEW_STORAGE.STORAGE_ACCESS_NAME+"' : '"+NEW_STORAGE.STORAGE_NAME+"'";

            gulp.src(project_root+'_js/app/storage/storage.js')
                .pipe(inject.before(prependObjDestStr, objstr))
                .pipe(gulp.dest(project_root+'_js/app/storage'));

            gulp.src(project_root+'_js/app/storage/store_names.js')
                .pipe(inject.before(prependNameDestStr, nameStr))
                .pipe(gulp.dest(project_root+'_js/app/storage'));

        });

        return gulp;

    };
})();
//======================================================================================================
