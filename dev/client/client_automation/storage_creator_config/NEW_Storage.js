(function(){
    "use strict";

    module.exports = function(){

        return ({
            'STORAGE_NAME'          : 'user_access_list',          // name_name
            'STORAGE_ACCESS_NAME'   : 'userAccessList'            // nameName
        });

    }()

})();
