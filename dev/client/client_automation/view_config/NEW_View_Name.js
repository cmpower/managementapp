(function(){
    "use strict";

    module.exports = function(){

        return ({
            'FOLDER_NAME'       : 'user_removal',        // ( parent_folder_name/ )name_name
            'FILE_NAME'         : 'user_removal',                      // name_name
            'VIEW_LANGUAGE_ID'  : 'UserRemovalView',         // NameNameView
            'VIEW_TEMPLATE_ID'  : 'UserRemovalView',         // NameNameView

            'template_str_to_replace'  : 'VIEW_TEMPLATE_ID', //do not change this
            'language_str_to_replace'  : 'VIEW_LANGUAGE_ID'  //do not change this
        });

    }()

})();
