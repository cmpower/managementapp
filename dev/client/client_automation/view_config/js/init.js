(function(){
    "use strict";
    // VIEW_TEMPLATE_ID

    var _ = require('underscore');
    var viewContentLang = require("../../../app/utils/language/multi_lingual_module.js");
    var languageId = "VIEW_LANGUAGE_ID", templateId = "VIEW_TEMPLATE_ID";

    var initView = function(){
        this.template = _.template(viewContentLang.getCurrentLangViewTemplate(languageId, templateId));
    };

    module.exports = function(){
        return (initView);
    }();

})();