(function(){
    "use strict";

    module.exports = function(){

        return ({
            'UTILITY_TYPE'              : 'sub_view_render',
            // 1. storage,
            // 2. server_call
            // 3. data_manipulation
            // 5. sub_view_render

            'VIEW_NAME'                 : 'tree',                      // name_name
            'VIEW_FOLDER_PATH'          : 'tree',         // ( parent_folder_name/ )name_name

            'view_name_to_replace_big'          : 'VIEW_NAME',         //do not change this
            'view_name_to_replace_small'        : 'view_name'          //do not change this
        });

    }()

})();
