// VIEW_NAME
(function(){
    "use strict";

    var ModelInit = require('../../../app/models/model_initiator');
    var ModelConfigNames = require('../../../app/models/model_config_names');

    var SERVER_CALLS = {
    //  *-------------------- Get  --------------------------*
        "challengeComplete" : function(options, thisObj){
            var model = ModelInit(ModelConfigNames.submitChallengeModel);
            var modelData = options.challengeData;
            model.setModelData(modelData);
            model.sendServerCall();
        }
    //  *-------------------- Post --------------------------*

    //  *------------------- Update -------------------------*

    //  *------------------- Delete -------------------------*

    };


    //************************
    module.exports = function(){
        return SERVER_CALLS;
    }();
    //************************

})();