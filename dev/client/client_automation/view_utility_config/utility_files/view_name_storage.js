// VIEW_NAME
(function(){
    "use strict";

    var _ = require('underscore');

    var view_storage = {
        'example' : ""
    };

    var view_storage_manager = function () {
        this.storage = view_storage;
    };

    view_storage_manager.prototype = {

        getStoreData : function(objName){
            var obj = this.storage[objName];
            return (typeof obj == "undefined"? {} : _.clone(obj));
        },
        setStoreData : function(objName, objData){
            if(typeof this.storage[objName] != "undefined"){
                this.storage[objName] = objData;
                return true;
            }else{
                console.error("Storage doesn't exists");
                return false;
            }
        },
        clearStoreData : function(storeNames){
            var storageData = view_storage;
            for(var i = 0; i < storeNames.length; i++){
                this.storage[storeNames[i]] = storageData[storeNames[i]];
            }
        },
        clearStorage : function(){
            this.storage = view_storage;
        }
    };

    module.exports = function () {
        return (new view_storage_manager());
    }();

})();
