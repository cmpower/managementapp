(function(){
    "use strict";

    module.exports = function(){

        return ({
            'FOLDER_NAME'                : 'user_removal',        // ( parent_folder_name/ )name_name

            'ACTIVITY_NAME'              : 'userRemovalActivity',   // nameNameActivity
            'ROUTE_NAME'                 : 'user-removal',           // name-name
            'ROUTE_CONTROLLER_NAME'      : 'userRemovalRoute',    // nameNameRoute
            'ROUTE_STATE'                : true,
            'COMPONENT_NAME'             : 'userRemoval',         // nameName
            'INITIALIZER_NAME'           : 'userRemovalView',     // nameNameView
            'ELEMENT_NAME'               : 'getUserRemovalView',  // getNameNameView
            'MODULE_NAME'                : 'UserRemovalView'      // NameNameView
        });

    }()

})();
