(function(){
    "use strict";

    module.exports = function(){

        return ({
            'FOLDER_NAME'       : 'user_permission/user_permission_invite',        // ( parent_folder_name/ )name_name
            'FILE_NAME'         : 'user_permission_invite',                      // name_name
            'VIEW_LANGUAGE_ID'  : 'UserPermissionInviteView',         // NameNameView
            'VIEW_TEMPLATE_ID'  : 'UserPermissionInviteView',         // NameNameView

            'template_str_to_replace'  : 'VIEW_TEMPLATE_ID', //do not change this
            'language_str_to_replace'  : 'VIEW_LANGUAGE_ID'  //do not change this
        });

    }()

})();
