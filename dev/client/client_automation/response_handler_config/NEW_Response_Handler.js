(function(){
    "use strict";

    module.exports = function(){

        return ({
            'URL_NAME'                      : 'user_appliance_permission_invite', //name_name
            'URL'                           : "'/client/user/appliance/permission/invite'", // ( '/url' )
            //----------------------------
            'MODEL_CONFIG_NAME'             : 'user_appliance_permission_invite_model_config', // name_name_model_config
            'MODEL_CONFIG_PATH'             : 'require("./configs/user_appliance_permission_invite_config")', // require("./configs/name_name_config")
            //----------------------------
            'MODEL_CONFIG_REFERENCE_NAME'   : 'userAppliancePermissionInviteModel', // nameNameModel
            //----------------------------
            'CONFIG_FILE_NAME'  : "user_appliance_permission_invite_config.js",   // name_name_config
            'ATTRIBUTES'        : '{"userId": "string","deviceId": "string","subOwnerEmailId": "string"}',
            'METHOD'            : "POST",
            'SUCCESS_QUERIES'   :   "[]",     // ['store_data','clear_data', 'app_state_in', 'app_state_out']
            'ERROR_QUERIES'     :   "[]",     // ['store_data','clear_data', 'app_state_in', 'app_state_out']
            'SUCCESS_COMMANDS'  :   "['call_view_function', 'notification']",     // ['navigate', 'call_view_function', 'notification']
            'ERROR_COMMANDS'    :   "['notification']",     // ['navigate', 'call_view_function', 'notification']
            //----------------------------
            //----------------------------
            'RESPONSE_HANDLER_NAME'     : "user_appliance_permission_invite_response",             // name_name_response
            'COMMAND_FILE_NAME'         : "user_appliance_permission_invite_command.js",           // name_name_command.js
            'QUERY_FILE_NAME'           : "user_appliance_permission_invite_query.js",             // name_name_query.js



            //----------------------------
            //------------ DO NOT CHANGE  -----------------------
            //----------------------------
            'url_name_replace_str' : 'URL_NAME',
            'attributes_replace_str' : 'ATTRIBUTES',
            'method_replace_str' : 'METHOD',
            'response_handler_name_replace_str' : 'RESPONSE_HANDLER_NAME',
            'success_queries_replace_str' : 'SUCCESS_QUERIES',
            'error_queries_replace_str' : 'ERROR_QUERIES',
            'success_commands_replace_str' : 'SUCCESS_COMMANDS',
            'error_commands_replace_str' : 'ERROR_COMMANDS',
            //----------------------------
            'URL_APPEND_STR'           : "/*AppendApiUrl*/", // Append String Location (do not change this)
            'MODEL_CONFIG_APPEND_STR'  : "/*AppendModelConfig*/", // Append String Location (do not change this)
            'MODEL_CONFIG_REF_APPEND_STR'  : "/*AppendModelConfigName*/", // Append String Location (do not change this)
            'RESPONSE_HANDLER_APPEND_STR'  : "/*AppendResponseHandler*/" // Append String Location (do not change this)
            //----------------------------
            //----------------------------


        });

    }()

})();
