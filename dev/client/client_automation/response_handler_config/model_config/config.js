(function() {
    "use strict";

    var AppApiUrl = require('../../api/api_url');

    module.exports = {

        'getModelProperties':   function(){
            return ({
                'url':   AppApiUrl.getApiUrl("URL_NAME"),
                'defaults': ATTRIBUTES
            });
        },
        'method'            :   'METHOD',                 // POST / GET

        'responseHandler'   :   'RESPONSE_HANDLER_NAME',
        'success_queries'   :   SUCCESS_QUERIES,                     // ['store_data','clear_data', 'app_state_in', 'app_state_out']
        'error_queries'     :   ERROR_QUERIES,                     // ['store_data','clear_data', 'app_state_in', 'app_state_out']
        'success_commands'  :   SUCCESS_COMMANDS,                     // ['navigate', 'call_view_function', 'notification']
        'error_commands'    :   ERROR_COMMANDS                      // ['navigate', 'call_view_function', 'notification']

    };

})();

