(function(){
    "use strict";

    module.exports = function(){

        return ({
            'FOLDER_NAME'                : 'user_permission/user_permission_invite',        // ( parent_folder_name/ )name_name
            'FILE_NAME'                  : 'user_permission_invite',                      // name_name

            'ELEMENT_NAME'               : 'getUserPermissionInviteView',  // getNameNameView
            'MODULE_NAME'                : 'UserPermissionInviteView'      // NameNameView
        });

    }()

})();
