(function(){
    "use strict";

    var Marionette = require('backbone.marionette');
    //--------------------------------------------------------------------------

    var AppLayout = function(){

        var Layout = Marionette.View.extend({
            el: '#appContent',
            template: "#AppTemplate",
            regions: {
                header      : "#HeaderRegion",
                sidebar     : "#SidebarRegion",
                content     : "#ContentRegion",
                footer      : "#FooterRegion"
            }
        });

        this.layout = new Layout();
    };

    AppLayout.prototype = {

        getAppLayoutView : function(){
            return this.layout;
        },
        getAppLayoutHeaderRegion : function(){
            console.log(this.layout.getRegion("header"));
            return(this.layout.getRegion("header"));
        },
        getAppLayoutSidebarRegion : function(){
            return(this.layout.getRegion("sidebar"));
        },
        getAppLayoutContentRegion : function(){
            return(this.layout.getRegion("content"));
        },
        getAppLayoutFooterRegion : function(){
            return(this.layout.getRegion("footer"));
        }
    };

    module.exports = function(){
        return (new AppLayout());
    }();

})();
