"use strict";

(function () {
    "use strict";

    var AppLayout = require('./app_layout.js');
    //--------------------------------------------------------------------------

    var ViewLayout = function ViewLayout() {
        this.AppView = AppLayout.getAppLayoutView();
        this.HeaderRegion = AppLayout.getAppLayoutHeaderRegion();
        this.FooterRegion = AppLayout.getAppLayoutFooterRegion();
        this.SidebarRegion = AppLayout.getAppLayoutSidebarRegion();
        this.ContentRegion = AppLayout.getAppLayoutContentRegion();
    };

    ViewLayout.prototype = {

        initRegionWithLayout: function initRegionWithLayout() {
            this.AppView.render();
        },
        resetViewLayout: function resetViewLayout() {
            this.HeaderRegion.reset();
            this.FooterRegion.reset();
            this.SidebarRegion.reset();
            this.ContentRegion.reset();
        },
        renderAllViewsOnLayout: function renderAllViewsOnLayout() {
            this.resetViewLayout();
            console.log(arguments);
            this.HeaderRegion.show(arguments[0]);
            this.FooterRegion.show(arguments[1]);
            this.SidebarRegion.show(arguments[2]);
            this.ContentRegion.show(arguments[3]);
        },
        renderHeaderOnLayout: function renderHeaderOnLayout() {
            this.HeaderRegion.reset();
            this.HeaderRegion.show(arguments[0]);
        },
        renderFooterOnLayout: function renderFooterOnLayout() {
            this.FooterRegion.reset();
            this.FooterRegion.show(arguments[0]);
        },
        renderSidebarOnLayout: function renderSidebarOnLayout() {
            this.SidebarRegion.reset();
            this.SidebarRegion.show(arguments[0]);
        },
        renderViewOnLayout: function renderViewOnLayout() {
            this.ContentRegion.reset();
            this.ContentRegion.show(arguments[0]);
        }
    };

    module.exports = (function () {
        return new ViewLayout();
    })();
})();