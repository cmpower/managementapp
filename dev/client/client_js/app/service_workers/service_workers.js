(function(){
    "use strict";

    var storeNames = require('../storage/store_names');
    var StoreManager = require('../storage/store_manager');
    var Local_Storage = require('../local_storage/local_storage');

    var Push_Notification = function(){
        this.subscribeObj = {};
        this.registrationObj = {};
    };

    Push_Notification.prototype = {

        init: function(){
            if(('serviceWorker' in navigator) && WebApp){
                var that = this;
                console.log("Service Worker is Supported");
                navigator.serviceWorker.register("js/sw.js").then(function(reg){
                    console.log("Service Worker Registration----*");
                    console.log(reg);

                    that.registrationObj = reg;

                }).catch(function(err){
                    console.log("Error----*");
                    console.log(err);
                });

            }else{
                console.log("This browser doesn't support Service Workers");
            }
        },

        requestPermission: function(){
            if (Notification.permission !== "granted")
                Notification.requestPermission();


            function notifyMe() {
                if (!Notification) {
                    alert('Desktop notifications not available in your browser. Try Chromium.');
                    return;
                }

                if (Notification.permission !== "granted")
                    Notification.requestPermission();
                else {
                    var notification = new Notification('Notification title', {
                        icon: 'http://cdn.sstatic.net/stackexchange/img/logos/so/so-icon.png',
                        body: "Hey there! You've been notified!"
                    });

                    notification.onclick = function () {
                        window.open("http://stackoverflow.com/a/13328397/1269037");
                    };

                }

            }
        },

        registerBrowserPushNotification: function(){
            if(!WebApp || !('serviceWorker' in navigator) || (typeof this.registrationObj.pushManager == "undefined")){
                return false;
            }

            var that = this, subscriptionId;
            var ModelInit = require('../models/model_initiator');
            var ModelConfigNames = require('../models/model_config_names');
            var userInfo = StoreManager.getStoreData(storeNames.userInfo);
            var model = ModelInit(ModelConfigNames.pushRegisterModel);

            this.registrationObj.pushManager.subscribe({
                userVisibleOnly: true
            }).then(function(sub){
                console.log(sub);

                Local_Storage.store("isSubscribed", true);
                that.subscribeObj = sub;
                var subObj = JSON.parse(JSON.stringify(sub));

                console.log(subObj);
                console.log("endpoint : ", sub.endpoint);
                console.log("auth : ", sub.getKey('auth'));
                console.log("p256dh : ", sub.getKey('p256dh'));

                subscriptionId = (sub.endpoint).replace("https://android.googleapis.com/gcm/send/", "");

                model.setModelData({
                    "externalId": userInfo.userId,
                    "isRegistered": true,
                    "notificationDeviceType": device.platform,
                    "notificationDeviceId": device.uuid,
                    "projectId": "klario-72a3e",
                    "registrationId": data.registrationId,
                    "clientKey" : {
                        "publicKey" : subObj.keys.p256dh,
                        "auth" : subObj.keys.auth
                    }
                });
                model.sendServerCall();

            });
        },

        unSubscribe: function(){
            if(!WebApp || !('serviceWorker' in navigator) || (typeof this.registrationObj.pushManager == "undefined")){
                return false;
            }

            this.subscribeObj.unsubscribe().then(function(event) {
                console.log('Unsubscribed!', event);
                Local_Storage.store("isSubscribed", false);
            }).catch(function(error) {
                console.log('Error unsubscribing', error);
            });
        }
    };

    module.exports = function(){
        return (new Push_Notification());
    }();

})();