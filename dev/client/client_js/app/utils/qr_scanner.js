(function(){
    "use strict";

    var QR_SCANNER= function(){

    };

    QR_SCANNER.prototype = {

        scan : function(callback){
            cordova.plugins.barcodeScanner.scan(
                function (result) {
                    console.log("We got a barcode\n" +
                        "Result: " + result.text + "\n" +
                        "Format: " + result.format + "\n" +
                        "Cancelled: " + result.cancelled);

                    callback(result.text, result.format);
                },
                function (error) {
                    console.log("Scanning failed: " + error);
                }
            );
        }
    };

    module.exports = function(){
        return  (new QR_SCANNER());
    }();

})();