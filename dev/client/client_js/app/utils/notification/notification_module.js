(function(){
    "use strict";

    var $ = require('jquery');

    var notification = function(){
        window.$ = $;
    };

    notification.prototype = {
        success: function(){
            this.notyObj = $.notify({
                // options
                message: arguments[0]
            },{
                // settings
                type: 'success',
                newest_on_top: true,
                offset: {
                    x: 20,
                    y: 60
                },
                delay: 3000,
                z_index: 999999999999
            });

            if($("div[data-notify='container']").length > 5){
                setTimeout(function() {
                    $.notifyClose();
                }, 1000);
            }
        },
        info: function(){
            this.notyObj = $.notify({
                // options
                message: arguments[0]
            },{
                // settings
                type: 'info',
                newest_on_top: true,
                offset: {
                    x: 20,
                    y: 60
                },
                delay: 3000,
                z_index: 999999999999
            });
            if($("div[data-notify='container']").length > 5){
                setTimeout(function() {
                    $.notifyClose();
                }, 1000);
            }
        },
        warning: function(){
            this.notyObj = $.notify({
                // options
                message: arguments[0]
            },{
                // settings
                type: 'warning',
                newest_on_top: true,
                offset: {
                    x: 20,
                    y: 60
                },
                delay: 3000,
                z_index: 999999999999
            });
            if($("div[data-notify='container']").length > 5){
                setTimeout(function() {
                    $.notifyClose();
                }, 1000);
            }
        },
        error: function(){
            this.notyObj = $.notify({
                // options
                message: arguments[0]
            },{
                // settings
                type: 'danger',
                newest_on_top: true,
                offset: {
                    x: 20,
                    y: 60
                },
                delay: 3000,
                z_index: 999999999999
            });
            if($("div[data-notify='container']").length > 5){
                setTimeout(function() {
                    $.notifyClose();
                }, 1000);
            }
        },
        device_success: function(){
            this.notyObj = $.notify({
                // options
                message: arguments[0]
            },{
                // settings
                type: 'success',
                newest_on_top: true,
                offset: {
                    x: 20,
                    y: 60
                },
                delay: 3000,
                z_index: 999999999999
            });
            if($("div[data-notify='container']").length > 5){
                setTimeout(function() {
                    $.notifyClose();
                }, 1000);
            }
        },
        device_error: function(){
            this.notyObj = $.notify({
                // options
                message: arguments[0]
            },{
                // settings
                type: 'danger',
                newest_on_top: true,
                offset: {
                    x: 20,
                    y: 60
                },
                delay: 3000,
                z_index: 999999999999
            });
            if($("div[data-notify='container']").length > 5){
                setTimeout(function() {
                    $.notifyClose();
                }, 1000);
            }
        },
        clear: function(){
            $.notifyClose();
        }
    };

    module.exports = function(){
        return (new notification());
    }();

})();