(function(){
    "use strict";

    var $ = require('jquery');
    var ValidatorOpt = require('./validator_options');

    var validation= function(id,optionsName){
        this.id=id;
        this.option=optionsName;
    };

    validation.prototype = {

        initValidator : function(){
            $('#'+this.id)
                .formValidation(ValidatorOpt.getOptions(this.option))
                .on('success.form.fv', function(e) {
                    // Prevent form submission
                    e.preventDefault();
                    return false;
                });
        },

        validate : function(){
            if(!$('#'+this.id).data('formValidation').isValid()){
               /* $("html, body").animate({
                    scrollTop: $("input:focus").offset().top-100
                }, 1000);*/

                return false;
            }else{
                return true;
            }
        },

        reset : function(){
            $('#'+this.id).data('formValidation').resetForm();
        }
    };

    module.exports = function(id,option){
        return  (new validation(id,option));

    };

})();