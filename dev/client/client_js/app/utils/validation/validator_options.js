(function(){
    "use strict";

    module.exports = function(){

        //var Moment = require("moment");

        var ValidatorOptions = {};

        var EnglishOptions = {

            login: {
                message: 'This value is not valid',
                live: 'disabled',
                fields: {
                    userId: {
                        validators: {
                            notEmpty: {
                                message: 'The email is required and cannot be empty'
                            }
                        }
                    },
                    inputPassword: {
                        validators: {
                            notEmpty: {
                                message: 'The password is required and cannot be empty'
                            }
                        }
                    }

                }
            },

            forgotPassword: {
                message: 'This value is not valid',
                live: 'disabled',
                fields: {
                    emailId: {
                        validators: {
                            notEmpty: {
                                message: 'The email is required and cannot be empty'
                            },
                            emailAddress: {
                                message: 'Please enter a valid email address'
                            }
                        }
                    }

                }
            },

            changePassword: {
                message: 'This value is not valid',
                live: 'disabled',
                fields: {
                    password: {
                        validators: {
                            notEmpty: {
                                message: 'The password is required and cannot be empty'
                            },
                            identical: {
                                field: 'confirmPassword',
                                message: 'The password and its confirm are not the same'
                            }
                        }
                    },
                    confirmPassword: {
                        validators: {
                            notEmpty: {
                                message: 'The confirm password is required and cannot be empty'
                            },
                            identical: {
                                field: 'password',
                                message: 'The password and its confirm are not the same'
                            }
                        }
                    }

                }
            },

            changePass : {
                message: 'This value is not valid',
                live: 'disabled',
                fields: {
                    password: {
                        validators: {
                            notEmpty: {
                                message: 'The password is required and cannot be empty'
                            },
                            identical: {
                                field: 'confirmPassword',
                                message: 'The password and its confirm are not the same'
                            }
                        }
                    },
                    confirmPassword: {
                        validators: {
                            notEmpty: {
                                message: 'The confirm password is required and cannot be empty'
                            },
                            identical: {
                                field: 'password',
                                message: 'The password and its confirm are not the same'
                            }
                        }
                    }

                }
            },

            newRegister: {
                message: 'This value is not valid',
                live: 'disabled',
                fields: {
                    firstName: {
                        validators: {
                            notEmpty: {
                                message: 'The first name is required and cannot be empty'
                            }
                        }
                    },
                    lastName: {
                        validators: {
                            notEmpty: {
                                message: 'The last name is required and cannot be empty'
                            }
                        }
                    },
                    company: {
                        validators: {
                            notEmpty: {
                                message: 'The company is required and cannot be empty'
                            }
                        }
                    },
                    emailId: {
                        validators: {
                            notEmpty: {
                                message: 'The email is required and cannot be empty'
                            },
                            emailAddress: {
                                message: 'Please enter a valid email address'
                            }
                        }
                    },
                    phoneNo: {
                        validators: {
                            notEmpty: {
                                message: 'The phone number is required and cannot be empty'
                            },
                            intlTelNo: {
                                message: 'Please enter a valid phone number'
                            }
                        }
                    },
                    password: {
                        validators: {
                            notEmpty: {
                                message: 'The password is required and cannot be empty'
                            },
                            identical: {
                                field: 'confirmPassword',
                                message: 'The password and its confirm are not the same'
                            }
                        }
                    },
                    confirmPassword: {
                        validators: {
                            notEmpty: {
                                message: 'The confirm password is required and cannot be empty'
                            },
                            identical: {
                                field: 'password',
                                message: 'The password and its confirm are not the same'
                            }
                        }
                    }

                }
            },

            addNewDevice: {
                message: 'This value is not valid',
                live: 'disabled',
                fields: {
                    serialNo: {
                        validators: {
                            notEmpty: {
                                message: 'The serial no. is required and cannot be empty'
                            }
                        }
                    }
                }
            },

            inviteNewUserPermission: {
                message: 'This value is not valid',
                live: 'disabled',
                fields: {
                    subOwnerEmailId: {
                        validators: {
                            notEmpty: {
                                message: 'Email Id is required and cannot be empty'
                            }
                        }
                    }
                }
            },

            addDeviceData : {
                message: 'This value is not valid',
                live: 'disabled',
                fields: {
                    deviceName: {
                        validators: {
                            notEmpty: {
                                message: 'Device Name is required and cannot be empty'
                            }
                        }
                    },
                    deviceLocation: {
                        validators: {
                            notEmpty: {
                                message: 'Location is required and cannot be empty'
                            }
                        }
                    }
                }
            },

            applianceSettings : {
                message: 'This value is not valid',
                live: 'disabled',
                fields: {
                    applianceName: {
                        validators: {
                            notEmpty: {
                                message: 'Device Name is required and cannot be empty'
                            }
                        }
                    },
                    applianceLocation: {
                        validators: {
                            notEmpty: {
                                message: 'Location is required and cannot be empty'
                            }
                        }
                    }
                }
            },

            addHoliday: {
                message: 'This value is not valid',
                live: 'disabled',
                fields: {
                    holidayDesc: {
                        validators: {
                            notEmpty: {
                                message: 'The holiday name is required and cannot be empty'
                            }
                        }
                    }
                }
            }

        };

        ValidatorOptions.getOptions = function(viewName){

            return EnglishOptions[viewName];

            /*if(window.langSelected == "EN"){
                return EnglishOptions[viewName];
            }else if(window.langSelected == "FR"){
                return EnglishOptions[viewName];
            }else if(window.langSelected == "DE"){
                return EnglishOptions[viewName];
            }else if(window.langSelected == "IT"){
                return EnglishOptions[viewName];
            }else if(window.langSelected == "ES"){
                return SpanishOptions[viewName];
            }else{
                return EnglishOptions[viewName];
            }*/
        };

        return ValidatorOptions;
    }();

})();
