(function(){
    "use strict";

    var $ = require('jquery');

    var Utils= function(){
        
    };

    Utils.prototype = {


        bindBackButtonEvent : function(){
            document.addEventListener("backbutton", onBackKeyDown, false);
            function onBackKeyDown() {
                var routeHash = window.location.hash;

                if($(".modal").is(":visible")){
                    $(".modal").modal("hide");
                    return false;
                }

                if(routeHash == "#login" || routeHash == "#appliance"){
                    navigator.app.exitApp();
                }else{
                    window.history.back();
                }
            }
        },

        removeBase64MimeText : function(text){
            return (text.replace(new RegExp("data:(.*)base64,","g") , ""));
        },

        scrollElementToTop: function(event){
            var elemPos = $(event.currentTarget).offset().top;
            var bodyHeight = $('html,body').height();
            var cutOffHeight = bodyHeight - 150;
            var scrollPos = elemPos - (bodyHeight/2);

            if(elemPos > cutOffHeight){
                $('html,body').animate({
                    scrollTop : scrollPos
                }, 800);
            }

        },
        scrollElementToTopWithId: function(id){
            var elemPos = $(id).offset().top;
            var bodyHeight = $('html,body').height();
            var cutOffHeight = bodyHeight - 150;
            var scrollPos = elemPos - (bodyHeight/2);

            if(elemPos > cutOffHeight){
                $('html,body').animate({
                    scrollTop : scrollPos
                }, 800);
            }

        },

        scrollIdToTop: function(id){
            $('html,body').animate({scrollTop: $(id).offset().top}, 800);
        }
    };

    module.exports = function(){
        return  (new Utils());
    }();

})();