(function(){
    "use strict";

    var UnderI18 = require('../../../lib/underi18n.js').underi18n;
    var templateLoader = require("../../modules/tpl_loader_module.js");

    var langContent = function(){
        this.ENGLISH = {

            'HeaderView': {

            },
            'FooterView': {

            },
            'LoginView': {
                'loginHeader': "Login",
                'userIdLabelText': "User ID",
                'userIdPlaceholder': "User ID",
                'passwordLabelText': "Password",
                'passwordPlaceholder': "Password",
                'loginButtonText': "Login",
                'forgotPassLinkText': "Forgot Password?"
            },
            'SidebarMenuView': {
                'menu1' : "Menu"
            },
            'LandingPageView': {
                'viewHeaderText' : "Landing Page"
            },
			'DeviceCalendarView': {
				 '' : ''
			},
			'CalendarView': {
				 '' : ''
			},
			'HolidayView': {
				 '' : ''
			},
			'WeeklyView': {
				 '' : ''
			},
			'HolidayListView': {
				 '' : ''
			},
			'AddHolidayView': {
				 '' : ''
			},
			'ApplianceView': {
				 '' : ''
			},
			'ApplianceListView': {
				 '' : ''
			},
			'AddApplianceView': {
				 '' : ''
			},
			'AboutView': {
				 '' : ''
			},
			'RegistrationView': {
				 '' : ''
			},
			'ApplianceSettingsView': {
				 '' : '' 
			},
			'SettingsView': {
				 '' : '' 
			},
			'ForgotPassView': {
				 '' : '' 
			},
			'ChangePassView': {
				 '' : '' 
			},
			'SettingChangePassView': {
				 '' : '' 
			},
			'GlobalSettingsView': {
				 '' : '' 
			},
			'UserPermissionView': {
				 '' : '' 
			},
			'UserAccessListView': {
				 '' : '' 
			},
			'UserRequestListView': {
				 '' : '' 
			},
			'UserApprovalView': {
				 '' : '' 
			},
			'UserRemovalView': {
				 '' : '' 
			},
			'UserPermissionInviteView': {
				 '' : '' 
			}/*PrependViewLanguageObj*/
        };
    };

    langContent.prototype = {

        getCurrentLangContent: function(){

            /*if(window.langSelected == "EN"){
             return ENGLISH;
             }else if(window.langSelected == "FR"){
             return ENGLISH;
             }else if(window.langSelected == "DE"){
             return ENGLISH;
             }else if(window.langSelected == "IT"){
             return ENGLISH;
             }else if(window.langSelected == "ES"){
             return ENGLISH;
             }else{
             return ENGLISH;
             }*/

            return this.ENGLISH;
        },

        getCurrentLangViewTemplate: function(){
                var l_en = UnderI18.MessageFactory(this.getCurrentLangContent()[arguments[0]]);
                var template = UnderI18.template(templateLoader.getViewTemplate(arguments[1]), l_en);

                return template;
        }

    };

    module.exports = function() {
        return (new langContent());
    }();
})();