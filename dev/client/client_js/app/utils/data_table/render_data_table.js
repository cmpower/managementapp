(function(){
    "use strict";

    var $ = require('jquery');

    var dataTableModule = function(id,options,data){
        this.id = id;
        this.options = options;
        this.options["aaData"] = data;
    };

    dataTableModule.prototype = {

        renderDataTable : function(){
            return ($('#'+this.id).DataTable(this.options));
        }

    };

    module.exports = function(id,options,data){
        return (new dataTableModule(id,options,data))
    }

})();