
/*
$('#signUpForm').submit(function(){
    var form = new formComponent('signUpForm',['userName','password','mobileNo','name','companyName','emailId']);
    var data= form.findElement();
    console.log(data);
});

$('#w').click(function(){
    var form1 = new formComponent('signUpForm');
    var data= form1.clearFormElementData();
    console.log(data);
});*/

(function(){
    "use strict";

    var $ = require('jquery');

    var formComponent = function(id,data){
        this.id = id;
        this.requiredData = data;
    };
    formComponent.prototype = {

        getValues : function(){

            var formData = {};

            var formInfo = document.getElementById(this.id);

            for(var i=0;i<formInfo.elements.length;i++){

                var name = formInfo.elements[i].name ;

                var v = (formInfo.elements[i].type == 'checkbox') ? 'checked' :(formInfo.elements[i].type == 'submit') ? 'innerHTML': 'value';

                formData[name]=formInfo.elements[i][v];

            }
            return  this.makeData(formData);


        },
        makeData : function(data){

            var obj = {};
            for(var i=0;i<this.requiredData.length;i++){
                obj[this.requiredData[i]] = data[this.requiredData[i]];
            }
            console.log(obj);
            return obj

        },
        clearFormElementData : function(){
            var formInfo = document.getElementById(this.id);

            for(var i=0;i<formInfo.elements.length;i++){

                var name = formInfo.elements[i].name ;

                var v = (formInfo.elements[i].type == 'checkbox') ? 'checked' :(formInfo.elements[i].type == 'submit') ? 'null': 'value';

                (v != 'null') ? formInfo.elements[i][v] = '':'';

            }
            return 'cleared';
        }
    };

    module.exports = function(id,data){
       return new formComponent(id,data);
    }

})();