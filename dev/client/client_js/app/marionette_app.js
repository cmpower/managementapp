(function () {
    "use strict";

    window.$ = require('jquery');

    var Backbone = require('backbone');
    var Marionette = require('backbone.marionette');

    require('./behaviors/behaviors');

    var AppActivity = require('./modules/app_activity_module');
    var Utils = require('./utils/utils');
    var attachFastClick = require('../lib/fastclick');

    var MarionetteApp = function MarionetteApp() {
        this.App = new Marionette.Application();
    };

    MarionetteApp.prototype = {
        start: function start() {
            this.App.start();
            Backbone.history.start();
            console.log("Marionette app started with history");

            AppActivity.AppSession();

            Utils.bindBackButtonEvent();
            attachFastClick(document.body);
        }
    };

    module.exports = (function () {
        return new MarionetteApp();
    })();
})();