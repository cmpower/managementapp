(function(){
    "use strict";

    module.exports = function () {

        var storage = {

            'user_info' : {},
            'appliance_list' : {},
            'schedule_list' : {},
            'chosen_device_id' : {},
            'set_schedule_response_data' : {},
            'chosen_device_name' : {},
            'current_selected_device' : {},
            'appliance_wiFi_settings' : {},
            'loin_error' : {},
            'appliance_notification_setting' : {},
            'add_device_data' : {},
			'user_request_list' : {},
			'user_access_list' : {}/*Prepend_Storage_Object*/

        };

        return storage;
    };

})();
