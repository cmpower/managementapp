(function(){
    "use strict";

    module.exports = function () {
        var storeNames = {

            'userInfo' : "user_info",
            'applianceList' : 'appliance_list',
            'scheduleList' : 'schedule_list',
            'chosenDeviceId' : 'chosen_device_id',
            'setScheduleResponseData' : 'set_schedule_response_data',
            'chosenDeviceName' : 'chosen_device_name',
            'currentSelectedDevice' : 'current_selected_device',
            'applianceWiFiSettings' : 'appliance_wiFi_settings',
            'loinError' : 'loin_error',
            'applianceNotificationSetting' : 'appliance_notification_setting',
            'addDeviceData' : 'add_device_data',
			'userRequestList' : 'user_request_list',
			'userAccessList' : 'user_access_list'/*Prepend_Storage_Name*/


        };
        return storeNames;
    }();

})();
