(function(){
    "use strict";

    var _ = require('underscore');
    var Storage = require('./storage');

    var storageManager = function () {

    };

    storageManager.prototype = {

        init : function(){
            this.storage = Storage();
        },
        getStoreData : function(objName){
            var obj = this.storage[objName];
            if(typeof obj == "undefined"){
                console.error("Storage doesn't exists : "+objName+"");
                return {};
            }else{
                return _.clone(obj);
            }
        },
        setStoreData : function(objName, objData){
            if(typeof this.storage[objName] != "undefined"){
                this.storage[objName] = (typeof objData != "undefined")? objData : {};
                return true;
            }else{
                console.error("Storage doesn't exists : "+objName+"");
                return false;
            }
        },
        clearStoreData : function(storeNames){
            var storageData = Storage();
            for(var i = 0; i < storeNames.length; i++){
                this.storage[storeNames[i]] = storageData[storeNames[i]];
            }
        },
        clearStorage : function(){
            this.storage = Storage();
        }
    };

    module.exports = function () {
        return (new storageManager());
    }();

})();
