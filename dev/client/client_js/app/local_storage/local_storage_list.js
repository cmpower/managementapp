(function(){
    "use strict";

    module.exports = function () {

        var LocalStorageNames = {

            'targetUrl' : "target_url",
            'loggedIn' : "logged_in",
            'isSubscribed' : "is_subscribed",
            'appNotification' : "app_notification",
            'rememberMe' : "remember_me",
            'userName' : "user_name",
            'userPassword' : "userPassword"



        };

        return LocalStorageNames;
    }();

})();
