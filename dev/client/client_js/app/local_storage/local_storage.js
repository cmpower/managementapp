(function(){
    "use strict";

    var LocalStorage= function(){

    };

    LocalStorage.prototype = {

        store : function(key, value){
            window.localStorage.setItem(key, value);
        },

        retrieve : function(key){
            return ( (typeof window.localStorage.getItem(key) != "undefined")? window.localStorage.getItem(key) : null );
        },

        clear : function(key){
            window.localStorage.getItem(key, "");
        },

        clearItem : function (key) {
          window.localStorage.removeItem(key);
        },
        clearStoreItems : function(key){
            for(var i = 0; i < key.length; i++){
                window.localStorage.removeItem(key[i]);
            }
        },

        clearAll : function(){
            window.localStorage.clear();
        }
    };

    module.exports = function(){
        return  (new LocalStorage());
    }();

})();