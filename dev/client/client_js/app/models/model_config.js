(function() {
    "use strict";

    module.exports = {

        'login_model_config' : require("./configs/login_config"),

        'logout_model_config' : require("./configs/logout_config"),

		'appliance_list_config' : require("./configs/appliance_list_config"),

		'appliance_status_change_model_config' : require("./configs/appliance_status_change_config"),

		'push_register_model_config' : require("./configs/push_register_config"),

		'new_register_model_config' : require("./configs/new_register_config"),

		'schedule_calendar_model_config' : require("./configs/schedule_calendar_config"),

		'calendar_schedule_list_model_config' : require("./configs/calendar_schedule_list_config"),

		'add_appliance_model_config' : require("./configs/add_appliance_config"),

		'appliance_onboarding_update_model_config' : require("./configs/appliance_onboarding_update_config"),

		'get_appliance_wifi_settings_model_config' : require("./configs/get_appliance_wifi_settings_config"),

		'forgot_password_model_config' : require("./configs/forgot_password_config"),

		'change_password_model_config' : require("./configs/change_password_config"),

		'setting_change_password_model_config' : require("./configs/setting_change_password_config"),

		'update_custom_schedule_model_config' : require("./configs/update_custom_schedule_config"),

		'update_weekly_schedule_model_config' : require("./configs/update_weekly_schedule_config"),

		'update_holiday_schedule_model_config' : require("./configs/update_holiday_schedule_config"),

		'get_appliance_notification_setting_model_config' : require("./configs/get_appliance_notification_setting_config"),

		'set_appliance_notification_setting_model_config' : require("./configs/set_appliance_notification_setting_config"),

		'user_request_list_model_config' : require("./configs/user_request_list_config"),

		'user_access_list_model_config' : require("./configs/user_access_list_config"),

		'approve_access_model_config' : require("./configs/approve_access_config"),

		'reject_access_model_config' : require("./configs/reject_access_config"),

		'revoke_access_model_config' : require("./configs/revoke_access_config"),

		'remove_appliance_owner_model_config' : require("./configs/remove_appliance_owner_config"),

		'user_appliance_permission_invite_model_config' : require("./configs/user_appliance_permission_invite_config")/*AppendModelConfig*/
    };

})();

