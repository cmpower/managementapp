(function() {
    "use strict";

    var Backbone = require('backbone');
    var ModelConfig = require('./model_config');
    var ApiCall = require("../api/api_call");

    var Model = function Model(configName) {
        this.configName = configName;
        this.config = ModelConfig[configName];
        this.modelObject = this.config.getModelProperties();

        var NewModel = Backbone.Model.extend(this.modelObject);
        this.model = new NewModel();
    };

    Model.prototype = {

        setModelData: function(modelData){
            this.model.set(modelData);
        },

        sendServerCall: function () {
            var serverCall = ApiCall(this.configName);
            serverCall.makeCall(this.model);
        }

    };

    module.exports = function(configName){
        return (new Model(configName));
    };
})();