(function() {
    "use strict";

    module.exports = {


        'loginModel'    : 'login_model_config',
        'logoutModel'   : 'logout_model_config',

		'applianceListModel' : 'appliance_list_config',

		'applianceStatusChangeModel' : 'appliance_status_change_model_config',

		'pushRegisterModel' : 'push_register_model_config',

		'newRegisterModel' : 'new_register_model_config',

		'scheduleCalendarModel' : 'schedule_calendar_model_config',

		'calendarScheduleListModel' : 'calendar_schedule_list_model_config',

		'addApplianceModel' : 'add_appliance_model_config',

		'applianceOnboardingUpdateModel' : 'appliance_onboarding_update_model_config',

		'getApplianceWifiSettingsModel' : 'get_appliance_wifi_settings_model_config',

		'forgotPasswordModel' : 'forgot_password_model_config',

		'changePasswordModel' : 'change_password_model_config',

		'settingChangePasswordModel' : 'setting_change_password_model_config',

		'updateCustomScheduleModel' : 'update_custom_schedule_model_config',

		'updateWeeklyScheduleModel' : 'update_weekly_schedule_model_config',

		'updateHolidayScheduleModel' : 'update_holiday_schedule_model_config',

		'getApplianceNotificationSettingModel' : 'get_appliance_notification_setting_model_config',

		'setApplianceNotificationSettingModel' : 'set_appliance_notification_setting_model_config',

		'getUserRequestListModel' : 'user_request_list_model_config',

		'getUserAccessListModel' : 'user_access_list_model_config',

		'getApproveAccessModel' : 'approve_access_model_config',

		'getRejectAccessModel' : 'reject_access_model_config',

		'getRevokeAccessModel' : 'revoke_access_model_config',

		'removeApplianceOwnerModel' : 'remove_appliance_owner_model_config',

		'userAppliancePermissionInviteModel' : 'user_appliance_permission_invite_model_config'/*AppendModelConfigName*/


	};

})();

