(function() {
    "use strict";

    var AppApiUrl = require('../../api/api_url');

    module.exports = {

        'getModelProperties':   function(){
            return ({
                'url':   AppApiUrl.getApiUrl("login"),
                'defaults': {
                    "userName": "",
                    "password": ""
                }
            });
        },
        'method'            :   'POST',

        'responseHandler'   :   'login_response',
        'success_queries'   :   ['store_data', 'app_state_in'],         // ['store_data','clear_data', 'app_state_in', 'app_state_out']
        'error_queries'     :   ['store_data'],                                     // ['store_data','clear_data', 'app_state_in', 'app_state_out']
        'success_commands'  :   ['navigate'],                           // ['navigate', 'call_view_function', 'notification']
        'error_commands'    :   ['call_view_function']                        // ['navigate', 'call_view_function', 'notification']

    };

})();

