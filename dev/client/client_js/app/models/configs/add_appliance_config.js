(function() {
    "use strict";

    var AppApiUrl = require('../../api/api_url');

    module.exports = {

        'getModelProperties':   function(){
            return ({
                'url':   AppApiUrl.getApiUrl("add_appliance"),
                'defaults': {"userId": "", "serialId": ""}
            });
        },
        'method'            :   'POST',                 // POST / GET

        'responseHandler'   :   'add_appliance_response',
        'success_queries'   :   ['store_data'],                     // ['store_data','clear_data', 'app_state_in', 'app_state_out']
        'error_queries'     :   [],                     // ['store_data','clear_data', 'app_state_in', 'app_state_out']
        'success_commands'  :   ['call_view_function','notification'],                     // ['navigate', 'call_view_function', 'notification']
        'error_commands'    :   ['notification']                      // ['navigate', 'call_view_function', 'notification']

    };

})();

