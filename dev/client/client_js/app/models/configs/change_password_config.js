(function() {
    "use strict";

    var AppApiUrl = require('../../api/api_url');

    module.exports = {
        'getModelProperties':   function(){
            return ({
                'url':   AppApiUrl.getApiUrl("change_password"),
                'defaults': {"userId" : "", "password" : ""}
            });
        },
        'method'            :   'POST',                 // POST / GET

        'responseHandler'   :   'change_password_response',
        'success_queries'   :   ['store_data'],                     // ['store_data','clear_data', 'app_state_in', 'app_state_out']
        'error_queries'     :   [],                     // ['store_data','clear_data', 'app_state_in', 'app_state_out']
        'success_commands'  :   ['navigate', 'notification'],                     // ['navigate', 'call_view_function', 'notification']
        'error_commands'    :   ['notification']                      // ['navigate', 'call_view_function', 'notification']

    };

})();

