(function() {
    "use strict";

    var AppApiUrl = require('../../api/api_url');

    module.exports = {

        'getModelProperties':   function(){
            return ({
                'url':   AppApiUrl.getApiUrl("new_register"),
                'defaults': {
                    "userName": "",
                    "password": "",
                    "userData": {
                        "company": "",
                        "phoneNo": "",
                        "emailId": "",
                        "firstName": "",
                        "lastName": ""
                }}
            });
        },
        'method'            :   'POST',                 // POST / GET

        'responseHandler'   :   'new_register_response',
        'success_queries'   :   [],                     // ['store_data','clear_data', 'app_state_in', 'app_state_out']
        'error_queries'     :   [],                     // ['store_data','clear_data', 'app_state_in', 'app_state_out']
        'success_commands'  :   ['navigate','notification'],                     // ['navigate', 'call_view_function', 'notification']
        'error_commands'    :   ['notification']                      // ['navigate', 'call_view_function', 'notification']

    };

})();

