(function() {
    "use strict";

    var AppApiUrl = require('../../api/api_url');

    module.exports = {

        'getModelProperties':   function(){
            return ({
                'url':   AppApiUrl.getApiUrl("logout"),
                'defaults': {"userId": "string"}
            });
        },
        'method'            :   'POST',                 // POST / GET

        'responseHandler'   :   'logout_response',
        'success_queries'   :   [],                     // ['store_data','clear_data', 'app_state_in', 'app_state_out']
        'error_queries'     :   [],                     // ['store_data','clear_data', 'app_state_in', 'app_state_out']
        'success_commands'  :   ['navigate'],                     // ['navigate', 'call_view_function', 'notification']
        'error_commands'    :   ['navigate']                      // ['navigate', 'call_view_function', 'notification']

    };

})();

