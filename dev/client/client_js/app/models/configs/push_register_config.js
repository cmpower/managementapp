(function() {
    "use strict";

    var AppApiUrl = require('../../api/api_url');

    module.exports = {

        'getModelProperties':   function(){
            return ({
                'url':   AppApiUrl.getApiUrl("push_register"),
                'defaults': {"externalId": "","isRegistered": true,"notificationDeviceType": "","notificationDeviceId": "","projectId": "", "registrationId": ""}
            });
        },
        'method'            :   'POST',                 // POST / GET

        'responseHandler'   :   'push_register_response',
        'success_queries'   :   ['store_data'],                     // ['store_data','clear_data', 'app_state_in', 'app_state_out']
        'error_queries'     :   ['store_data'],                     // ['store_data','clear_data', 'app_state_in', 'app_state_out']
        'success_commands'  :   [],                     // ['navigate', 'call_view_function', 'notification']
        'error_commands'    :   ['notification']                      // ['navigate', 'call_view_function', 'notification']

    };

})();

