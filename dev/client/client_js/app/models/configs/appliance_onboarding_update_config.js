(function() {
    "use strict";

    var AppApiUrl = require('../../api/api_url');

    module.exports = {

        'getModelProperties':   function(){
            return ({
                'url':   AppApiUrl.getApiUrl("appliance_onboarding_update"),
                'defaults': {
                    "userId": "string",
                    "deviceId": "string",
                    "deviceName": "string",
                    "deviceLocationName": "string"
                }
            });
        },
        'method'            :   'POST',                 // POST / GET

        'responseHandler'   :   'appliance_onboarding_update_response',
        'success_queries'   :   [],                     // ['store_data','clear_data', 'app_state_in', 'app_state_out']
        'error_queries'     :   [],                     // ['store_data','clear_data', 'app_state_in', 'app_state_out']
        'success_commands'  :   ['call_view_function', 'notification'],                     // ['navigate', 'call_view_function', 'notification']
        'error_commands'    :   ['notification']                      // ['navigate', 'call_view_function', 'notification']

    };

})();

