"use strict";

require('./import_libs/import_libs.js');
var App_Start = require("./radio/app_start");

function deviceReadyFunction(){
    App_Start.trigger("app:start:core");
}
document.addEventListener("deviceready", deviceReadyFunction, false);

if(WebApp) {
    App_Start.trigger("app:start:core");
}