(function(){
    "use strict";

    var viewBuilder = require("./view_builder");
    var viewModule = require("./view_modules");
    var viewModel = require("./view_model");

    var ViewConstants = function(){

    };

    ViewConstants.prototype = {
        //---------------------------------
        'getHeaderView': function(){
            var model = viewModel();
            var view = viewBuilder.buildView(viewModule.HeaderView, model);
            return (view);
        },
        //---------------------------------
        'getFooterView': function(){
            var view = viewBuilder.buildView(viewModule.FooterView);
            return (view);
        },
        //---------------------------------
        'getSidebarView': function(){
            var view = viewBuilder.buildView(viewModule.SidebarView);
            return (view);
        },
        //---------------------------------
        'getLandingPageView': function(){
            var view = viewBuilder.buildView(viewModule.LandingPageView);
            return (view);
        },
        'getLoginView': function(){
            var view = viewBuilder.buildView(viewModule.LoginView);
            return (view);
        },
		'getDeviceCalendarView': function(){
			 var view = viewBuilder.buildView(viewModule.DeviceCalendarView);
			 return (view);
		},
		'getCalendarView': function(data){
			 var model = viewModel(data); 
			 var view = viewBuilder.buildView(viewModule.CalendarView, model);
			 return (view);
		},
		'getHolidayView': function(data){
			 var model = viewModel(data); 
			 var view = viewBuilder.buildView(viewModule.HolidayView, model);
			 return (view);
		},
		'getWeeklyView': function(data){
			 var model = viewModel(data); 
			 var view = viewBuilder.buildView(viewModule.WeeklyView, model);
			 return (view);
		},
		'getHolidayListView': function(data){
			 var model = viewModel(data); 
			 var view = viewBuilder.buildView(viewModule.HolidayListView, model);
			 return (view);
		},
		'getAddHolidayView': function(data){
			 var model = viewModel(data); 
			 var view = viewBuilder.buildView(viewModule.AddHolidayView, model);
			 return (view);
		},
		'getApplianceView': function(){
			 var view = viewBuilder.buildView(viewModule.ApplianceView);
			 return (view);
		},
		'getApplianceListView': function(data){
			 var model = viewModel(data); 
			 var view = viewBuilder.buildView(viewModule.ApplianceListView, model);
			 return (view);
		},
		'getAddApplianceView': function(data){
			 var model = viewModel(data); 
			 var view = viewBuilder.buildView(viewModule.AddApplianceView, model);
			 return (view);
		},
		'getAboutView': function(){
			 var view = viewBuilder.buildView(viewModule.AboutView);
			 return (view);
		},
		'getRegistrationView': function(){
			 var view = viewBuilder.buildView(viewModule.RegistrationView);
			 return (view);
		},
		'getApplianceSettingsView': function(){
			 var view = viewBuilder.buildView(viewModule.ApplianceSettingsView);
			 return (view);
		},
		'getSettingsView': function(){
			 var view = viewBuilder.buildView(viewModule.SettingsView);
			 return (view);
		},
		'getForgotPassView': function(){
			 var view = viewBuilder.buildView(viewModule.ForgotPassView);
			 return (view);
		},
		'getChangePassView': function(){
			 var view = viewBuilder.buildView(viewModule.ChangePassView);
			 return (view);
		},
		'getSettingChangePassView': function(){
			 var view = viewBuilder.buildView(viewModule.SettingChangePassView);
			 return (view);
		},
		'getGlobalSettingsView': function(){
			 var view = viewBuilder.buildView(viewModule.GlobalSettingsView);
			 return (view);
		},
		'getUserPermissionView': function(){
			var view = viewBuilder.buildView(viewModule.UserPermissionView);
			return (view);
		},
		'getUserAccessListView': function(data){
			 var model = viewModel(data); 
			 var view = viewBuilder.buildView(viewModule.UserAccessListView, model);
			 return (view);
		},
		'getUserRequestListView': function(data){
			 var model = viewModel(data); 
			 var view = viewBuilder.buildView(viewModule.UserRequestListView, model);
			 return (view);
		},
		'getUserApprovalView': function(){
			 var view = viewBuilder.buildView(viewModule.UserApprovalView);
			 return (view);
		},
		'getUserRemovalView': function(){
			 var view = viewBuilder.buildView(viewModule.UserRemovalView);
			 return (view);
		},
		'getUserPermissionInviteView': function(data){
			 var model = viewModel(data); 
			 var view = viewBuilder.buildView(viewModule.UserPermissionInviteView, model);
			 return (view);
		}/*Prepend_View_Elements_Object*/
        //---------------------------------
    };

    module.exports = function(){
        return (new ViewConstants());
    }();

})();
