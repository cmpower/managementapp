(function(){
    "use strict";

    module.exports = function(){
        var ViewPath = {

            'HeaderView' : {
                "view_elems" : require( '../../../views/header/view/view_elements'),
                "view_funcs" : require( '../../../views/header/view/view_functions')
            },
            'FooterView' : {
                "view_elems" : require( '../../../views/footer/view/view_elements'),
                "view_funcs" : require( '../../../views/footer/view/view_functions')
            },
            'SidebarView' : {
                "view_elems" : require( '../../../views/sidebar/view/view_elements'),
                "view_funcs" : require( '../../../views/sidebar/view/view_functions')
            },

            'LandingPageView' : {
                "view_elems" : require( '../../../views/landing_page/view/view_elements'),
                "view_funcs" : require( '../../../views/landing_page/view/view_functions')
            },
            'LoginView' : {
                "view_elems" : require( '../../../views/login/view/view_elements'),
                "view_funcs" : require( '../../../views/login/view/view_functions')
            },
			'DeviceCalendarView': {
				 'view_elems' : require( '../../../views/device_calendar/view/view_elements'),
				 'view_funcs' : require( '../../../views/device_calendar/view/view_functions')
			},
			'CalendarView': {
				 'view_elems' : require( '../../../views/device_calendar/calendar/view/view_elements'),
				 'view_funcs' : require( '../../../views/device_calendar/calendar/view/view_functions')
			},
			'HolidayView': {
				 'view_elems' : require( '../../../views/device_calendar/holiday/view/view_elements'),
				 'view_funcs' : require( '../../../views/device_calendar/holiday/view/view_functions')
			},
			'WeeklyView': {
				 'view_elems' : require( '../../../views/device_calendar/weekly/view/view_elements'),
				 'view_funcs' : require( '../../../views/device_calendar/weekly/view/view_functions')
			},
			'HolidayListView': {
				 'view_elems' : require( '../../../views/device_calendar/holiday/holiday_list/view/view_elements'),
				 'view_funcs' : require( '../../../views/device_calendar/holiday/holiday_list/view/view_functions')
			},
			'AddHolidayView': {
				 'view_elems' : require( '../../../views/device_calendar/holiday/add_holiday/view/view_elements'),
				 'view_funcs' : require( '../../../views/device_calendar/holiday/add_holiday/view/view_functions')
			},
			'ApplianceView': {
				 'view_elems' : require( '../../../views/appliance/view/view_elements'),
				 'view_funcs' : require( '../../../views/appliance/view/view_functions')
			},
			'ApplianceListView': {
				 'view_elems' : require( '../../../views/appliance/appliance_list/view/view_elements'),
				 'view_funcs' : require( '../../../views/appliance/appliance_list/view/view_functions')
			},
			'AddApplianceView': {
				 'view_elems' : require( '../../../views/appliance/add_appliance/view/view_elements'),
				 'view_funcs' : require( '../../../views/appliance/add_appliance/view/view_functions')
			},
			'AboutView': {
				 'view_elems' : require( '../../../views/about/view/view_elements'),
				 'view_funcs' : require( '../../../views/about/view/view_functions')
			},
			'RegistrationView': {
				 'view_elems' : require( '../../../views/registration/view/view_elements'),
				 'view_funcs' : require( '../../../views/registration/view/view_functions')
			},
			'ApplianceSettingsView': {
				 'view_elems' : require( '../../../views/appliance/appliance_settings/view/view_elements'),
				 'view_funcs' : require( '../../../views/appliance/appliance_settings/view/view_functions')
			},
			'SettingsView': {
				 'view_elems' : require( '../../../views/settings/view/view_elements'),
				 'view_funcs' : require( '../../../views/settings/view/view_functions')
			},
			'ForgotPassView': {
				 'view_elems' : require( '../../../views/forgot-pass/view/view_elements'),
				 'view_funcs' : require( '../../../views/forgot-pass/view/view_functions')
			},
			'ChangePassView': {
				 'view_elems' : require( '../../../views/change-pass/view/view_elements'),
				 'view_funcs' : require( '../../../views/change-pass/view/view_functions')
			},
			'SettingChangePassView': {
				'view_elems' : require( '../../../views/settings/setting_change_pass/view/view_elements'),
				'view_funcs' : require( '../../../views/settings/setting_change_pass/view/view_functions')
			},
			'GlobalSettingsView': {
				 'view_elems' : require( '../../../views/global_settings/view/view_elements'),
				 'view_funcs' : require( '../../../views/global_settings/view/view_functions')
			},
			'UserPermissionView': {
				'view_elems' : require( '../../../views/user_permission/view/view_elements'),
				'view_funcs' : require( '../../../views/user_permission/view/view_functions')
			},
			'UserAccessListView': {
				 'view_elems' : require( '../../../views/user_permission/user_access_list/view/view_elements'),
				 'view_funcs' : require( '../../../views/user_permission/user_access_list/view/view_functions')
			},
			'UserRequestListView': {
				 'view_elems' : require( '../../../views/user_permission/user_request_list/view/view_elements'),
				 'view_funcs' : require( '../../../views/user_permission/user_request_list/view/view_functions')
			},
			'UserApprovalView': {
				 'view_elems' : require( '../../../views/user_approval/view/view_elements'),
				 'view_funcs' : require( '../../../views/user_approval/view/view_functions')
			},
			'UserRemovalView': {
				 'view_elems' : require( '../../../views/user_removal/view/view_elements'),
				 'view_funcs' : require( '../../../views/user_removal/view/view_functions')
			},
			'UserPermissionInviteView': {
				 'view_elems' : require( '../../../views/user_permission/user_permission_invite/view/view_elements'),
				 'view_funcs' : require( '../../../views/user_permission/user_permission_invite/view/view_functions')
			}/*Prepend_View_Module_Object*/

        };

        return ViewPath;
    }();

})();
