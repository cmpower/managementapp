(function(){
    "use strict";

    var Backbone = require('backbone');
    var model = Backbone.Model.extend();

    var ViewModel = function(data){
        this.model = new model(data);
        return this.model;
    };

    ViewModel.prototype = {

        setModelData: function(data){
            this.model = new model(data);
        },

        getModelData: function(){
            return this.model;
        }
    };

    module.exports = function(data){
        return (new ViewModel(data));
    };

})();
