(function(){
    "use strict";

    module.exports = function(){

//----------------- Library Files ---------------------------------------------------------
        var Backbone = require('backbone');
        var Marionette = require('backbone.marionette');
        var _ = require('underscore');
//---------------------------------------------------------------------------

        var ViewBuilder = function(){

        };

        ViewBuilder.prototype = {

            buildView: function(viewModule, modelObj){
                var viewOpt = {};
                var ViewElements = viewModule.view_elems;
                var ViewFunctions = viewModule.view_funcs;

                var TotalView = _.extend( ViewElements, ViewFunctions);
                var View = Marionette.View.extend(
                    TotalView
                );

                if(typeof modelObj != "undefined" && modelObj instanceof Backbone.Model){
                    viewOpt = {
                        model : modelObj
                    };
                }
                return (new View(viewOpt));
            }

        };

        return (new ViewBuilder());

    }();

})();
