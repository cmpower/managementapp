(function(){
    "use strict";

    var ViewLayout = require('../layout/view_layout.js');
    var ViewInitializer = require('./view_initializer');
    var AppOnStart = require('../modules/app_on_start_module.js');
    //--------------------------------------------------------------------------

    var ViewComponents = function(){
        ViewLayout.initRegionWithLayout();
        AppOnStart.init();
    };

    ViewComponents.prototype = {

        'landing': function(){
            ViewLayout.renderAllViewsOnLayout(
                ViewInitializer.headerView(),
                ViewInitializer.footerView(),
                ViewInitializer.sidebarView(),
                ViewInitializer.landingView()
            );
        },
        'login': function(){
            ViewLayout.renderAllViewsOnLayout(
                ViewInitializer.headerView(),
                ViewInitializer.footerView(),
                ViewInitializer.sidebarView(),
                ViewInitializer.loginView()
            );
        },
		'deviceCalendar': function(){
			 ViewLayout.renderAllViewsOnLayout(
                 ViewInitializer.headerView(),
                 ViewInitializer.footerView(),
                 ViewInitializer.sidebarView(),
				 ViewInitializer.deviceCalendarView() 
			 );  
		},
		'appliance': function(){
			 ViewLayout.renderAllViewsOnLayout(
                 ViewInitializer.headerView(),
                 ViewInitializer.footerView(),
                 ViewInitializer.sidebarView(),
				 ViewInitializer.applianceView() 
			 );  
		},
		'about': function(){
			 ViewLayout.renderAllViewsOnLayout(
                 ViewInitializer.headerView(),
                 ViewInitializer.footerView(),
                 ViewInitializer.sidebarView(),
				 ViewInitializer.aboutView() 
			 );  
		},
		'registration': function(){
			 ViewLayout.renderViewOnLayout(
				 ViewInitializer.registrationView() 
			 );  
		},
		'applianceSettings': function(){
			 ViewLayout.renderViewOnLayout( 
				 ViewInitializer.applianceSettingsView() 
			 );  
		},
		'settings': function(){
			 ViewLayout.renderViewOnLayout( 
				 ViewInitializer.settingsView() 
			 );  
		},
		'forgotPass': function(){
			 ViewLayout.renderViewOnLayout( 
				 ViewInitializer.forgotPassView() 
			 );  
		},
		'changePass': function(){
			 ViewLayout.renderViewOnLayout( 
				 ViewInitializer.changePassView() 
			 );  
		},
		'settingChangePass': function(){
			 ViewLayout.renderViewOnLayout( 
				 ViewInitializer.settingChangePassView() 
			 );  
		},
		'globalSettings': function(){
			 ViewLayout.renderViewOnLayout( 
				 ViewInitializer.globalSettingsView() 
			 );  
		},
		'userPermission': function(){
			 ViewLayout.renderViewOnLayout( 
				 ViewInitializer.userPermissionView() 
			 );  
		},
		'userApproval': function(){
			 ViewLayout.renderViewOnLayout( 
				 ViewInitializer.userApprovalView() 
			 );  
		},
		'userRemoval': function(){
			 ViewLayout.renderViewOnLayout( 
				 ViewInitializer.userRemovalView() 
			 );  
		}/*Prepend_View_Component_Object*/
    };

    module.exports = function(){
        return (new ViewComponents());
    }();

})();
