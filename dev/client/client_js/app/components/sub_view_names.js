(function(){
    "use strict";
    // TreeView

    module.exports = function(){
        var regions = {
            // sub_view_name : element_name

			'branch' : 'getBranchView',
			'calendar' : 'getCalendarView',
			'holiday' : 'getHolidayView',
			'weekly' : 'getWeeklyView',
			'holiday_list' : 'getHolidayListView',
			'add_holiday' : 'getAddHolidayView',
			'appliance_list' : 'getApplianceListView',
			'add_appliance' : 'getAddApplianceView',
			'user_access_list' : 'getUserAccessListView',
			'user_request_list' : 'getUserRequestListView',
			'user_permission_invite' : 'getUserPermissionInviteView'/*Prepend_Sub_View_Name*/

        };

        return (regions);
    }();

})();