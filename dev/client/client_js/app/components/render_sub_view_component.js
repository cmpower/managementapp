"use strict";

(function () {
    "use strict";

    var renderComponent = function renderComponent() {};

    renderComponent.prototype = {

        renderSubView: function renderSubView(region, view_element, data) {
            var ViewElements = require("./view/view_elements.js");
            var view = ViewElements[view_element](data);

            var ViewInitializer = require("./view_initializer");
            var viewRegion = ViewInitializer.CURRENT_CONTENT_VIEW.getRegion(region);
            viewRegion.reset();
            viewRegion.show(view);

            return view;
        },
        renderChildrenSubViews: function renderChildrenSubViews(viewRegion, view_element, data) {
            var ViewElements = require("./view/view_elements.js");
            var view = ViewElements[view_element](data);
            viewRegion.reset();
            viewRegion.show(view);

            return view;
        }

    };

    module.exports = (function () {
        return new renderComponent();
    })();
})();