(function(){
    "use strict";

    var ViewElements = require("./view/view_elements.js");
    
    var ViewInitializer = function(){
        this.CURRENT_HEADER_VIEW = "";
        this.CURRENT_FOOTER_VIEW = "";
        this.CURRENT_SIDEBAR_VIEW = "";
        this.CURRENT_CONTENT_VIEW = "";
    };

    ViewInitializer.prototype = {

        //---------------------
        'headerView': function(){
            this.CURRENT_HEADER_VIEW = ViewElements.getHeaderView();
            return this.CURRENT_HEADER_VIEW;
        },
        //---------------------
        'footerView': function(){
            this.CURRENT_FOOTER_VIEW = ViewElements.getFooterView();
            return this.CURRENT_FOOTER_VIEW;
        },
        //---------------------
        'sidebarView': function(){
            this.CURRENT_SIDEBAR_VIEW = ViewElements.getSidebarView();
            return this.CURRENT_SIDEBAR_VIEW;
        },
        //---------------------
        'landingView': function(){
            this.CURRENT_CONTENT_VIEW = ViewElements.getLandingPageView();
            return this.CURRENT_CONTENT_VIEW;
        },
        'loginView': function(){
            this.CURRENT_CONTENT_VIEW = ViewElements.getLoginView();
            return this.CURRENT_CONTENT_VIEW;
        },
		'deviceCalendarView': function(){
			 this.CURRENT_CONTENT_VIEW = ViewElements.getDeviceCalendarView();
			 return this.CURRENT_CONTENT_VIEW;
		},
		'applianceView': function(){
			 this.CURRENT_CONTENT_VIEW = ViewElements.getApplianceView();
			 return this.CURRENT_CONTENT_VIEW;
		},
		'aboutView': function(){
			 this.CURRENT_CONTENT_VIEW = ViewElements.getAboutView();
			 return this.CURRENT_CONTENT_VIEW;
		},
		'registrationView': function(){
			 this.CURRENT_CONTENT_VIEW = ViewElements.getRegistrationView();
			 return this.CURRENT_CONTENT_VIEW;
		},
		'applianceSettingsView': function(){
			 this.CURRENT_CONTENT_VIEW = ViewElements.getApplianceSettingsView();
			 return this.CURRENT_CONTENT_VIEW;
		},
		'settingsView': function(){
			 this.CURRENT_CONTENT_VIEW = ViewElements.getSettingsView();
			 return this.CURRENT_CONTENT_VIEW;
		},
		'forgotPassView': function(){
			 this.CURRENT_CONTENT_VIEW = ViewElements.getForgotPassView();
			 return this.CURRENT_CONTENT_VIEW;
		},
		'changePassView': function(){
			 this.CURRENT_CONTENT_VIEW = ViewElements.getChangePassView();
			 return this.CURRENT_CONTENT_VIEW;
		},
		'settingChangePassView': function(){
			 this.CURRENT_CONTENT_VIEW = ViewElements.getSettingChangePassView();
			 return this.CURRENT_CONTENT_VIEW;
		},
		'globalSettingsView': function(){
			 this.CURRENT_CONTENT_VIEW = ViewElements.getGlobalSettingsView();
			 return this.CURRENT_CONTENT_VIEW;
		},
		'userPermissionView': function(){
			 this.CURRENT_CONTENT_VIEW = ViewElements.getUserPermissionView();
			 return this.CURRENT_CONTENT_VIEW;
		},
		'userApprovalView': function(){
			 this.CURRENT_CONTENT_VIEW = ViewElements.getUserApprovalView();
			 return this.CURRENT_CONTENT_VIEW;
		},
		'userRemovalView': function(){
			 this.CURRENT_CONTENT_VIEW = ViewElements.getUserRemovalView();
			 return this.CURRENT_CONTENT_VIEW;
		}/*Prepend_View_Initializer_Object*/
        //---------------------
    };

    module.exports = function(){
        return (new ViewInitializer());
    }();

})();
