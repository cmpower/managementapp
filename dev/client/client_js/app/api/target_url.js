(function(){
    "use strict";

    module.exports = function(){

        var Target_Url_List = {

            'klario_in'                         : "https://www.klario.in",
            'klario_net'                        : "https://www.klario.net"

        };

        return Target_Url_List;
    }();
})();