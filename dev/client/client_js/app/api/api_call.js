'use strict';

(function () {
    "use strict";

    var Backbone = require('backbone');
    var response_handler = require('../response_handler/response_handler');
    var ModelConfig = require('../models/model_config');

    var ApiCalls = function ApiCalls(configName) {

        this.config = ModelConfig[configName];

        this.syncType = {
            'POST': "create",
            'GET': "read"
        };

        this.responseHandler = response_handler[this.config.responseHandler];
    };

    ApiCalls.prototype = {

        makeCall: function makeCall(modelObj) {
            var that = this;
            var method = this.config.method;

            var func;
            var queries = this.responseHandler.query;
            var commands = this.responseHandler.command;

            var successQueries = this.config.success_queries;
            var errorQueries = this.config.error_queries;
            var successCommands = this.config.success_commands;
            var errorCommands = this.config.error_commands;

            var successFunction = function successFunction(response) {

                for (var i = 0; i < successQueries.length; i++) {
                    func = queries.success[successQueries[i]];
                    func(response);
                }
                for (var k = 0; k < successCommands.length; k++) {
                    func = commands.success[successCommands[k]];
                    func(response);
                }
            };
            var errorFunction = function errorFunction(response) {

                for (var i = 0; i < errorQueries.length; i++) {
                    func = queries.error[errorQueries[i]];
                    func(response);
                }
                for (var k = 0; k < errorCommands.length; k++) {
                    func = commands.error[errorCommands[k]];
                    func(response);
                }
            };

            var serverCall = Backbone.sync(that.syncType[method], modelObj, {
                headers: that.config.header || {},
                success: function success(response) {
                    //that.config.token = serverCall.getResponseHeader("x-csrf");
                    successFunction(response);
                },
                error: function error(response) {
                    errorFunction(response);
                }
            });
        }
    };

    module.exports = function (configName) {
        return new ApiCalls(configName);
    };
})();