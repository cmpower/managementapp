(function(){
    "use strict";

    //var targetUrl = "https://www.klario.in/";
    var Local_Storage = require('../local_storage/local_storage');
    var Local_Storage_Names = require('../local_storage/local_storage_list');

    var appApiUrl = function(){

        this.urlList = {
            'login'                         	    : '/client/user/login',
            'logout'						 	    : '/client/user/logout',
            'appliance_list'					    : '/client/user/appliance/list',
            'appliance_status_change'			    : '/client/appliance/status/change',
            'push_register'						    : '/notification/setting/add',
            'new_register'						    : '/client/user/create',
            'schedule_calendar'					    : '/client/appliance/schedule/update',
            'calendar_schedule_list'			    : '/client/appliance/schedule/list',
            'add_appliance'						    : '/client/appliance/add',
            'appliance_onboarding_update'		    : '/client/appliance/onboarding/update',
			'get_appliance_wifi_settings'		    : '/client/appliance/wifi/setting/get',
			'forgot_password'					    : '/client/user/password/forgot',
			'change_password'					    : '/client/user/password/change',
			'setting_change_password'			    : '/client/user/password/change',
			'update_custom_schedule'			    : '/client/appliance/schedule/list',
			'update_weekly_schedule'			    : '/client/appliance/schedule/list',
			'update_holiday_schedule'			    : '/client/appliance/schedule/list',
			'get_appliance_notification_setting'    : '/client/user/appliance/setting/show',
			'set_appliance_notification_setting'    : '/client/user/appliance/setting/change',
			'user_request_list'					    : '/client/user/appliance/permission/list',
			'user_access_list'					    : '/client/appliance/subowner/list',
			'approve_access'					    : '/client/user/appliance/permission/approve',
			'reject_access'						    : '/client/user/appliance/permission/reject',
			'revoke_access'						    : '/client/user/appliance/permission/revoke',
			'remove_appliance_owner'			    : '/admin/appliance/owners/remove',
			'user_appliance_permission_invite'	    : '/client/user/appliance/permission/invite'/*AppendApiUrl*/
        };
    };

    appApiUrl.prototype = {

        getApiUrl: function(url){
            var target_url = Local_Storage.retrieve(Local_Storage_Names.targetUrl);
            return (target_url+this.urlList[url]);
        }

    };

    module.exports = function(){
        return (new appApiUrl());
    }();

})();