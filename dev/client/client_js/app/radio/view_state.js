(function(){
    "use strict";

    var Radio = require("backbone.radio");
    var Marionette = require('backbone.marionette');
    //----------------------------------
    var ViewStateManager = require("../state_manager/view_state_manager");
    //----------------------------------

    var VIEW_STATE = Radio.channel("view_state");

    var ViewStateHandler = Marionette.Object.extend({

        channelName: "view_state",

        radioRequests: {},

        radioEvents: {
            "view:state:landing"    : "landingState",
            "view:state:login"      : "loginState",
            "view:state:logout"     : "logoutState"
        },

        initialize: function() {},

        landingState: function(){
             ViewStateManager.HeaderViewState.landing();
             ViewStateManager.FooterViewState.landing();
             ViewStateManager.SidebarViewState.landing();
             ViewStateManager.ContentViewState.landing();
        },

        loginState: function(){
            ViewStateManager.HeaderViewState.login();
            ViewStateManager.FooterViewState.login();
            ViewStateManager.SidebarViewState.login();
            ViewStateManager.ContentViewState.login();
        },

        logoutState: function(){
             ViewStateManager.HeaderViewState.logout();
             ViewStateManager.FooterViewState.logout();
             ViewStateManager.SidebarViewState.logout();
             ViewStateManager.ContentViewState.logout();
        }

    });

    new ViewStateHandler();

    module.exports = VIEW_STATE;

})();