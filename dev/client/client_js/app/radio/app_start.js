(function(){
    "use strict";

    var Radio = require("backbone.radio");
    var Marionette = require('backbone.marionette');
    //----------------------------------
    var load_template_module = require("../modules/tpl_loader_module.js");
    var marionetteApp = require("../marionette_app.js");
    //----------------------------------

    var APP_START = Radio.channel("app_start");

    var AppStartHandler = Marionette.Object.extend({

        channelName: "app_start",

        radioRequests: {},

        radioEvents: {
            "app:start:core" : "startCoreApp",
            "app:start:marionette" : "startMarionetteApp"
        },

        startCoreApp: function(data){
            console.log("LOAD TEMPLATE");
            load_template_module.loadTemplates();
        },

        startMarionetteApp: function(data){
            console.log("START MARIONETTE APP");
            marionetteApp.start();
        }

    });

    new AppStartHandler();

    module.exports = APP_START;

})();