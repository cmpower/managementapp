(function(){
    "use strict";

    var Radio = require("backbone.radio");
    var Marionette = require('backbone.marionette');
    //----------------------------------
    //----------------------------------

    var APP_STATE = Radio.channel("app_state");

    var AppStateHandler = Marionette.Object.extend({

        channelName: "app_state",

        radioRequests: {
            "app:state:true"    : "stateTrue",
            "app:state:false"   : "stateFalse",
            "app:state:get"     : "sendAppState"
        },

        radioEvents: {},

        initialize: function() {
            this.appState = true;
        },

        stateTrue: function(){
            this.appState = true;
        },

        stateFalse: function(){
            this.appState = true;
        },

        sendAppState: function(){
            return (this.appState);
        }

    });

    new AppStateHandler();

    module.exports = APP_STATE;

})();