"use strict";

(function () {

    var $ = require('jquery');

    var SlideToggle = function SlideToggle() {};
    SlideToggle.prototype = {

        initSlideEffect: function initSlideEffect(toggleClassName) {
            var startX = null;
            var isChecked = null;
            var clientX;

            function onTouchStart(event) {
                //console.log("Start");
                var touchEvent = event.originalEvent;
                clientX = touchEvent.touches[0].clientX;
                startX = clientX;
                isChecked = $($(event.currentTarget).children().children("input")[0]).is(":checked");
            }
            function onTouchEnd(event) {
                //console.log("end");

                function swipeLeft() {
                    if (isChecked) {
                        //$($(event.currentTarget).parent().siblings("input")[0]).prop("checked", false).trigger("change");
                        $($(event.currentTarget).children().children("input")[0]).prop("checked", false).trigger("change");
                    }
                }

                function swipeRight() {
                    if (!isChecked) {
                        //$($(event.currentTarget).parent().siblings("input")[0]).prop("checked", true).trigger("change");
                        $($(event.currentTarget).children().children("input")[0]).prop("checked", true).trigger("change");
                    }
                }

                if (clientX < startX) {
                    swipeLeft();
                }
                if (clientX > startX) {
                    swipeRight();
                }

                startX = null;
                isChecked = null;
            }
            function onTouchMove(event) {
                var touchEvent = event.originalEvent;
                //console.log(touchEvent.touches[0].clientX);
                //console.log(touchEvent.touches[0].clientY);
                //console.log("-------------------------------");

                clientX = touchEvent.touches[0].clientX;
                function swipeLeft() {
                    $($(event.currentTarget).children().children("input")[0]).prop("checked", false);
                }

                function swipeRight() {
                    $($(event.currentTarget).children().children("input")[0]).prop("checked", true);
                }

                if (clientX < startX) {
                    swipeLeft();
                }
                if (clientX > startX) {
                    swipeRight();
                }
            }

            $("."+toggleClassName).off("touchstart", onTouchStart);
            $("."+toggleClassName).off("touchend", onTouchEnd);
            $("."+toggleClassName).off("touchmove", onTouchMove);

            $("."+toggleClassName).on("touchstart", onTouchStart);
            $("."+toggleClassName).on("touchend", onTouchEnd);
            $("."+toggleClassName).on("touchmove", onTouchMove);
        }
    };

    module.exports = (function () {
        return new SlideToggle();
    })();
})();