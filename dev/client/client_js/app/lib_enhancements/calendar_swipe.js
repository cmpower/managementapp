"use strict";
(function () {

    var $ = require('jquery');

    var SwipeCalendar = function SwipeCalendar() {};
    SwipeCalendar.prototype = {

        init: function(calendarDivId) {
            var startX = null;
            var isChecked = null;
            var clientX;

            function onTouchStart(event) {
                //console.log("Start");
                var touchEvent = event.originalEvent;
                clientX = touchEvent.touches[0].clientX;
                startX = clientX;
            }
            function onTouchEnd(event) {
                //console.log("end");
                var dragDiff;
                function swipeLeft() {
                    $("th.next:visible").trigger("click");
                }

                function swipeRight() {
                    $("th.prev:visible").trigger("click");
                }

                dragDiff = clientX-startX;

                if (dragDiff < -100) {
                    swipeLeft();
                }
                if (dragDiff > 100) {
                    swipeRight();
                }

                startX = null;
                isChecked = null;
            }
            function onTouchMove(event) {
                var touchEvent = event.originalEvent;
                //console.log(touchEvent.touches[0].clientX);
                //console.log(touchEvent.touches[0].clientY);
                //console.log("-------------------------------");

                clientX = touchEvent.touches[0].clientX;
                /*function swipeLeft() {

                }

                function swipeRight() {

                }

                if (clientX < startX) {
                    swipeLeft();
                }
                if (clientX > startX) {
                    swipeRight();
                }*/
            }

            $("#"+calendarDivId).off("touchstart", onTouchStart);
            $("#"+calendarDivId).off("touchend", onTouchEnd);
            $("#"+calendarDivId).off("touchmove", onTouchMove);

            $("#"+calendarDivId).on("touchstart", onTouchStart);
            $("#"+calendarDivId).on("touchend", onTouchEnd);
            $("#"+calendarDivId).on("touchmove", onTouchMove);
        }
    };

    module.exports = (function () {
        return new SwipeCalendar();
    })();
})();