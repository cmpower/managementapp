(function(){
    "use strict";

    var $ = require('jquery');
    var Marionette = require('backbone.marionette');

    window.Behaviors.MaterializeBehaviour = Marionette.Behavior.extend({

        defaults : {
            "message" : "",
            "elements" : []
        },

        ui: {

        },

        onDomRefresh : function(){
            $.material.init();
        }


    });

})();