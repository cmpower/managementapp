(function(){
    "use strict";

    var Marionette = require('backbone.marionette');
    var Ripples = require('../utils/ripples');

    window.Behaviors.RippleBehaviour = Marionette.Behavior.extend({

        defaults : {
            "message" : "",
            "elements" : []
        },

        ui: {
            button: '.button'
        },

        onShow : function(){
            Ripples();
        }


    });

})();