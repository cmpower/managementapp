(function(){
    "use strict";

    var App_State = require("../radio/app_state");
    var View_State = require("../radio/view_state");

    var AppStateChangeModule = function(){

    };

    AppStateChangeModule.prototype = {

        //===========================================

        loginApp: function loginApp(userInfo) {
            var Local_Storage = require('../local_storage/local_storage');
            var Local_Storage_Names = require('../local_storage/local_storage_list');

            var Push_Notification = require('../push/push_notification');
            var Service_Workers = require('../service_workers/service_workers');

            App_State.request("app:state:true");
            View_State.trigger("view:state:login");

            Local_Storage.store(Local_Storage_Names.loggedIn, true); // User Logged in or not

            Push_Notification.registerPush();
            //Service_Workers.registerBrowserPushNotification();
        },
        logoutApp: function logoutApp() {
            var Local_Storage = require('../local_storage/local_storage');
            var Local_Storage_Names = require('../local_storage/local_storage_list');
            var ModelInit = require('../models/model_initiator');
            var ModelConfigNames = require('../models/model_config_names');
            var StoreManager = require('../storage/store_manager');
            var storeNames = require('../storage/store_names');
            var Push_Notification = require('../push/push_notification');
            var Service_Workers = require('../service_workers/service_workers');
            var userInfo = StoreManager.getStoreData(storeNames.userInfo);
            var model = ModelInit(ModelConfigNames.logoutModel);
            var modelData = {
                "userId": typeof userInfo.userId == "undefined" ? "" : userInfo.userId,
                "loginVerificationToken": typeof userInfo.loginVerificationToken == "undefined" ? "" : userInfo.loginVerificationToken
            };

            App_State.request("app:state:false");
            View_State.trigger("view:state:logout");

            StoreManager.clearStorage();
            var remember = Local_Storage.retrieve(Local_Storage_Names.rememberMe);
            if(remember == "true"){
                Local_Storage.clearStoreItems([
                    Local_Storage_Names.loggedIn
                ]);
            }else{
                Local_Storage.clearStoreItems([
                    Local_Storage_Names.loggedIn,
                    Local_Storage_Names.rememberMe,
                    Local_Storage_Names.userName,
                    Local_Storage_Names.userPassword
                ]);
            }


            Push_Notification.unRegisterPush();
            //Service_Workers.unSubscribe();

            model.setModelData(modelData);
            model.sendServerCall();
        },
        refreshApp: function refreshApp() {
            var StoreManager = require('../storage/store_manager');
            StoreManager.clearStorage();

            var AppActivity = require('../modules/app_activity_module');
            AppActivity.dummyActivity();
        }

    };

    module.exports = (function () {
        return new AppStateChangeModule();
    })();
})();