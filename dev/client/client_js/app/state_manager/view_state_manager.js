(function(){
    "use strict";

    var $ = require("jquery");

    var ViewStateManager = function(){

    };

    ViewStateManager.prototype = {

        'HeaderViewState' : {
            landing : function(){
                $(".login-state").hide();
                $(".logout-state").hide();
                $(".landing-state").show();
            },
            login : function(){
                $(".logout-state").hide();
                $(".landing-state").hide();
                $(".login-state").show();
            },
            logout : function(){
                $(".login-state").hide();
                $(".landing-state").hide();
                $(".logout-state").show();
            }
        },

        'FooterViewState' : {
            landing : function(){

            },
            login : function(){

            },
            logout : function(){

            }
        },

        'SidebarViewState' : {
            landing : function(){

            },
            login : function(){

            },
            logout : function(){

            }
        },

        'ContentViewState' : {
            landing : function(){

            },
            login : function(){

            },
            logout : function(){

            }
        }

    };

    module.exports = function(){
        return (new ViewStateManager());
    }();

})();
