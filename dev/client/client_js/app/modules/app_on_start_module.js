'use strict';

(function () {
    "use strict";

    var $ = require('jquery');
    var Marionette = require('backbone.marionette');
    var pace = require('../../lib/pace');
    var Service_Workers = require('../service_workers/service_workers');

    var App_On_Start = function App_On_Start() {
        pace.start();
    };

    App_On_Start.prototype = {

        init: function init() {

            /*if(WebApp) {
                Service_Workers.init();
            }*/

            Marionette.Behaviors.behaviorsLookup = function () {
                return window.Behaviors;
            };

            $.material.init();
        }
    };

    module.exports = (function () {
        return new App_On_Start();
    })();
})();