(function(){
    "use strict";

    var $ = require('jquery');
    var _ = require('underscore');
    var Constants = require("../constants/constants");

    var tpl = function(){};
    tpl.prototype = {

        loadTemplates : function(){
            var that = this;
            this.templates = [];
            var templateData;

            var loadTemplate = function(){

                $.get(Constants.TEMPLATE_PATH , function(data){
                    templateData = $(data).children();
                    for(var i=0; i<templateData.length; i++){
                        that.templates[templateData[i].id] = _.unescape(templateData[i].outerHTML);
                    }
                    var App_Start = require("../radio/app_start");
                    App_Start.trigger("app:start:marionette");
                });
            };

            loadTemplate();

        },
        getViewTemplate : function(){

            return (this.templates[arguments[0]]);

        }
    };

    module.exports = function(){
        return (new tpl());
    }();


})();

