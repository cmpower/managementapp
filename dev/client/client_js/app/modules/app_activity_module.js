(function(){

    var AppRouter = require("../router/app_router.js");
    var RouteConstants = require("../constants/route_constants");
    var Local_Storage = require('../local_storage/local_storage');
    var Local_Storage_Names = require('../local_storage/local_storage_list');

    var AppActivity = function(){
        this.appRouter = AppRouter.getRouter();
    };

    AppActivity.prototype = {

        'AppSession' : function(){
            //var loggedIn = Local_Storage.retrieve("loggedIn");

            //if(loggedIn == null || loggedIn == false){
                this.loginActivity();
            //}else{
            //    this.loggedInActivity();
            //}
        },

        //===========================================================================

        'loggedInActivity' : function(){
            var AppStateChange =require('../state_manager/app_state_manager');
            AppStateChange.restoreAppInState();
            this.applianceActivity();
        },
        'dummyActivity' : function(){
            this.appRouter.navigate("#a", RouteConstants.getRouteConstant());
        },
        //===========================================================================
        'landingPageActivity' : function(){
            this.appRouter.navigate("#landing", RouteConstants.getRouteConstant());
        },
        'loginActivity' : function(){
            this.appRouter.navigate("#login", RouteConstants.getRouteConstant());
        },
        'deviceCalendarActivity': function(){
            this.appRouter.navigate('#device-calendar', RouteConstants.getRouteConstant());
        },
        'applianceActivity': function(){
            this.appRouter.navigate('#appliance', RouteConstants.getRouteConstant());
        },
        'aboutActivity': function(){
            this.appRouter.navigate('#about', RouteConstants.getRouteConstant());
        },
        'registrationActivity': function(){
            this.appRouter.navigate('#registration', RouteConstants.getRouteConstant());
        },
        'applianceSettingsActivity': function(){
            this.appRouter.navigate('#appliance-settings', RouteConstants.getRouteConstant());
        },
        'settingsActivity': function(){
            this.appRouter.navigate('#settings', RouteConstants.getRouteConstant());
        },
        'forgotPassActivity': function(){
            this.appRouter.navigate('#forgot-pass', RouteConstants.getRouteConstant());
        },
        'changePassActivity': function(){
            this.appRouter.navigate('#change-pass', RouteConstants.getRouteConstant());
        },
        'settingChangePassActivity': function(){
            this.appRouter.navigate('#setting-change-pass', RouteConstants.getRouteConstant());
        },
		'globalSettingsActivity': function(){
			 this.appRouter.navigate('#global-settings', RouteConstants.getRouteConstant()); 
		},
		'userPermissionActivity': function(){
			 this.appRouter.navigate('#user-permission', RouteConstants.getRouteConstant()); 
		},
		'userApprovalActivity': function(){
			 this.appRouter.navigate('#user-approval', RouteConstants.getRouteConstant()); 
		},
		'userRemovalActivity': function(){
			 this.appRouter.navigate('#user-removal', RouteConstants.getRouteConstant()); 
		}/*Prepend_Activity_Object*/

    };

    module.exports = function(){
        return (new AppActivity());
    }();

})();