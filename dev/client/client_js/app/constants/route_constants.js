(function(){
    "use strict";

    var _ = require('underscore');

    var RouteConstants = function(){

    };

    RouteConstants.prototype = {
        getRouteConstant: function(){
            return ({trigger: true, replace: false});
        },
        getNonRouteConstant: function(){
            return ({trigger: true, replace: true});
        },

        setRouteData: function(data){
            this.routeData = data;
        },
        getRouteData: function(){
            return _.clone(this.routeData);
        }
    };

    module.exports = function(){
        return (new RouteConstants());
    }();

})();
