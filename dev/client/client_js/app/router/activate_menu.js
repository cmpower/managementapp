(function(){
    "use strict";
    var $ = require("jquery");

    var activateMenu = function(elemList){
        $(".menuItems").removeClass("active");
        for(var i = 0;i < elemList.length; i++){
            $("#"+elemList[i]).addClass("active");
        }

    };

    module.exports = function(){
        return (activateMenu);
    }();

})();