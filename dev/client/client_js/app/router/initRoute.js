"use strict";

(function () {
    "use strict";

    var initRoute = function initRoute() {
        console.log("APP ROUTER INITIALIZED");
    };

    module.exports = (function () {
        return initRoute;
    })();
})();