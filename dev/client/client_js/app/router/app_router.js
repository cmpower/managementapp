"use strict";

(function () {
    "use strict";

    var Marionette = require('backbone.marionette');
    var routes = require("./routes.js");
    var controller = require("./route_controller.js");
    var initRoute = require("./initRoute.js");
    var onRoute = require("./onRoute.js");

    var AppRouter = function AppRouter() {
        var Router = Marionette.AppRouter.extend({
            controller: controller.getRouterControllerList(),
            appRoutes: routes,
            initialize: initRoute,
            onRoute: onRoute
        });
        this.appRouter = new Router();
    };

    AppRouter.prototype = {
        getRouter: function getRouter() {
            return this.appRouter;
        }
    };

    module.exports = (function () {
        return new AppRouter();
    })();
})();