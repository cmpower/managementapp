(function(){
    "use strict";

    var routes = {

        ''                              : 'loginRoute',
        'landing'                       : 'landingPageRoute',
        'login'                         : 'loginRoute',
		'device-calendar' 						: 'deviceCalendarRoute',
		'appliance' 						: 'applianceRoute',
		'about' 						: 'aboutRoute',
		'registration' 						: 'registrationRoute',
		'appliance-settings' 						: 'applianceSettingsRoute',
		'settings' 						: 'settingsRoute',
		'forgot-pass' 						: 'forgotPassRoute',
		'change-pass' 						: 'changePassRoute',
		'setting-change-pass' 						: 'settingChangePassRoute',
		'global-settings' 						: 'globalSettingsRoute',
		'user-permission' 						: 'userPermissionRoute',
		'user-approval' 						: 'userApprovalRoute',
		'user-removal' 						: 'userRemovalRoute'/*Prepend_Route_Object*/

    };

    module.exports = function(){
        return (routes);
    }();

})();