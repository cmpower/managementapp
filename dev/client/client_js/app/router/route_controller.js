(function(){
    "use strict";
    var _ = require('underscore');
    var ViewComponents = require('../components/render_view_components');
    var RouteConstants = require('../constants/route_constants');
    var AppStateChange =require('../state_manager/app_state_manager');
    var App_State =require('../radio/app_state');
    //--------------------------------------------------------------------------

    var RouteController = function(){

    };

    RouteController.prototype = {

        getOutOfStateControllers: function(){
            var outStates = {

                'loginRoute': function(){
                    ViewComponents.login();
                },
                'landingPageRoute': function(){
                    ViewComponents.landing();
                },
                'aboutRoute': function(){
                    ViewComponents.about();
                },
                'registrationRoute': function(){
                    ViewComponents.registration();
                },
                'dummyRoute': function(){

                },
                'forgotPassRoute': function(){
                    ViewComponents.forgotPass();
                },
                'globalSettingsRoute': function(){
                    ViewComponents.globalSettings();
                }/*Prepend_Out_State_Route_Controller_Object*/

            };

            return outStates;
        },
        getInStateControllers: function(){
            var inState = {

                /*'exampleRoute': function(){
                 var AppInState = App_State.request("app:state:get");
                 (AppInState)? ViewComponents.example() : AppStateChange.refreshApp();
                 }*/

                'deviceCalendarRoute': function(){
                    var AppInState = App_State.request("app:state:get");
                    (AppInState)? ViewComponents.deviceCalendar() : AppStateChange.refreshApp();
                },
                'applianceRoute': function(){
                    var AppInState = App_State.request("app:state:get");
                    (AppInState)? ViewComponents.appliance() : AppStateChange.refreshApp();
                },
                'applianceSettingsRoute': function(){
                    var AppInState = App_State.request("app:state:get");
                    (AppInState)? ViewComponents.applianceSettings() : AppStateChange.refreshApp();
                },
                'settingsRoute': function(){
                    var AppInState = App_State.request("app:state:get");
                    (AppInState)? ViewComponents.settings() : AppStateChange.refreshApp();
                },
                'changePassRoute': function(){
                    var AppInState = App_State.request("app:state:get");
                    (AppInState)? ViewComponents.changePass() : AppStateChange.refreshApp();
                },
                'settingChangePassRoute': function(){
                    var AppInState = App_State.request("app:state:get");
                    (AppInState)? ViewComponents.settingChangePass() : AppStateChange.refreshApp();
                },
                'userPermissionRoute': function(){
                    var AppInState = App_State.request("app:state:get");
                    (AppInState)? ViewComponents.userPermission() : AppStateChange.refreshApp();
                },
                'userApprovalRoute': function(){
                    var AppInState = App_State.request("app:state:get");
                    (AppInState)? ViewComponents.userApproval() : AppStateChange.refreshApp();
                },
                'userRemovalRoute': function(){
                    var AppInState = App_State.request("app:state:get");
                    (AppInState)? ViewComponents.userRemoval() : AppStateChange.refreshApp();
                }/*Prepend_In_State_Route_Controller_Object*/

            };

            return inState;
        },

        getRouterControllerList : function(){
            var InStateControllers = this.getInStateControllers();
            var OutOfStateControllers = this.getOutOfStateControllers();

            return (_.extend(OutOfStateControllers, InStateControllers));
        }

    };

    module.exports = function(){
        return (new RouteController());
    }();

})();