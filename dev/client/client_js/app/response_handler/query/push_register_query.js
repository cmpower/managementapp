(function() {
    "use strict";

    var storeNames = require('../../storage/store_names');
    var StoreManager = require('../../storage/store_manager');
    var AppStateChange =require('../../state_manager/app_state_manager');

    module.exports = {

        success : {
            store_data: function (response){
                var Local_Storage = require('../../local_storage/local_storage');
                var Local_Storage_Names = require('../../local_storage/local_storage_list');
                Local_Storage.store(Local_Storage_Names.isSubscribed, true);
            },
            clear_data: function (response){},

            app_state_in: function(response){},
            app_state_out: function(response){}
        },
        error : {
            store_data: function (response){
                var Local_Storage = require('../../local_storage/local_storage');
                var Local_Storage_Names = require('../../local_storage/local_storage_list');
                Local_Storage.store(Local_Storage_Names.isSubscribed, false);
            },
            clear_data: function (response){},

            app_state_in: function(response){},
            app_state_out: function(response){}
        }

    };

})();

