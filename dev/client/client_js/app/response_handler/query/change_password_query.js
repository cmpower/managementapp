(function() {
    "use strict";

    var storeNames = require('../../storage/store_names');
    var StoreManager = require('../../storage/store_manager');
    var AppStateChange =require('../../state_manager/app_state_manager');

    module.exports = {

        success : {
            store_data: function (response){},
            clear_data: function (response){},

            app_state_in: function(response){},
            app_state_out: function(response){}
        },
        error : {
            store_data: function (response){},
            clear_data: function (response){},

            app_state_in: function(response){},
            app_state_out: function(response){}
        }

    };

})();

