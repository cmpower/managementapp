(function() {
    "use strict";

    var notification = require('../../utils/notification/notification_module');
    var errorHandler = require('../notification_text_retriever/error_handler');
    var successHandler = require('../notification_text_retriever/success_handler');

    module.exports = {

        success : {
            navigate: function (response) {},
            call_view_function: function (response) {
                var ViewInitializer = require('../../components/view_initializer');
                ViewInitializer.CURRENT_CONTENT_VIEW.updatePendingUserList();
            },
            notification: function(response){
                var successMessage = successHandler.getSuccessText(response);
                notification.success(successMessage);
            }
        },
        error : {
            navigate: function (response) {},
            call_view_function: function (response) {},
            notification: function(response){
                var errMessage = errorHandler.getErrorText(response);
                notification.error(errMessage);
            }
        }

    };

})();

