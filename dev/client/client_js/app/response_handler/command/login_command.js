(function() {
    "use strict";

    var notification = require('../../utils/notification/notification_module');
    var errorHandler = require('../notification_text_retriever/error_handler');
    var successHandler = require('../notification_text_retriever/success_handler');

    module.exports = {

        success : {
            navigate: function (response) {
                var userInfo = response.responseData.response;
                var AppActivity = require('../../modules/app_activity_module');

                if(typeof userInfo.userData.isChangePassword == "undefined" || userInfo.userData.isChangePassword == false){
                    AppActivity.applianceActivity();
                }else{
                    AppActivity.changePassActivity();
                }

            },
            call_view_function: function (response) {},
            notification: function(response){}
        },
        error : {
            navigate: function (response) {},
            call_view_function: function (response) {
                var ViewInitializer = require('../../components/view_initializer');
                ViewInitializer.CURRENT_CONTENT_VIEW.errorLoginFunc();
            },
            notification: function(response){
                var errMessage = errorHandler.getErrorText(response);
                notification.error(errMessage);
            }
        }

    };

})();

