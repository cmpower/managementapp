(function() {
    "use strict";

    var SuccessHandler = function(){

    };

    SuccessHandler.prototype = {

        getSuccessText: function(response){
            var successText = "";
            successText = response.responseData.message;
            return successText;
        }
    };

    module.exports = function(){
        return (new SuccessHandler());
    }();

})();