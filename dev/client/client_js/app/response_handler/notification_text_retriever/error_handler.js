"use strict";

(function () {
    "use strict";

    var AppStateChange = require("../../state_manager/app_state_manager");

    var ErrorHandler = function ErrorHandler() {};

    ErrorHandler.prototype = {

        getErrorText: function getErrorText(response) {
            var errText = "";

            if (typeof response.responseJSON != "undefined") {
                errText = response.responseJSON.responseData.message;
            } else if (response.status == 401) {
                errText = "Session has expired.";
            } else if (response.status == 504 || response.status == 502) {
                errText = "Server is not reachable. Please try again later.";
            } else {
                errText = response.statusText;
            }

            if (response.status == 401) {
                AppStateChange.logoutApp();
            }

            return errText;
        }
    };

    module.exports = (function () {
        return new ErrorHandler();
    })();
})();