(function() {
    "use strict";

    var StoreManager = require('../storage/store_manager');

    var viewResponseHandler = function() {
        StoreManager.init();
    };

    viewResponseHandler.prototype = {

        'login_response' : {
            'command' : require('./command/login_command.js'),
            'query'   : require('./query/login_query.js')
        },
		'logout_response' : {
			 'command' : require( './command/logout_command.js'),
			 'query'   : require( './query/logout_query.js')
		},
		'appliance_list_response' : {
			 'command' : require( './command/appliance_list_command.js'),
			 'query'   : require( './query/appliance_list_query.js')
		},
		'appliance_status_change_response' : {
			 'command' : require( './command/appliance_status_change_command.js'),
			 'query'   : require( './query/appliance_status_change_query.js')
		},
		'push_register_response' : {
			 'command' : require( './command/push_register_command.js'),
			 'query'   : require( './query/push_register_query.js')
		},
		'new_register_response' : {
			 'command' : require( './command/new_register_command.js'),
			 'query'   : require( './query/new_register_query.js')
		},
		'schedule_calendar_response' : {
			 'command' : require( './command/schedule_calendar_command.js'),
			 'query'   : require( './query/schedule_calendar_query.js')
		},
		'calendar_schedule_list_response' : {
			 'command' : require( './command/calendar_schedule_list_command.js'),
			 'query'   : require( './query/calendar_schedule_list_query.js')
		},
		'add_appliance_response' : {
			 'command' : require( './command/add_appliance_command.js'),
			 'query'   : require( './query/add_appliance_query.js')
		},
		'appliance_onboarding_update_response' : {
			'command' : require( './command/appliance_onboarding_update_command.js'),
			'query'   : require( './query/appliance_onboarding_update_query.js')
		},
		'get_appliance_wifi_settings_response' : {
			 'command' : require( './command/get_appliance_wifi_settings_command.js'),
			 'query'   : require( './query/get_appliance_wifi_settings_query.js')
		},
		'forgot_password_response' : {
			 'command' : require( './command/forgot_password_command.js'),
			 'query'   : require( './query/forgot_password_query.js')
		},
		'change_password_response' : {
			 'command' : require( './command/change_password_command.js'),
			 'query'   : require( './query/change_password_query.js')
		},
		'setting_change_password_response' : {
			 'command' : require( './command/setting_change_password_command.js'),
			 'query'   : require( './query/setting_change_password_query.js')
		},
		'update_custom_schedule_response' : {
			 'command' : require( './command/update_custom_schedule_command.js'),
			 'query'   : require( './query/update_custom_schedule_query.js')
		},
		'update_weekly_schedule_response' : {
			 'command' : require( './command/update_weekly_schedule_command.js'),
			 'query'   : require( './query/update_weekly_schedule_query.js')
		},
		'update_holiday_schedule_response' : {
			 'command' : require( './command/update_holiday_schedule_command.js'),
			 'query'   : require( './query/update_holiday_schedule_query.js')
		},
		'get_appliance_notification_setting_response' : {
			 'command' : require( './command/get_appliance_notification_setting_command.js'),
			 'query'   : require( './query/get_appliance_notification_setting_query.js')
		},
		'set_appliance_notification_setting_response' : {
			 'command' : require( './command/set_appliance_notification_setting_command.js'),
			 'query'   : require( './query/set_appliance_notification_setting_query.js')
		},
		'user_request_list_response' : {
			 'command' : require( './command/user_request_list_command.js'),
			 'query'   : require( './query/user_request_list_query.js')
		},
		'user_access_list_response' : {
			 'command' : require( './command/user_access_list_command.js'),
			 'query'   : require( './query/user_access_list_query.js')
		},
		'approve_access_response' : {
			 'command' : require( './command/approve_access_command.js'),
			 'query'   : require( './query/approve_access_query.js')
		},
		'reject_access_response' : {
			 'command' : require( './command/reject_access_command.js'),
			 'query'   : require( './query/reject_access_query.js')
		},
		'revoke_access_response' : {
			 'command' : require( './command/revoke_access_command.js'),
			 'query'   : require( './query/revoke_access_query.js')
		},
		'remove_appliance_owner_response' : {
			 'command' : require( './command/remove_appliance_owner_command.js'),
			 'query'   : require( './query/remove_appliance_owner_query.js')
		},
		'user_appliance_permission_invite_response' : {
			 'command' : require( './command/user_appliance_permission_invite_command.js'),
			 'query'   : require( './query/user_appliance_permission_invite_query.js')
		}/*AppendResponseHandler*/
    };

    module.exports = function() {
        return (new viewResponseHandler());
    }();

})();
