'use strict';
(function () {
    "use strict";

    var storeNames = require('../storage/store_names');
    var StoreManager = require('../storage/store_manager');
    var Local_Storage = require('../local_storage/local_storage');
    var Local_Storage_Names = require('../local_storage/local_storage_list');
    var notification = require('../utils/notification/notification_module');

    var Push_Notification = function Push_Notification() {};

    Push_Notification.prototype = {

        init: function init() {},

        registerPush: function registerPush() {
            if (WebApp) {
                return false;
            }

            this.push = PushNotification.init({
                "android": {
                    "senderID": "241569268727",
                    "icon": "icon",
                    "iconColor": "blue",
                    "sound": "true",
                    "vibrate": "true"
                },
                "ios": {
                    "senderID": "241569268727",
                    "icon": "icon",
                    "iconColor": "blue",
                    "vibrate": "true",
                    "gcmSandbox": "false",
                    "alert": "true",
                    "badge": "true",
                    "sound": "true"
                },
                "windows": {}
            });

            this.push.on('registration', this.registerPushData);

            this.push.on('notification', this.receivePush);

            this.push.on('error', this.errorPush);

            return this.push;
        },

        registerPushData: function registerPushData(data) {
            console.log(data.registrationId);
            var ModelInit = require('../models/model_initiator');
            var ModelConfigNames = require('../models/model_config_names');
            var userInfo = StoreManager.getStoreData(storeNames.userInfo);
            var model = ModelInit(ModelConfigNames.pushRegisterModel);

            model.setModelData({
                "externalId": userInfo.userId,
                "isRegistered": true,
                "notificationDeviceType": device.platform,
                "notificationDeviceId": device.uuid,
                "projectId": "klario-72a3e",
                "registrationId": data.registrationId
            });
            model.sendServerCall();
        },
        unRegisterPush: function unRegisterPush() {
            if (WebApp) {
                return false;
            }

            this.push.unregister(function () {
                Local_Storage.store(Local_Storage_Names.isSubscribed, false);
                console.log('success');
            }, function () {
                Local_Storage.store(Local_Storage_Names.isSubscribed, true);
                console.log('error');
            });
        },

        receivePush: function receivePush(data) {
            console.log(data);
            console.log(data.message);
            //console.log(data.additionalData.get());
            var msg = data.message;
            var jsonStr = window.atob(msg.split(",")[1]).replace("\n", "");
            var jsonObj = JSON.parse(jsonStr);
            var ViewInitializer = require('../components/view_initializer');
            var notiType = jsonObj.notificationType;

            switch (notiType){
                case "user" : notification.info(jsonObj.message);
                    if(typeof  ViewInitializer.CURRENT_CONTENT_VIEW.updatePendingUserList != "undefined"){
                        ViewInitializer.CURRENT_CONTENT_VIEW.updatePendingUserList();
                    }
                    break;
                case "applianceUpdate" : notification.info(jsonObj.message);
                    if(typeof  ViewInitializer.CURRENT_CONTENT_VIEW.refreshApplianceList != "undefined"){
                        ViewInitializer.CURRENT_CONTENT_VIEW.refreshApplianceList();
                    }
                    break;
                case "appliance" : ViewInitializer.CURRENT_CONTENT_VIEW.updateDeviceStatusFromNoti(jsonObj);
                    break;
                case "message" : notification.info(jsonObj.message);
                    break;
                default : notification.warning(jsonObj.message);
                    break;
            }

            // data.title,
            // data.count,
            // data.sound,
            // data.image,
            // data.additionalData
        },

        errorPush: function errorPush(e) {
            Local_Storage.store(Local_Storage_Names.isSubscribed, false);
            notification.error(""+e.message+".  Push notification registration failed.");
            notification.info("Please logout and login again to register for push notification.");
            console.log(e);
            console.log(e.message);
        }
    };

    module.exports = (function () {
        return new Push_Notification();
    })();
})();