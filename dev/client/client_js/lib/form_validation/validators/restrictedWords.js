(function($) {
    FormValidation.Validator.restrictedWords = {
        /**
         * @param {FormValidation.Base} validator The validator plugin instance
         * @param {jQuery} $field The jQuery object represents the field element
         * @param {Object} options The validator options
         * @returns {Boolean}
         */
        validate: function(validator, $field, options) {
            // You can get the field value
            // var value = $field.val();
            //
            // Perform validating
            // ...
            //
            // return true if the field value is valid
            // otherwise return false
            var value = $field.val(),
                words = options.words;
            for(var i = 0; i < words.length; i++){
                if(value == words[i]){
                    return {
                        valid: false,
                        message: ""+options.message+" "+words.toString()+""
                    };
                }
            }
            return true;
        }
    };
}(require("jquery")));