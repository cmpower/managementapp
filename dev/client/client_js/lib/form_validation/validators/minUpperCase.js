(function($) {
    FormValidation.Validator.minUpperCase = {
        /**
         * @param {BootstrapValidator} validator The validator plugin instance
         * @param {jQuery} $field The jQuery object represents the field element
         * @param {Object} options The validator options
         * @returns {boolean}
         */
        validate: function(validator, $field, options) {
            // You can get the field value
            // var value = $field.val();
            //
            // Perform validating
            // ...
            //
            // return true if the field value is valid
            // otherwise return false
            var value = $field.val();
            var upper =   /[A-Z]/g;
            if (value == '') {
                return true;
            }
            return ((value.match(upper) || []).length >= options.min);

        }
    };
}(require("jquery")));