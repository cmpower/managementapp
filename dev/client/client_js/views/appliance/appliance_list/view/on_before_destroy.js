(function(){
    "use strict";
    // ApplianceListView

    module.exports = function(){

        function on_before_destroy(){
            this.clearAllTimeouts();
        }

        return (on_before_destroy);
    }();

})();