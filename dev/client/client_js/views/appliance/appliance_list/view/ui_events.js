(function(){
    "use strict";
    // ApplianceListView

    module.exports = function(){

        var ui_events = {

            "change @ui.deviceSwitch" : "changeDeviceStatus",

            "click @ui.refreshDeviceList" : "refreshDeviceList",

            "click td.extraDetail" : "showTableDropDown",

            "click .calendarIcon" : "getCalendarSchedule",

            "click @ui.deviceSettingsIcon" : "deviceSettings",

            "click .userPermissions" : "showUserPermissions",
            "click .disableSubUserAccess" : "subUserAccessDenied"

        };

        return (ui_events);
    }();

})();