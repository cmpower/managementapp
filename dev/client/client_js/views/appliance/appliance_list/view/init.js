"use strict";

(function () {
    "use strict"
    // ApplianceListView

    ;
    var _ = require('underscore');
    var viewContentLang = require("../../../../app/utils/language/multi_lingual_module.js");
    var languageId = "ApplianceListView",
        templateId = "ApplianceListView";

    var initView = function initView() {
        this.template = _.template(viewContentLang.getCurrentLangViewTemplate(languageId, templateId));
    };

    module.exports = (function () {
        return initView;
    })();
})();