(function(){
    "use strict";
    // ApplianceListView
    var $ = require('jquery');
    var _ = require('underscore');
    var StoreManager = require('../../../../app/storage/store_manager');
    var storeNames = require('../../../../app/storage/store_names');
    var ModelInit = require('../../../../app/models/model_initiator');
    var ModelConfigNames = require('../../../../app/models/model_config_names');
    var Utils = require('../../../../app/utils/utils');
    var dataTable = require('../../../../app/utils/data_table/render_data_table');
    var notification = require('../../../../app/utils/notification/notification_module');
    module.exports = function(){

        var ViewFunctions = {

            initResponseArray: function(){
                this.responseArray = [];
                this.refreshTimeoutColl = {};
            },

            //-----------------------------------
            getDeviceList: function () {
                var model = ModelInit(ModelConfigNames.applianceListModel);
                var userInfo = StoreManager.getStoreData(storeNames.userInfo);
                model.setModelData({
                    "userId": userInfo.userId
                });
                model.sendServerCall();
            },
            refreshDeviceListTimeout: function () {
                notification.warning("Oops! It took longer than anticipated. Appliance list is refreshed now.");
                var model = ModelInit(ModelConfigNames.applianceListModel);
                var userInfo = StoreManager.getStoreData(storeNames.userInfo);
                model.setModelData({
                    "userId": userInfo.userId
                });
                model.sendServerCall();
            },

            refreshDeviceList: function () {
                this.getDeviceList();
            },

            //--------------------------------------

            populateApplianceListSub : function () {
                var deviceList = StoreManager.getStoreData(storeNames.applianceList);
                var userInfo  = StoreManager.getStoreData(storeNames.userInfo);
                var tableData = [], isOn = "";
                var deviceMode = "", inProgressStatus = "",subOwner="",isOwner="";
                var deviceId, deviceStatus, actionStatus;
                var i;

                this.ui.deviceListDiv.show();
                for(i = 0; i < deviceList.length; i++){

                    deviceId = deviceList[i].deviceId;
                    deviceStatus = deviceList[i].deviceStatus;
                    actionStatus = deviceList[i].actionStatus;
                    isOn = (deviceStatus == "on")? "checked":"";
                    deviceMode = "<div class='power_checkbox_div'><input type='checkbox' class='power_checkbox deviceControlCheckBox' id='"+deviceId+"' "+isOn+"><label for='"+deviceId+"'></label></div>";
                    subOwner = '<span class="userPermissions c-teal"  data-device-id="'+deviceId+'"><i class="fa fa-users fa-2x"></i></span>';
                    isOwner = (userInfo.userId == deviceList[i].deviceOwner)?subOwner:'<span class="disableSubUserAccess customGray"><i class="fa fa-users fa-2x"></i></span>';
                    tableData.push({
                        "On/Off"    : deviceMode,
                        "Device"    : ""+deviceList[i].deviceName+" <br> <small>"+deviceList[i].deviceLocationName+"</small>",
                        "Info"      : '<a href="javascript:void(0);"><i class="fa fa-chevron-down f-20 rowExpandTable" aria-hidden="true"></i></a>',
                        "setting"   : '<span class="c-teal" data-device-id="'+deviceId+'"> <i class="fa fa-cog fa-2x" aria-hidden="true"></i></span>',
                        "subOwners"   : isOwner,
                        "calendar"  : '<span class="c-teal" data-device-id="'+deviceId+'" data-device-name="'+deviceList[i].deviceName+'" data-device-loc="'+deviceList[i].deviceLocationName+'"> <i class="fa fa-calendar fa-2x" aria-hidden="true"></i></span>',
                        "DT_RowClass"  : deviceId
                    });
                }

                this.applianceTable = this.ui.deviceList.DataTable({
                    "paging":   false,
                    "ordering": false,
                    "info":     false,
                    responsive: {
                        details: false
                    },
                    destroy:true,
                    aaData:tableData,
                    "dom": "<'well well-sm m-b-10'<'row'<'col-sm-12'f>>>" +
                    "<'well well-sm'<'row'<'col-sm-12'tr>>>",
                    "oLanguage": {
                        "sSearch": "<i class='fa fa-search pull-left'></i>"
                    },
                    "aoColumns": [
                        { data:"On/Off", "sClass": "all text-center" },
                        { data:"Device", "sClass": "all deviceNameCol text-left extraDetail" },
                        { data:"Info", "sClass": "tablet-p mobile-l mobile-p text-center dropDownIconTd extraDetail" },
                        { data:"setting", "sClass": "desktop tablet-l deviceSettingsIcon" },
                        { data:"subOwners", "sClass": "desktop tablet-l" },
                        { data:"calendar", "sClass": "desktop tablet-l calendarIcon" }
                    ],
                    "order": [[1, 'asc']]
                });
              /*  setTimeout( function(){
                    mRefresh.resolve();
                }  , 2000 );*/

                this.initMaterialComponents();
            },

            //--------------------------------------
            showTableDropDown: function(event){
                if(!$(".rowExpandTable").is(":visible")){
                    return false;
                }
                var tr = $(event.currentTarget).closest('tr');
                var row = this.applianceTable.row(tr);

                function format ( d ) {
                    // `d` is the original data object for the row
                    return '<div class="row text-center">'+
                        '<div class="col-xs-4 deviceSettingsIcon c-teal">'+ d.setting+'</div>'+
                        '<div class="col-xs-4">'+ d.subOwners+'</div>'+
                        '<div class="col-xs-4 calendarIcon c-teal">'+ d.calendar+'</div>'+
                        '</div>';
                }

                if ( row.child.isShown() ) {
                    // This row is already open - close it
                    row.child.hide();
                    tr.removeClass('shown');
                    $(event.currentTarget).find('.rowExpandTable').toggleClass('fa-chevron-down fa-chevron-up');
                    $($(event.currentTarget).siblings(".dropDownIconTd")).find('.rowExpandTable').toggleClass('fa-chevron-down fa-chevron-up');
                }
                else {
                    // Open this row
                    row.child( format(row.data()) ).show();
                    tr.addClass('shown');
                    $('head').append('<style>table.dataTable.dtr-inline.collapsed > tbody > tr > td:first-child:before, table.dataTable.dtr-inline.collapsed > tbody > tr > th:first-child:before{content: none !important;}</style>');
                    $(event.currentTarget).find('.rowExpandTable').toggleClass('fa-chevron-down fa-chevron-up');
                    $($(event.currentTarget).siblings(".dropDownIconTd")).find('.rowExpandTable').toggleClass('fa-chevron-down fa-chevron-up');
                }
                Utils.scrollElementToTop(event);
            },
            //--------------------------------------
            initMaterialComponents: function(){
                $.material.init();
            },
            //    ----------------------------------------------
            changeDeviceStatus: function(event){
                var status = ($(event.currentTarget).is(":checked"))? "on" : "off";
                var model = ModelInit(ModelConfigNames.applianceStatusChangeModel);
                var userInfo = StoreManager.getStoreData(storeNames.userInfo);

                this.showInProgressDevice({
                    "deviceId" : event.currentTarget.id,
                    "turnTo" : status
                });

                model.setModelData({
                    "userId" : userInfo.userId,
                    "deviceId" : event.currentTarget.id,
                    "turnTo" : status
                });

                $(event.currentTarget).prop("checked", !($(event.currentTarget).prop("checked")));

                model.sendServerCall();
            },
            showInProgressDevice: function(data){
                var deviceId = data.deviceId,
                    deviceStatus = data.turnTo;
                var tableTd = $("#"+deviceId+"").parent().parent()[0];
                $($("#"+deviceId+"").parent()[0]).hide();
                $("span."+deviceId+"").remove();
                $(tableTd).append("<span class='"+deviceId+"'>"+deviceStatus+"<br><span class='label label-warning in-progress-label'>Pending</span></span>");

                this.refreshTimeoutColl[deviceId] = setTimeout(this.refreshDeviceListTimeout, 60000);
            },
            updateDeviceStatus: function(msg, type, deviceId){
                this.responseArray.push({
                    "message" : msg,
                    "type" : type,
                    "deviceId" : deviceId
                });
                this.showOverlayResponse();
                clearTimeout(this.refreshTimeoutColl[deviceId]);
            },
            //    ----------------------------------------------
            updateDeviceStatusFromNoti: function (data) {
                var dId = data.deviceId,
                    dStatus = data.deviceStatus,
                    dMsg = data.message,
                    msgType = (data.isSuccess)? "success" : "error";

                var onStatus = (dStatus == "on")? true : false;
                $("#"+dId+"").prop("checked", onStatus);

                this.updateDeviceStatus(dMsg, msgType, dId);
            },

            showOverlayResponse: function(){
                var that = this, i;
                var deviceId, msg, type;

                while(this.responseArray.length > 0){
                    i=0;

                    msg = this.responseArray[i].message;
                    type = this.responseArray[i].type;
                    deviceId = this.responseArray[i].deviceId;

                    if(typeof deviceId == "undefined"){
                        that.refreshDeviceList();
                    }else{
                        $("span."+deviceId+"").remove();
                        $($("#"+deviceId+"").parent()[0]).show();
                    }

                    if(type == "error"){
                        notification.device_error(msg);
                    }else{
                        notification.device_success(msg);
                    }

                    this.responseArray.shift();
                }
                //notification.clear();
            },

            //    ----------------------------------------------
            getCalendarSchedule: function(event){
                var deviceId = $($(event.currentTarget).children("span")[0]).attr("data-device-id");
                var deviceName = $($(event.currentTarget).children("span")[0]).attr("data-device-name");
                var deviceLocationName = $($(event.currentTarget).children("span")[0]).attr("data-device-loc");
                var model = ModelInit(ModelConfigNames.calendarScheduleListModel);

                StoreManager.setStoreData(storeNames.chosenDeviceId, deviceId);
                StoreManager.setStoreData(storeNames.chosenDeviceName, {"deviceName":deviceName,"deviceLocationName": deviceLocationName});

                model.setModelData({
                    "deviceId": deviceId
                });
                model.sendServerCall();
            },
            //    ----------------------------------------------
            deviceSettings : function (event) {
                var deviceId = $($(event.currentTarget).children("span")[0]).attr("data-device-id");
                StoreManager.setStoreData(storeNames.currentSelectedDevice,{id:deviceId});
                var AppActivity = require("../../../../app/modules/app_activity_module");
                AppActivity.applianceSettingsActivity();
            },
            //    --------------------------------------------------
            showUserPermissions: function(event){
                var deviceId = $(event.currentTarget).attr("data-device-id");
                StoreManager.setStoreData(storeNames.currentSelectedDevice,{id:deviceId});
                var AppActivity = require("../../../../app/modules/app_activity_module");
                AppActivity.userPermissionActivity();
            },
            subUserAccessDenied: function(){
                notification.warning("Please contact the owner to invite another user.");
            },

        //    --------------------------------------------------------
            clearAllTimeouts: function(){
                for(var deviceId in this.refreshTimeoutColl){
                    clearTimeout(this.refreshTimeoutColl[deviceId]);
                }
            }


        };


        return ViewFunctions;

    }();

})();
