(function(){
    "use strict";
    // View

    module.exports = function(){

        function on_dom_refresh(){
            this.initResponseArray();
            this.getDeviceList();
        }

        return (on_dom_refresh);
    }();

})();