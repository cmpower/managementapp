(function(){
    "use strict";
    // ApplianceListView

    module.exports = function(){
        var ui = {
            "deviceSwitch" : ".deviceControlCheckBox",

            "refreshDeviceList" : "#refreshDeviceList",
            "deviceList" : "#deviceList",
            "deviceListDiv" : "#deviceListDiv",
            "deviceSettingsIcon" : ".deviceSettingsIcon",

            "deviceNotification" : "#deviceNotification"

        };

        return (ui);
    }();

})();