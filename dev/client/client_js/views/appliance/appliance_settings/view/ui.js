(function(){
    "use strict";
    // DeviceSettingsView

    module.exports = function(){
        var ui = {



            "applianceSoftwareVer" : "#applianceSoftwareVer",
            "hostname" : "#hostname",
            "ipAddress" : "#ipAddress",
            "macAddress" : "#macAddress",
            "ssid" : "#ssid",
            "txBytes" : "#txBytes",
            "rxBytes" : "#rxBytes",
            "rssi" : "#rssi",




            "applianceSettingsForm" : "#applianceSettingsForm",
            "applianceName" : "#applianceName",
            "applianceLocation" : "#applianceLocation",


           /* "wifiSsid" : "#wifiSsid",
            "wifiUser" : "#wifiUser",
            "encryptionType" : "#encryptionType",
            "selectedMode" : "#selectedMode",
            "staticModeDiv" : "#staticModeDiv",
            "ip" : "#ip",
            "netmask" : "#netmask",
            "gateway" : "#gateway",*/


            "editApplianceDiv" : "#editApplianceDiv",

            "editApplianceInfo" : "#editApplianceInfo",
            "applianceNameDis" : "#applianceNameDis",
            "applianceLocationDis" : "#applianceLocationDis",

            "deviceName" : "#deviceName",
            "deviceLocationName" : "#deviceLocationName",

            "applianceInfoEditModal" : "#applianceInfoEditModal",
            "applianceInfoEditBtn" : "#applianceInfoEditBtn",

            "applianceNotificationSettings" : "#applianceNotificationSettings",
            "applianceNotificationSettingsText" : "#applianceNotificationSettingsText",

            "removerApplianceFromMyList" : "#removerApplianceFromMyList"
        };

        return (ui);
    }();

})();