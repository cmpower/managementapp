(function(){
    "use strict";
    // DeviceSettingsView

    module.exports = function(){
        var ui_events = {
            "submit @ui.applianceSettingsForm" : "applianceInfoEdit",
            "click @ui.editApplianceInfo" : "editApplianceInfo",
            "click @ui.cancelApplianceInfo" : "cancelApplianceInfo",

            "click @ui.removerApplianceFromMyList" : "removerApplianceFromMyList",

            /*"click @ui.applianceInfoEditBtn" : "applianceInfoEdit",*/
            "change @ui.applianceNotificationSettings" : "setApplianceNotification"

        };

        return (ui_events);
    }();

})();