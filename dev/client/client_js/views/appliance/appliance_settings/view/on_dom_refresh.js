(function(){
    "use strict";
    // View
    var FormValidation = require('../../../../app/utils/validation/form_validation');

    module.exports = function(){

        function on_dom_refresh(){

            this.applianceSettingsForm = FormValidation("applianceSettingsForm", "applianceSettings");
            this.applianceSettingsForm.initValidator();

            this.init();
        }

        return (on_dom_refresh);
    }();

})();