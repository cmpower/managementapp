(function(){
    "use strict";
    // DeviceSettingsView
    var $ = require("jquery");
    var _ = require("underscore");
    var smalltalk = require("../../../../lib/smalltalk");
    var StoreManager = require('../../../../app/storage/store_manager');
    var storeNames = require('../../../../app/storage/store_names');
    var ModelInit = require('../../../../app/models/model_initiator');
    var ModelConfigNames = require('../../../../app/models/model_config_names');
    module.exports = function(){

        var ViewFunctions = {

            init: function () {
                this.userInfo = StoreManager.getStoreData(storeNames.userInfo);
                this.currentId = StoreManager.getStoreData(storeNames.currentSelectedDevice);
                this.deviceList = StoreManager.getStoreData(storeNames.applianceList);
                this.deviceData = _.findWhere(this.deviceList,{deviceId:this.currentId.id});

                this.ui.applianceNameDis.html("Name : "+this.deviceData.deviceName);
                this.ui.applianceLocationDis.html("Location : "+this.deviceData.deviceLocationName);

                this.ui.deviceName.html(this.deviceData.deviceName);
                this.ui.deviceLocationName.html(this.deviceData.deviceLocationName);

                this.checkForDeviceOwner(this.userInfo.userId,this.deviceData.deviceOwner);
                this.getWiFiConfig();
                this.getApplianceNotification();

            },

            checkForDeviceOwner : function (userId,deviceOwnerId) {
                if(userId != deviceOwnerId){
                    this.ui.editApplianceInfo.prop("disabled",true);
                    this.ui.removerApplianceFromMyList.prop("disabled",true);
                }else{
                    this.ui.editApplianceInfo.prop("disabled",false);
                    this.ui.removerApplianceFromMyList.prop("disabled",false);
                }
            },

            getApplianceNotification : function () {
                var model = ModelInit(ModelConfigNames.getApplianceNotificationSettingModel);
                model.setModelData({
                    "deviceId": this.currentId.id,
                    "userId":this.userInfo.userId
                });
                model.sendServerCall();
            },

            populateApplianceNotificationSettings : function () {
                this.applianceNotificationSettings = StoreManager.getStoreData(storeNames.applianceNotificationSetting);
                var val = this.applianceNotificationSettings.deviceNotification;
                (val)?this.ui.applianceNotificationSettingsText.html("Notification Enabled") : this.ui.applianceNotificationSettingsText.html("Notification Disabled");
                this.ui.applianceNotificationSettings.prop("checked",val);
            },

            setApplianceNotification : function (event) {
                var model = ModelInit(ModelConfigNames.setApplianceNotificationSettingModel);
                model.setModelData({
                    "deviceId": this.currentId.id,
                    "userId":this.userInfo.userId,
                    "deviceNotification" : event.currentTarget.checked
                });
                model.sendServerCall();
            },

            refreshApplianceNotificationSettings : function () {
                this.getApplianceNotification();
            },

            populateSetApplianceNotificationSettings : function () {
                this.applianceNotificationSettings = StoreManager.getStoreData(storeNames.applianceNotificationSetting);
                this.ui.applianceNotificationSettings.prop("checked",this.applianceNotificationSettings.deviceNotification);
            },

            getWiFiConfig : function () {
                var model = ModelInit(ModelConfigNames.getApplianceWifiSettingsModel);
                model.setModelData({
                    "deviceId": this.currentId.id
                });
                model.sendServerCall();
            },

            populateApplianceWiFiSettings : function () {
                this.currentWiFiSettings = StoreManager.getStoreData(storeNames.applianceWiFiSettings);
                if(this.currentWiFiSettings.applianceSoftwareVer !== undefined){

                    this.ui.applianceSoftwareVer.html("applianceSoftwareVer : "+this.currentWiFiSettings.applianceSoftwareVer);
                    this.ui.hostname.html("hostname : "+this.currentWiFiSettings.hostname);
                    this.ui.ipAddress.html("ipAddress : "+this.currentWiFiSettings.ipAddress);
                    this.ui.macAddress.html("macAddress : "+this.currentWiFiSettings.macAddress);
                    this.ui.ssid.html("ssid : "+this.currentWiFiSettings.ssid);
                    this.ui.txBytes.html("txBytes : "+this.currentWiFiSettings.txBytes);
                    this.ui.rxBytes.html("rxBytes : "+this.currentWiFiSettings.rxBytes);
                    this.ui.rssi.html("Signal level : "+this.currentWiFiSettings.wifiSignalLevel);
                }

            },

            applianceInfoEdit : function (event) {
                if(!this.applianceSettingsForm.validate()){
                    event.preventDefault();
                    return false;
                }
                var model = ModelInit(ModelConfigNames.applianceOnboardingUpdateModel);
                model.setModelData({
                    "userId": this.userInfo.userId,
                    "deviceId": this.currentId.id,
                    "deviceName": this.ui.applianceName.val(),
                    "deviceLocationName": this.ui.applianceLocation.val()
                });
                model.sendServerCall();
            },

            editApplianceInfo : function (event) {
                this.ui.applianceInfoEditModal.modal('show');
                this.ui.applianceName.val(this.deviceData.deviceName);
                this.ui.applianceLocation.val(this.deviceData.deviceLocationName);

            },

            updateDevice : function () {
                this.ui.applianceInfoEditModal.modal('hide');
                this.ui.applianceNameDis.html(""+this.deviceData.deviceName);
                this.ui.applianceLocationDis.html("Location : "+this.deviceData.deviceLocationName);

                this.ui.deviceName.html(this.deviceData.deviceName);
                this.ui.deviceLocationName.html(this.deviceData.deviceLocationName);
            },

            deviceDataUpdated : function () {
                this.deviceData.deviceName = this.ui.applianceName.val();
                this.deviceData.deviceLocationName = this.ui.applianceLocation.val();
                this.updateDevice();
            },

            removerApplianceFromMyList : function (event) {
                var deviceId = this.currentId.id;

                function deleteAppliance(){
                    var model = ModelInit(ModelConfigNames.removeApplianceOwnerModel);
                    model.setModelData({
                        "deviceId": deviceId
                    });
                    model.sendServerCall();
                }
                smalltalk.confirm('Klario', "Are you sure you want to delete "+this.deviceData.deviceName+" ?").then(function() {
                    deleteAppliance();
                }, function() {
                    console.log('no');
                });
            }
        };


        return ViewFunctions;

    }();

})();
