'use strict';

// DEVICE_CALENDAR
(function () {
    "use strict";

    var RenderComponent = require('../../../app/components/render_sub_view_component');
    var _ = require('underscore');
    //var device_calendar_storage = require("./device_calendar_storage");

    var Sub_View_Render = function Sub_View_Render() {
        //this.( ViewName or RegionName ) = "";
    };

    Sub_View_Render.prototype = {

        /*'setViewNameorRegionName' : function(options){
            this.( ViewName or RegionName ) = options.value;
        },*/

        'renderAddApplianceView': function renderAddApplianceView(data) {
            var view = RenderComponent.renderSubView('add_appliance', 'getAddApplianceView', {});
        },
        'renderApplianceListView': function renderApplianceListView(data) {
            var view = RenderComponent.renderSubView('appliance_list', 'getApplianceListView', {});
        }

    };

    module.exports = (function () {
        return new Sub_View_Render();
    })();
})();