(function(){
    "use strict";
    // AddApplianceView

    var $ = require('jquery');
    var _ = require('underscore');
    var StoreManager = require('../../../../app/storage/store_manager');
    var storeNames = require('../../../../app/storage/store_names');
    var ModelInit = require('../../../../app/models/model_initiator');
    var ModelConfigNames = require('../../../../app/models/model_config_names');
    var Utils = require('../../../../app/utils/utils');
    var QR_Scanner = require('../../../../app/utils/qr_scanner');
    var notification = require('../../../../app/utils/notification/notification_module');

    module.exports = function(){

        var ViewFunctions = {

            //-------------------------------------------------------------
            initAddNewDeviceCollapseEvents: function(){
                var that = this;
                this.ui.addDevicePanel.on('show.bs.collapse', function () {
                    that.ui.showAddDeviceFormButton.hide();
                });
                this.ui.addDevicePanel.on('hide.bs.collapse', function () {
                    that.ui.showAddDeviceFormButton.show();
                });
            },

            initAddDeviceDataModal : function () {
                this.ui.addDeviceDataModal.on('hide.bs.modal', function () {
                    var AppViewInitializer = require('../../../../app/components/view_initializer');
                    AppViewInitializer.CURRENT_CONTENT_VIEW.deviceDataListUpdate();
                });
            },

            scrollDeviceForm: function(){
                Utils.scrollIdToTop("#addDevicePanel");
            },
            //-------------------------------------------------------------

            addNewDevice: function(event){
                var isValid = this.formValidity.validate();
                var userInfo = StoreManager.getStoreData(storeNames.userInfo);
                var model = ModelInit(ModelConfigNames.addApplianceModel);

                if(!isValid){
                    event.preventDefault();
                    return false;
                }
                model.setModelData({
                    "userId": userInfo.userId,
                    "serialId": this.ui.serialNo.val()
                });
                model.sendServerCall();
            },
            addNewDeviceSuccess: function(){
                this.ui.serialNo.val("");
                this.ui.addDevicePanel.collapse("hide");
                this.showDeviceData();
            },
            showDeviceData: function(){
                this.ui.serialNo.val("");
                var addDeviceData = StoreManager.getStoreData(storeNames.addDeviceData);
                var deviceData = StoreManager.getStoreData(storeNames.addDeviceData).response;
                if(addDeviceData.isNewUser){
                    this.ui.addDeviceDataModal.modal("show");
                    this.ui.deviceName.val(deviceData.deviceName);
                    this.ui.deviceLocation.val(deviceData.deviceLocationName);
                }
            },
            submitAddNewDeviceData: function(event){
                var isValid = this.addDeviceDataForm.validate();
                //this.addDeviceDataForm.formValidation('revalidateField','deviceName');
                if(!isValid){
                    event.preventDefault();
                    return false;
                }
                var userInfo = StoreManager.getStoreData(storeNames.userInfo);
                var newDeviceData = StoreManager.getStoreData(storeNames.addDeviceData).response;
                var model = ModelInit(ModelConfigNames.applianceOnboardingUpdateModel);
                model.setModelData({
                    "userId": userInfo.userId,
                    "deviceId": newDeviceData.deviceId,
                    "deviceName": this.ui.deviceName.val(),
                    "deviceLocationName": this.ui.deviceLocation.val()
                });
                model.sendServerCall();
            },
            addNewDeviceDataUpdated : function (ApplianceListView) {
                this.ui.addDeviceDataModal.modal("hide");
                ApplianceListView.refreshDeviceList();
            },
            deviceListUpdate : function (ApplianceListView) {
                ApplianceListView.refreshDeviceList();
            },

            scanQR: function(){
                QR_Scanner.scan(this.scanDetails);
            },
            scanDetails: function(text, format){
                $("#serialNo").val(text);
            }

            //=================================================================================================

        };


        return ViewFunctions;

    }();

})();
