(function(){
    "use strict";
    // AddApplianceView

    module.exports = function(){
        var ui_events = {

            "submit @ui.addNewDeviceForm"   : "addNewDevice",
            "submit @ui.addDeviceDataForm"  : "submitAddNewDeviceData",
            "click @ui.scanQR"              : "scanQR",
            "click @ui.showAddDeviceFormButton" : "scrollDeviceForm"

        };

        return (ui_events);
    }();

})();