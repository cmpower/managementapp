(function(){
    "use strict";
    // AddApplianceView

    module.exports = function(){
        var ui = {


            "addDevicePanel" : "#addDevicePanel",
            "showAddDeviceFormButton" : "#showAddDeviceFormButton",

            "addNewDeviceForm" : "#addNewDeviceForm",
            "addDeviceDataModal" : "#addDeviceDataModal",
            "addDeviceDataForm" : "#addDeviceDataForm",
            "deviceName" : "#deviceName",
            "deviceLocation" : "#deviceLocation",
            "submitAddDeviceData" : "#submitAddDeviceData",
            "serialNo" : "#serialNo",
            "scanQR" : "#scanQR"

        };

        return (ui);
    }();

})();