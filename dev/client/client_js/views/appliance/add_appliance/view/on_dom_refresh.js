"use strict";

(function () {
    "use strict"
    // View
    ;
    var FormValidation = require('../../../../app/utils/validation/form_validation');
    module.exports = (function () {

        function on_dom_refresh() {

            this.formValidity = FormValidation("addNewDeviceForm", "addNewDevice");
            this.formValidity.initValidator();

            this.addDeviceDataForm = FormValidation("addDeviceDataForm", "addDeviceData");
            this.addDeviceDataForm.initValidator();

            this.initAddNewDeviceCollapseEvents();
            this.initAddDeviceDataModal();

        }

        return on_dom_refresh;
    })();
})();