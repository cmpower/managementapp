(function(){
    "use strict";
    // ApplianceView

    module.exports = function(){

        function on_before_destroy(){
            this.showBackButton();
        }

        return (on_before_destroy);
    }();

})();