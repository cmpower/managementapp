"use strict";

(function () {
    "use strict"
    // View

    ;
    module.exports = (function () {

        function on_dom_refresh() {
            this.renderSubViews();
            this.hideBackButton();
        }

        return on_dom_refresh;
    })();
})();