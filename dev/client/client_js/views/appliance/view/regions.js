(function(){
    "use strict";
    // ApplianceView

    module.exports = function(){
        var regions = {
            "add_appliance":"#add_appliance",
            "appliance_list":"#appliance_list"

        };

        return (regions);
    }();

})();