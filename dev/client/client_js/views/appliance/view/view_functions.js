"use strict";

(function () {
    "use strict"
    // ApplianceView
    ;
    var appliance_Sub_View_Render = require("../utility/appliance_sub_view_render");
    var $ = require('jquery');

    module.exports = (function () {

        var ViewFunctions = {

            hideBackButton: function () {
                $("#backHeaderButton").hide();
            },
            showBackButton: function () {
                $("#backHeaderButton").show();
            },

            //------------------------------------------------------
            renderSubViews: function () {
                console.log("render subview appliance");
                appliance_Sub_View_Render.renderApplianceListView();
                appliance_Sub_View_Render.renderAddApplianceView();
            },

            populateApplianceList: function () {
                var ApplianceListView = this.getChildView('appliance_list');
                ApplianceListView.populateApplianceListSub();
            },

            refreshApplianceList: function () {
                var ApplianceListView = this.getChildView('appliance_list');
                ApplianceListView.refreshDeviceList();
            },

            updateApplianceStatus: function (msg, type, deviceId) {
                var ApplianceListView = this.getChildView("appliance_list");
                ApplianceListView.updateDeviceStatus(msg, type, deviceId);
            },

            addNewDeviceSuccess: function () {
                var AddApplianceView = this.getChildView("add_appliance");
                AddApplianceView.addNewDeviceSuccess();
            },

            deviceDataUpdated: function () {
                var ApplianceListView = this.getChildView("appliance_list");
                var AddApplianceView = this.getChildView("add_appliance");
                AddApplianceView.addNewDeviceDataUpdated(ApplianceListView);
            },
            deviceDataListUpdate: function () {
                var ApplianceListView = this.getChildView("appliance_list");
                var AddApplianceView = this.getChildView("add_appliance");
                AddApplianceView.deviceListUpdate(ApplianceListView);
            },

            updateDeviceStatusFromNoti: function (data) {
                var ApplianceListView = this.getChildView('appliance_list');
                ApplianceListView.updateDeviceStatusFromNoti(data);
            }

        };

        return ViewFunctions;
    })();
})();