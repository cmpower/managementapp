(function(){
    "use strict";
    // GlobalSettingsView

    module.exports = function(){
        var ui_events = {
            "change @ui.domainRadioButton" : "domainRadioButtonFunc"

        };

        return (ui_events);
    }();

})();