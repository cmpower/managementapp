(function(){
    "use strict";
    // LoginView

    module.exports = function(){
        var behaviors = {
            MaterializeBehaviour: {
                message: "Materialize HTML Elements"
            }
        };

        return (behaviors);
    }();

})();