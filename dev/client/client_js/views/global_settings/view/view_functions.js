(function(){
    "use strict";
    // GlobalSettingsView

    module.exports = function(){

        var ViewFunctions = {

            hideHeaderFooter: function () {
                $("#HeaderRegion").hide();
                $("#SidebarRegion").hide();
                $("#FooterRegion").hide();
            },

            init: function() {
                var Local_Storage = require('../../../app/local_storage/local_storage');
                var Local_Storage_Names = require('../../../app/local_storage/local_storage_list');

                ((Local_Storage.retrieve(Local_Storage_Names.targetUrl)) == "https://www.klario.net") ? $("#klarioNET").prop("checked",true) :  $("#klarioIN").prop("checked",true) ;
            },

            domainRadioButtonFunc: function(event) {
                var Local_Storage = require('../../../app/local_storage/local_storage');
                var Local_Storage_Names = require('../../../app/local_storage/local_storage_list');
                var Target_Url = require('../../../app/api/target_url');

                (event.currentTarget.id == "klarioIN") ? Local_Storage.store(Local_Storage_Names.targetUrl, Target_Url.klario_in) : Local_Storage.store(Local_Storage_Names.targetUrl, Target_Url.klario_net);
            }

        };


        return ViewFunctions;

    }();

})();
