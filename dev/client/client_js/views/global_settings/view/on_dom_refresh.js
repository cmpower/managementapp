(function(){
    "use strict";
    // GlobalSettingsView

    module.exports = function(){

        function on_dom_refresh(){
            this.hideHeaderFooter();
            this.init();
        }

        return (on_dom_refresh);
    }();

})();