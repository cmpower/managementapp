"use strict";

(function () {
    "use strict"
    // HeaderView

    ;
    var $ = require("jquery");
    //var ViewStateManager = require("../../../app/state_manager/view_state_manager");

    module.exports = (function () {

        var ViewFunctions = {

            menuClicked: function menuClicked() {
                $("#navbar").collapse("hide");
            },

            goBack: function goBack() {
                $('.modal').modal('hide');
                window.history.back();
            },

            logoutApp: function logoutApp() {
                var AppStateChange = require('../../../app/state_manager/app_state_manager');
                AppStateChange.logoutApp();
            }

        };

        return ViewFunctions;
    })();
})();