(function(){
    "use strict";
    // HeaderView

    module.exports = function(){
        var ui_events = {
            "click @ui.menuOptions" : "menuClicked",
            "click @ui.backHeaderButton" : "goBack",
            "click @ui.logoutButton" : "logoutApp"
        };

        return (ui_events);
    }();

})();