(function(){
    "use strict";
    // HeaderView

    module.exports = function(){
        var ui = {
            "menuOptions" : ".menuOptions",
            "backHeaderButton" : "#backHeaderButton",
            "logoutButton" : "#logoutButton"
        };

        return (ui);
    }();

})();