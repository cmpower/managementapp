"use strict";

(function () {
    "use strict";
    // HeaderView

    var _ = require('underscore');
    var viewContentLang = require("../../../app/utils/language/multi_lingual_module.js");
    var languageId = "HeaderView",
        templateId = "HeaderView";

    var initView = function initView() {
        this.template = _.template(viewContentLang.getCurrentLangViewTemplate(languageId, templateId));
    };

    module.exports = (function () {
        return initView;
    })();
})();