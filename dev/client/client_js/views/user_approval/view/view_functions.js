(function(){
    "use strict";
    // UserApprovalView

    var StoreManager = require('../../../app/storage/store_manager');
    var storeNames = require('../../../app/storage/store_names');
    var ModelInit = require('../../../app/models/model_initiator');
    var ModelConfigNames = require('../../../app/models/model_config_names');
    var dataTable = require('../../../app/utils/data_table/render_data_table');
    var notification = require('../../../app/utils/notification/notification_module');
    
    module.exports = function(){

        var ViewFunctions = {

            updatePendingUserList: function(){
                this.getUserRequestList();
            },

            getUserRequestList: function(){
                var model = ModelInit(ModelConfigNames.getUserRequestListModel);
                var userInfo = StoreManager.getStoreData(storeNames.userInfo);
                model.setModelData({
                    "userId": userInfo.userId,
                    "permissionStatus": "pending"
                });
                model.sendServerCall();
            },

            renderUserRequestList: function(){
                var userRList = StoreManager.getStoreData(storeNames.userRequestList);
                var i, tableData = [], tableOpts = {};
                var requestor, deviceDetails;

                if(userRList.length > 0){
                    this.ui.initMsgDiv.hide();
                    this.ui.tableDiv.show();
                }else{
                    this.ui.tableDiv.hide();
                    this.ui.initMsgDiv.show();
                }

                for(i = 0; i < userRList.length; i++){
                    deviceDetails = userRList[i].deviceDetails;
                    requestor =  userRList[i].deviceRequesterDetails;
                    tableData.push({
                        "Info"      : '<a href="javascript:void(0);"><i class="fa fa-chevron-down f-20 rowExpandTable" aria-hidden="true"></i></a>',
                        "Name"      : "<p class='m-b-20'>"+requestor.name+"</p>"+"<button data-requestId='"+userRList[i].deviceRequestId+"' class='rejectUser btn btn-raised btn-sm btn-danger btn-block' type='button'>Reject</button>",
                        "Device"    : "<p class='m-b-20'> "+deviceDetails.deviceName+" <br></p>"+
                        "<button data-requestId='"+userRList[i].deviceRequestId+"' class='acceptUser btn btn-raised btn-sm btn-success btn-block' type='button'>Approve</button>",
                        "Location"   : deviceDetails.deviceLocationName,
                        "Email"     : requestor.emailId,
                        "Company"   : requestor.company,
                        "Phone"     : requestor.phoneNo
                    });
                }

                tableOpts = {
                    "paging":   false,
                    "ordering": false,
                    "info":     false,
                    responsive: {
                        details: false
                    },
                    destroy:true,
                    "dom": "<'well well-sm m-b-10'<'row'<'col-sm-12'f>>>" +
                    "<'well well-sm'<'row'<'col-sm-12'tr>>>",
                    "oLanguage": {
                        "sSearch": "<i class='fa fa-search pull-left'></i>"
                    },
                    "aoColumns": [
                        { data:"Info", "sClass": "tablet-p mobile-l mobile-p text-center dropDownIconTd extraDetail" },
                        { data:"Name", "sClass": "all text-left" },
                        { data:"Device", "sClass": "all text-left" },
                        { data:"Location", "sClass": "desktop tablet-l" },
                        { data:"Email", "sClass": "desktop tablet-l" },
                        { data:"Company", "sClass": "desktop tablet-l" },
                        { data:"Phone", "sClass": "desktop tablet-l" }
                    ],
                    "order": [[0, 'asc']]
                };

                var approveUserListTable = dataTable("userRequestTable", tableOpts, tableData);
                this.userRequestTable = approveUserListTable.renderDataTable();

                this.initMaterialComponents();
            },
            showTableDropDown: function(event){
                if(!$(".rowExpandTable").is(":visible")){
                    return false;
                }
                var tr = $(event.currentTarget).closest('tr');
                var row = this.userRequestTable.row(tr);

                function format ( d ) {
                    // `d` is the original data object for the row
                    return '<div class="row">'+
                        '<div class="col-xs-12">'+
                        '<p class="text-left">'+
                        '<span class="text-bold">Location</span> : ' + d.Location+
                        '<br>' +
                        '<span class="text-bold">Email</span> : ' + d.Email+
                        '<br>' +
                        '<span class="text-bold">Company</span> : ' + d.Company+
                        '<br>' +
                        '<span class="text-bold">Phone</span> : ' + d.Phone+
                        '</p>' +
                        '</div>'+
                        '</div>';
                }

                if ( row.child.isShown() ) {
                    // This row is already open - close it
                    row.child.hide();
                    tr.removeClass('shown');
                    $(event.currentTarget).find('.rowExpandTable').toggleClass('fa-chevron-down fa-chevron-up');
                    $($(event.currentTarget).siblings(".dropDownIconTd")).find('.rowExpandTable').toggleClass('fa-chevron-down fa-chevron-up');
                }
                else {
                    // Open this row
                    row.child( format(row.data()) ).show();
                    tr.addClass('shown');
                    $('head').append('<style>table.dataTable.dtr-inline.collapsed > tbody > tr > td:first-child:before, table.dataTable.dtr-inline.collapsed > tbody > tr > th:first-child:before{content: none !important;}</style>');
                    $(event.currentTarget).find('.rowExpandTable').toggleClass('fa-chevron-down fa-chevron-up');
                    $($(event.currentTarget).siblings(".dropDownIconTd")).find('.rowExpandTable').toggleClass('fa-chevron-down fa-chevron-up');
                }
                //Utils.scrollElementToTop(event);
            },

            //------------------------------------------------------

            rejectUser: function(event){
                var model = ModelInit(ModelConfigNames.getRejectAccessModel);
                var userInfo = StoreManager.getStoreData(storeNames.userInfo);
                var requestId = $(event.currentTarget).attr("data-requestId");
                model.setModelData({
                    "userId": userInfo.userId,
                    "deviceRequestId": requestId
                });
                model.sendServerCall();
            },

            acceptUser: function(event){
                var model = ModelInit(ModelConfigNames.getApproveAccessModel);
                var userInfo = StoreManager.getStoreData(storeNames.userInfo);
                var requestId = $(event.currentTarget).attr("data-requestId");
                model.setModelData({
                    "userId": userInfo.userId,
                    "deviceRequestId": requestId
                });
                model.sendServerCall();
            },

            //--------------------------------------
            initMaterialComponents: function(){
                $.material.init();
            }
            //    ----------------------------------------------
        };


        return ViewFunctions;

    }();

})();
