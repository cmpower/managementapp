(function(){
    "use strict";
    // UserApprovalView

    module.exports = function(){
        var ui = {
            "refreshUserApprovalList" : "#refreshUserApprovalList",
            "initMsgDiv" : "#initMsgDiv",
            "tableDiv" : "#tableDiv"

        };

        return (ui);
    }();

})();