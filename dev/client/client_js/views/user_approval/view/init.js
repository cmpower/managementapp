(function(){
    "use strict";
    // UserApprovalView

    var _ = require('underscore');
    var viewContentLang = require("../../../app/utils/language/multi_lingual_module.js");
    var languageId = "UserApprovalView", templateId = "UserApprovalView";

    var initView = function(){
        this.template = _.template(viewContentLang.getCurrentLangViewTemplate(languageId, templateId));
    };

    module.exports = function(){
        return (initView);
    }();

})();