(function(){
    "use strict";
    // UserApprovalView

    module.exports = function(){
        var ui_events = {
            "click .rejectUser" : "rejectUser",
            "click .acceptUser" : "acceptUser",
            "click td.extraDetail" : "showTableDropDown",
            "click @ui.refreshUserApprovalList" : "updatePendingUserList"
        };

        return (ui_events);
    }();

})();