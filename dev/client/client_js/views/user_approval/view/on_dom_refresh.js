(function(){
    "use strict";
    // UserApprovalView

    module.exports = function(){

        function on_dom_refresh(){
            this.getUserRequestList();
        }

        return (on_dom_refresh);
    }();

})();