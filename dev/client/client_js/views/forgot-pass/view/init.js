(function(){
    "use strict";
    // ForgotPassView

    var _ = require('underscore');
    var viewContentLang = require("../../../app/utils/language/multi_lingual_module.js");
    var languageId = "ForgotPassView", templateId = "ForgotPassView";

    var initView = function(){
        this.template = _.template(viewContentLang.getCurrentLangViewTemplate(languageId, templateId));
    };

    module.exports = function(){
        return (initView);
    }();

})();