(function(){
    "use strict";
    // ForgotPassView

    module.exports = function(){
        var ui_events = {
            "submit @ui.forgotPasswordForm" : "forgotPassword"

        };

        return (ui_events);
    }();

})();