(function(){
    "use strict";
    // ForgotPassView
    var $ = require('jquery');
    var ModelInit = require('../../../app/models/model_initiator');
    var ModelConfigNames = require('../../../app/models/model_config_names');

    module.exports = function(){

        var ViewFunctions = {

            hideHeaderFooter:function(){
                $("#HeaderRegion").hide();
                $("#SidebarRegion").hide();
                $("#FooterRegion").hide();
            },

            forgotPassword: function(event){
                var isValid = this.formValidity.validate();
                var model = ModelInit(ModelConfigNames.forgotPasswordModel);

                if(!isValid){
                    event.preventDefault();
                    return false
                }

                model.setModelData({
                    "emailId" : this.ui.emailId.val()
                });
                model.sendServerCall();
            }

        };


        return ViewFunctions;

    }();

})();
