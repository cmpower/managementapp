(function(){
    "use strict";
    // ForgotPassView
     var FormValidation = require('../../../app/utils/validation/form_validation');
    module.exports = function(){

        function on_dom_refresh(){
            this.formValidity = FormValidation("forgotPasswordForm", "forgotPassword");
            this.formValidity.initValidator();
            this.hideHeaderFooter();
        }

        return (on_dom_refresh);
    }();

})();