"use strict";

(function () {
    "use strict"
    // View
    ;
    var FormValidation = require('../../../app/utils/validation/form_validation');

    module.exports = (function () {

        function on_dom_refresh() {
            this.formValidity = FormValidation("newRegisterForm", "newRegister");
            this.formValidity.initValidator();
            this.hideHeaderFooter();
            this.initPhoneNumberValidation();

        }

        return on_dom_refresh;
    })();
})();