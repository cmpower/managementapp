(function(){
    "use strict";
    // RegistrationView

    module.exports = function(){
        var ui_events = {

            "submit @ui.newRegisterForm" : "submitNewRegister"

        };

        return (ui_events);
    }();

})();