'use strict';

(function () {
    "use strict"
    // RegistrationView
    ;
    var $ = require('jquery');
    var ModelInit = require('../../../app/models/model_initiator');
    var ModelConfigNames = require('../../../app/models/model_config_names');

    module.exports = (function () {

        var ViewFunctions = {

            hideHeaderFooter: function () {
                $("#HeaderRegion").hide();
                $("#SidebarRegion").hide();
                $("#FooterRegion").hide();
            },

            initPhoneNumberValidation: function () {
                $("#phoneNo").intlTelInput({
                    initialCountry: "auto",
                    geoIpLookup: function(callback) {
                        $.get("http://ipinfo.io", function() {}, "jsonp").always(function(resp) {
                            var countryCode = (resp && resp.country) ? resp.country : "";
                            callback(countryCode);
                        });
                    },
                    utilsScript: "js/lib/utilsTelPhone.js"
                });
            },

            submitNewRegister: function (event) {
                var isValid = this.formValidity.validate();
                var model = ModelInit(ModelConfigNames.newRegisterModel);

                if (!isValid) {
                    event.preventDefault();
                    return false;
                }

                model.setModelData({
                    "userName": this.ui.emailId.val(),
                    "password": this.ui.password.val(),
                    "userData": {
                        "firstName": this.ui.firstName.val(),
                        "lastName": this.ui.lastName.val(),
                        "company": this.ui.company.val(),
                        "emailId": this.ui.emailId.val(),
                        "phoneNo": this.ui.phoneNo.val()
                    }

                });

                model.sendServerCall();
            }

        };

        return ViewFunctions;
    })();
})();