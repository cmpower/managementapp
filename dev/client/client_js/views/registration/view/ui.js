(function(){
    "use strict";
    // RegistrationView

    module.exports = function(){
        var ui = {
            "newRegisterForm"   : "#newRegisterForm",

            "firstName"         : "#firstName",
            "lastName"          : "#lastName",
            "company"           : "#company",
            "emailId"           : "#emailId",
            "password"           : "#password",
            "phoneNo"           : "#phoneNo"

        };

        return (ui);
    }();

})();