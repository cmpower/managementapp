"use strict";

(function () {
    "use strict"
    // RegistrationView

    ;
    var _ = require('underscore');
    var viewContentLang = require("../../../app/utils/language/multi_lingual_module.js");
    var languageId = "RegistrationView",
        templateId = "RegistrationView";

    var initView = function initView() {
        this.template = _.template(viewContentLang.getCurrentLangViewTemplate(languageId, templateId));
    };

    module.exports = (function () {
        return initView;
    })();
})();