(function(){
    "use strict";
    // LoginView

    module.exports = function(){
        var behaviors = {
            RippleBehaviour: {
                    message: "Ripple Effect"
                }
            };

        return (behaviors);
    }();

})();