'use strict';

(function () {
    "use strict"
    // LandingPageView

    ;
    module.exports = (function () {

        //---------------------------------------------------------------------------
        var init = require('./init');
        var ui = require('./ui');
        var behaviors = require('./behaviors');
        var ui_events = require('./ui_events');
        var onRender = require('./on_render');
        var onAttach = require('./on_attach');
        var onDomRefresh = require('./on_dom_refresh');
        var onBeforeDestroy = require('./on_before_destroy');
        //--------------------------------------------------------------------------

        var ViewElements = {

            initialize: init,

            ui: ui,

            behaviors: behaviors,

            events: ui_events,

            onRender: onRender,

            onAttach: onAttach,

            onDomRefresh: onDomRefresh,

            onBeforeDestroy: onBeforeDestroy
        };

        return ViewElements;
    })();
})();