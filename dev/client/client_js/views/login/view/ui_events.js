(function(){
    "use strict";
    // LoginView

    module.exports = function(){
        var ui_events = {
            "submit @ui.loginForm"    : "submitLogin",
            "change @ui.rememberUser" : "rememberInfo"
        };

        return (ui_events);
    }();

})();