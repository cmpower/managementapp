(function(){
    "use strict";
    // LoginView

    module.exports = function(){

        function on_before_destroy(){
            this.onClose();
        }

        return (on_before_destroy);
    }();

})();