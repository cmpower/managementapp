"use strict";

(function () {
    "use strict"
    // VIEW_TEMPLATE_ID
    ;
    var FormValidation = require('../../../app/utils/validation/form_validation');

    module.exports = (function () {

        function on_dom_refresh() {
            this.init();
            this.loginFormValidity = FormValidation("loginForm", "login");
            this.loginFormValidity.initValidator();
        }

        return on_dom_refresh;
    })();
})();