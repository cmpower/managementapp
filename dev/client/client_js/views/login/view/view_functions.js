"use strict";

(function () {
    "use strict"
    // LoginView

    ;
    var $ = require("jquery");
    var ModelInit = require('../../../app/models/model_initiator');
    var ModelConfigNames = require('../../../app/models/model_config_names');
    var StoreManager = require('../../../app/storage/store_manager');
    var storeNames = require('../../../app/storage/store_names');
    //var Local_Storage = require('../../../app/local_storage/local_storage');

    module.exports = (function () {

        var ViewFunctions = {

            init: function init() {
                console.log("login-init");
                //  $('#errorLogin').hide();
                $('#errorLoginText').css("color", "#00acc4");
                $("#HeaderRegion").hide();
                $("#SidebarRegion").hide();
                $("#FooterRegion").hide();
                $('#ContentRegion').toggleClass("p-t-60", "p-0");
                this.initRememberInfo();
            },
            onClose: function onClose() {
                $("#HeaderRegion").show();
                $("#SidebarRegion").show();
                $("#FooterRegion").show();
                $('#ContentRegion').toggleClass("p-t-60", "p-0");
            },

            submitLogin: function submitLogin(event) {
                var isValid = this.loginFormValidity.validate();
                if (!isValid) {
                    event.preventDefault();
                    return false;
                }

                var model = ModelInit(ModelConfigNames.loginModel);
                model.setModelData({
                    "userName": this.ui.userId.val(),
                    "password": this.ui.inputPassword.val()
                });
                model.sendServerCall();

                if(this.ui.rememberUser.prop('checked')){
                    var Local_Storage = require('../../../app/local_storage/local_storage');
                    var Local_Storage_Names = require('../../../app/local_storage/local_storage_list');
                    Local_Storage.store(Local_Storage_Names.userName,this.ui.userId.val());
                    Local_Storage.store(Local_Storage_Names.userPassword,this.ui.inputPassword.val());
                }
            },

            errorLoginFunc: function errorLoginFunc() {
                var error = StoreManager.getStoreData(storeNames.loinError);
                this.ui.errorLoginText.html(error.message);
                $('#errorLoginText').css("color", "#f44336");
            },

            initRememberInfo : function () {
                var Local_Storage = require('../../../app/local_storage/local_storage');
                var Local_Storage_Names = require('../../../app/local_storage/local_storage_list');
                var remember = Local_Storage.retrieve(Local_Storage_Names.rememberMe);
                if(remember != null && remember != "false" ){
                    this.ui.userId.val(Local_Storage.retrieve(Local_Storage_Names.userName));
                    this.ui.inputPassword.val(Local_Storage.retrieve(Local_Storage_Names.userPassword));
                    this.ui.rememberUser.prop('checked',true);
                }
            },
            rememberInfo : function (event) {
                var Local_Storage = require('../../../app/local_storage/local_storage');
                var Local_Storage_Names = require('../../../app/local_storage/local_storage_list');
                var doRemember = event.currentTarget.checked;
                Local_Storage.store(Local_Storage_Names.rememberMe, doRemember);
                if(!doRemember){
                    Local_Storage.clearStoreItems([Local_Storage_Names.userName,Local_Storage_Names.userPassword]);

                }
            }

            //    --------------------------------------------------------------------
            /*checkAppInState: function(){
             var AppStateChange =require('../../../app/state_manager/app_state_manager');
             var appStore = store.getState();
             var AppInState = appStore.appInState;
             (AppInState)? AppStateChange.logoutApp() : "";
             },
             loadUserNamePass: function(){
             var uName = Local_Storage.retrieve("userId");
             var pass = Local_Storage.retrieve("pass");
              this.ui.userName.val((uName == null)? "" : uName);
             this.ui.password.val((pass == null)? "" : pass);
             }*/
            //    --------------------------------------------------------------------
        };

        return ViewFunctions;
    })();
})();