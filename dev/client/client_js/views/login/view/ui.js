(function(){
    "use strict";
    // LoginView

    module.exports = function(){
        var ui = {
            "loginForm" : "#loginForm",
            "userId"    : "#userId",
            "errorLogin"    : "#errorLogin",
            "errorLoginText"    : "#errorLoginText",
            "inputPassword"  : "#inputPassword",
            "rememberUser"  : "#rememberUser"
        };

        return (ui);
    }();

})();