(function(){
    "use strict";
    // UserPermissionView

    module.exports = function(){

        function on_dom_refresh(){
            this.renderSubViews();
            this.init();
        }

        return (on_dom_refresh);
    }();

})();