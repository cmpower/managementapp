(function(){
    "use strict";
    // UserPermissionView

    module.exports = function(){
        var regions = {
            "permission-tab" : "#permissionTab",
            "user-tab" : "#userTab",
            "invite-user-permission-tab" : "#inviteUserPermission"

        };

        return (regions);
    }();

})();