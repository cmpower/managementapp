(function(){
    "use strict";
    // UserPermissionView
    var _ = require("underscore");
    var StoreManager = require('../../../app/storage/store_manager');
    var storeNames = require('../../../app/storage/store_names');
    var ModelInit = require('../../../app/models/model_initiator');
    var ModelConfigNames = require('../../../app/models/model_config_names');
    var SubViewRender = require('../utility/user_perm_sub_view_render');

    module.exports = function(){

        var ViewFunctions = {

            renderSubViews: function() {
                //SubViewRender.renderUserRequestListView();
                SubViewRender.renderUserAccessListView();
                SubViewRender.renderUserPermissionInvite();
            },

            init: function(){
                var currentId = StoreManager.getStoreData(storeNames.currentSelectedDevice);
                var deviceList = StoreManager.getStoreData(storeNames.applianceList);
                var deviceData = _.findWhere(deviceList,{deviceId:currentId.id});

                this.ui.deviceName.html(deviceData.deviceName);
                this.ui.deviceLocationName.html(deviceData.deviceLocationName);
            },

            //    --------------------------------------------------------------
            //    --------------------------------------------------------------
            renderUserRequestList: function(){
                var RequestListView = this.getChildView('permission-tab');
                RequestListView.renderUserRequestList();
            },
            renderUserAccessList: function(){
                var AccessListView = this.getChildView('user-tab');
                AccessListView.renderUserAccessList();
            },
            updateSubUserList: function(){
                var AccessListView = this.getChildView('user-tab');
                AccessListView.updateUserAccessList();
            },
            userPermissionInviteSent : function () {
                this.updateSubUserList();
                var UserPermissionInviteView = this.getChildView('invite-user-permission-tab');
                UserPermissionInviteView.userPermissionInviteSent();
            }
            //    --------------------------------------------------------------

        };


        return ViewFunctions;

    }();

})();
