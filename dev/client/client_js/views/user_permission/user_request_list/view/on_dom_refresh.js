(function(){
    "use strict";
    // UserRequestListView

    module.exports = function(){

        function on_dom_refresh(){
            this.getUserRequestList();
        }

        return (on_dom_refresh);
    }();

})();