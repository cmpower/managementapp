(function(){
    "use strict";
    // UserRequestListView

    var StoreManager = require('../../../../app/storage/store_manager');
    var storeNames = require('../../../../app/storage/store_names');
    var ModelInit = require('../../../../app/models/model_initiator');
    var ModelConfigNames = require('../../../../app/models/model_config_names');
    var Utils = require('../../../../app/utils/utils');
    var dataTable = require('../../../../app/utils/data_table/render_data_table');
    var notification = require('../../../../app/utils/notification/notification_module');

    module.exports = function(){

        var ViewFunctions = {

            getUserRequestList: function(){
                var model = ModelInit(ModelConfigNames.getUserRequestListModel);
                var userInfo = StoreManager.getStoreData(storeNames.userInfo);
                var selectedDevice = StoreManager.getStoreData(storeNames.currentSelectedDevice);
                model.setModelData({
                    "deviceId": selectedDevice.id,
                    "userId": userInfo.userId,
                    "permissionStatus": "pending"
                });
                model.sendServerCall();
            },

            renderUserRequestList: function(){
                var userRList = StoreManager.getStoreData(storeNames.userRequestList);
                var i, tableData = [], tableOpts = {};
                var requestor;

                for(i = 0; i < userRList.length; i++){

                    requestor =  userRList[i].deviceRequesterDetails;
                    tableData.push({
                        "0" : requestor.name,
                        "1" : "<button data-requestId='"+userRList[i].deviceRequestId+"' class='rejectUser btn btn-raised btn-sm btn-danger' type='button'><i class='fa fa-times'></i></button>",
                        "2" : "<button data-requestId='"+userRList[i].deviceRequestId+"' class='acceptUser btn btn-raised btn-sm btn-success' type='button'><i class='fa fa-check'></i></button>"
                    });
                }

                tableOpts = {
                    "paging":   false,
                    "ordering": false,
                    "info":     false,
                    responsive: {
                        details: false
                    },
                    "aoColumns": [
                        { "sClass": "text-left" },
                        { "sClass": "" },
                        { "sClass": "" }
                    ],
                    destroy:true
                };

                this.requestTable = dataTable("userRequestTable", tableOpts, tableData);
                this.requestTable.renderDataTable();

            },

            rejectUser: function(event){
                var model = ModelInit(ModelConfigNames.getRejectAccessModel);
                var userInfo = StoreManager.getStoreData(storeNames.userInfo);
                var requestId = $(event.currentTarget).attr("data-requestId");
                model.setModelData({
                    "userId": userInfo.userId,
                    "deviceRequestId": requestId
                });
                model.sendServerCall();
            },

            acceptUser: function(event){
                var model = ModelInit(ModelConfigNames.getApproveAccessModel);
                var userInfo = StoreManager.getStoreData(storeNames.userInfo);
                var requestId = $(event.currentTarget).attr("data-requestId");
                model.setModelData({
                    "userId": userInfo.userId,
                    "deviceRequestId": requestId
                });
                model.sendServerCall();
            }

        };


        return ViewFunctions;

    }();

})();
