(function(){
    "use strict";
    // UserPermissionInviteView

    var _ = require('underscore');
    var viewContentLang = require("../../../../app/utils/language/multi_lingual_module.js");
    var languageId = "UserPermissionInviteView", templateId = "UserPermissionInviteView";

    var initView = function(){
        this.template = _.template(viewContentLang.getCurrentLangViewTemplate(languageId, templateId));
    };

    module.exports = function(){
        return (initView);
    }();

})();