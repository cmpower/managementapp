(function(){
    "use strict";
    // UserPermissionInviteView

    module.exports = function(){
        var ui = {
            "inviteNewUserPermissionForm" : "#inviteNewUserPermissionForm",
            "subOwnerEmailId" : "#subOwnerEmailId",
            "inviteUserPermissionPanel" : "#inviteUserPermissionPanel",
            "showInviteUserPermissionFormButton" : "#showInviteUserPermissionFormButton"

        };

        return (ui);
    }();

})();