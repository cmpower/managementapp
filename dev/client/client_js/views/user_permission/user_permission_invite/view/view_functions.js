(function(){
    "use strict";
    // UserPermissionInviteView

    var StoreManager = require('../../../../app/storage/store_manager');
    var storeNames = require('../../../../app/storage/store_names');
    var ModelInit = require('../../../../app/models/model_initiator');
    var ModelConfigNames = require('../../../../app/models/model_config_names');
    var dataTable = require('../../../../app/utils/data_table/render_data_table');
    var notification = require('../../../../app/utils/notification/notification_module');

    module.exports = function(){

        var ViewFunctions = {


            initAddNewDeviceCollapseEvents: function(){
                var that = this;
                this.ui.inviteUserPermissionPanel.on('show.bs.collapse', function () {
                    that.ui.showInviteUserPermissionFormButton.hide();
                });
                this.ui.inviteUserPermissionPanel.on('hide.bs.collapse', function () {
                    that.ui.showInviteUserPermissionFormButton.show();
                });
            },

            inviteNewUserPermission : function (event) {
                if(!this.inviteNewUserPermissionForm.validate()){
                    event.preventDefault();
                    return false;
                }
                var model = ModelInit(ModelConfigNames.userAppliancePermissionInviteModel);
                var userInfo = StoreManager.getStoreData(storeNames.userInfo);
                var selectedDevice = StoreManager.getStoreData(storeNames.currentSelectedDevice);
                model.setModelData({
                    "userId": userInfo.userId,
                    "deviceId": selectedDevice.id,
                    "subOwnerEmailId": (this.ui.subOwnerEmailId.val()).toLowerCase()
                });
                model.sendServerCall();
            },

            userPermissionInviteSent : function () {
                this.ui.subOwnerEmailId.val('');
                this.ui.inviteUserPermissionPanel.collapse("hide");
            }

        };
        return ViewFunctions;

    }();

})();
