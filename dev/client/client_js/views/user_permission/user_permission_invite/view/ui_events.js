(function(){
    "use strict";
    // UserPermissionInviteView

    module.exports = function(){
        var ui_events = {
            "submit @ui.inviteNewUserPermissionForm" : "inviteNewUserPermission"

        };

        return (ui_events);
    }();

})();