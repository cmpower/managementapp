(function(){
    "use strict";
    // UserPermissionInviteView
    var FormValidation = require('../../../../app/utils/validation/form_validation');
    module.exports = function(){

        function on_dom_refresh(){
            this.inviteNewUserPermissionForm = FormValidation("inviteNewUserPermissionForm", "inviteNewUserPermission");
            this.inviteNewUserPermissionForm.initValidator();

            this.initAddNewDeviceCollapseEvents();
        }

        return (on_dom_refresh);
    }();

})();