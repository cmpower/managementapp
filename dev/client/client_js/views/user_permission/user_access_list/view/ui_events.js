(function(){
    "use strict";
    // UserAccessListView

    module.exports = function(){
        var ui_events = {
            "click .removeUser" : "removeUser",
            "click td.extraDetail" : "showTableDropDown",
            "click @ui.refreshSubUserList" : "updateUserAccessList"

        };

        return (ui_events);
    }();

})();