(function(){
    "use strict";
    // UserAccessListView

    var StoreManager = require('../../../../app/storage/store_manager');
    var storeNames = require('../../../../app/storage/store_names');
    var ModelInit = require('../../../../app/models/model_initiator');
    var ModelConfigNames = require('../../../../app/models/model_config_names');
    var dataTableModule = require('../../../../app/utils/data_table/render_data_table');
    var notification = require('../../../../app/utils/notification/notification_module');

    module.exports = function(){

        var ViewFunctions = {

            updateUserAccessList: function(){
                this.getUserAccessList();
            },
            getUserAccessList: function(){
                var model = ModelInit(ModelConfigNames.getUserAccessListModel);
                var selectedDevice = StoreManager.getStoreData(storeNames.currentSelectedDevice);
                model.setModelData({
                    "deviceId": selectedDevice.id
                });
                model.sendServerCall();
            },
            renderUserAccessList: function(){
                var userAList = StoreManager.getStoreData(storeNames.userAccessList);
                var i, tableData = [], tableOpts = {};
                var requestor;

                if(userAList.length > 0){
                    this.ui.initMsgDiv.hide();
                    this.ui.tableDiv.show();
                }else{
                    this.ui.tableDiv.hide();
                    this.ui.initMsgDiv.show();
                }

                for(i = 0; i < userAList.length; i++){
                    requestor =  userAList[i].userData;
                    tableData.push({
                        "Name"      : requestor.name,
                        "Revoke"    : "<button data-userId='"+userAList[i].userId+"' class='removeUser btn btn-raised btn-sm btn-danger' type='button'><i class='fa fa-times'></i></button>",
                        "Info"      : '<a href="javascript:void(0);"><i class="fa fa-chevron-down f-20 rowExpandTable" aria-hidden="true"></i></a>',
                        "Email"     : requestor.emailId,
                        "Company"   : requestor.company,
                        "Phone"     : requestor.phoneNo
                    });
                }

                tableOpts = {
                    "paging":   false,
                    "ordering": false,
                    "info":     false,
                    "responsive": {
                        details: false
                    },
                    "destroy":true,
                    "dom": "<'well well-sm m-b-10'<'row'<'col-sm-12'f>>>" +
                    "<'well well-sm'<'row'<'col-sm-12'tr>>>",
                    "oLanguage": {
                        "sSearch": "<i class='fa fa-search pull-left'></i>"
                    },
                    "aoColumns": [
                        { data:"Name", "sClass": "all text-center" },
                        { data:"Revoke", "sClass": "all text-center" },
                        { data:"Info", "sClass": "tablet-p mobile-l mobile-p text-center dropDownIconTd extraDetail" },
                        { data:"Email", "sClass": "desktop tablet-l" },
                        { data:"Company", "sClass": "desktop tablet-l" },
                        { data:"Phone", "sClass": "desktop tablet-l" }
                    ],
                    "order": [[0, 'asc']]
                };

                var userListTable = dataTableModule("userAccessTable", tableOpts, tableData);
                this.userRevokeListTable = userListTable.renderDataTable();

                this.initMaterialComponents();
            },
            showTableDropDown: function(event){
                if(!$(".rowExpandTable").is(":visible")){
                    return false;
                }
                var tr = $(event.currentTarget).closest('tr');
                var row = this.userRevokeListTable.row(tr);

                function format ( d ) {
                    // `d` is the original data object for the row
                    return '<div class="row">'+
                        '<div class="col-xs-12">'+
                        '<p class="text-left">'+
                        '<span class="text-bold">Email</span> : ' + d.Email+
                        '<br>' +
                        '<span class="text-bold">Company</span> : ' + d.Company+
                        '<br>' +
                        '<span class="text-bold">Phone</span> : ' + d.Phone+
                        '</p>' +
                        '</div>'+
                        '</div>';
                }

                if ( row.child.isShown() ) {
                    // This row is already open - close it
                    row.child.hide();
                    tr.removeClass('shown');
                    $(event.currentTarget).find('.rowExpandTable').toggleClass('fa-chevron-down fa-chevron-up');
                    $($(event.currentTarget).siblings(".dropDownIconTd")).find('.rowExpandTable').toggleClass('fa-chevron-down fa-chevron-up');
                }
                else {
                    // Open this row
                    row.child( format(row.data()) ).show();
                    tr.addClass('shown');
                    $('head').append('<style>table.dataTable.dtr-inline.collapsed > tbody > tr > td:first-child:before, table.dataTable.dtr-inline.collapsed > tbody > tr > th:first-child:before{content: none !important;}</style>');
                    $(event.currentTarget).find('.rowExpandTable').toggleClass('fa-chevron-down fa-chevron-up');
                    $($(event.currentTarget).siblings(".dropDownIconTd")).find('.rowExpandTable').toggleClass('fa-chevron-down fa-chevron-up');
                }
                //Utils.scrollElementToTop(event);
            },

            removeUser: function(event){
                var model = ModelInit(ModelConfigNames.getRevokeAccessModel);
                var userInfo = StoreManager.getStoreData(storeNames.userInfo);
                var requestorId = $(event.currentTarget).attr("data-userId");
                var selectedDevice = StoreManager.getStoreData(storeNames.currentSelectedDevice);
                model.setModelData({
                    "userId": userInfo.userId,
                    "deviceId": selectedDevice.id,
                    "deviceSubOwnerId": requestorId
                });
                model.sendServerCall();
            },

            //--------------------------------------
            initMaterialComponents: function(){
                $.material.init();
            }
            //    ----------------------------------------------

        };


        return ViewFunctions;

    }();

})();
