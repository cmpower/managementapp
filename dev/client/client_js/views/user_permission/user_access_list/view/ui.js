(function(){
    "use strict";
    // UserAccessListView

    module.exports = function(){
        var ui = {
            "userAccessTable" : "#userAccessTable",
            "refreshSubUserList" : "#refreshSubUserList",
            "initMsgDiv" : "#initMsgDiv",
            "tableDiv" : "#tableDiv"

        };

        return (ui);
    }();

})();