(function(){
    "use strict";
    // UserAccessListView

    module.exports = function(){

        function on_dom_refresh(){
            this.getUserAccessList();
        }

        return (on_dom_refresh);
    }();

})();