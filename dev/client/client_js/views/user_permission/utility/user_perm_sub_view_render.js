'use strict';

// USER_PERMISSION
(function () {
    "use strict";

    var RenderComponent = require('../../../app/components/render_sub_view_component');
    var _ = require('underscore');

    var Sub_View_Render = function Sub_View_Render() {
        //this.( ViewName or RegionName ) = "";
    };

    Sub_View_Render.prototype = {

        /*'setViewNameorRegionName' : function(options){
            this.( ViewName or RegionName ) = options.value;
        },*/

        'renderUserRequestListView': function(data) {
            RenderComponent.renderSubView('permission-tab', 'getUserRequestListView', {});
        },
        'renderUserAccessListView': function(data) {
            RenderComponent.renderSubView('user-tab', 'getUserAccessListView', {});
        },
        'renderUserPermissionInvite': function(data) {
            RenderComponent.renderSubView('invite-user-permission-tab', 'getUserPermissionInviteView', {});
        }

    };

    module.exports = (function () {
        return new Sub_View_Render();
    })();
})();