(function(){
    "use strict";
    // UserRemovalView

    var _ = require('underscore');
    var viewContentLang = require("../../../app/utils/language/multi_lingual_module.js");
    var languageId = "UserRemovalView", templateId = "UserRemovalView";

    var initView = function(){
        this.template = _.template(viewContentLang.getCurrentLangViewTemplate(languageId, templateId));
    };

    module.exports = function(){
        return (initView);
    }();

})();