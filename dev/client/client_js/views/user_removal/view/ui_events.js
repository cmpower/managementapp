(function(){
    "use strict";
    // UserRemovalView

    module.exports = function(){
        var ui_events = {
            "click .removeUser" : "removeUser"

        };

        return (ui_events);
    }();

})();