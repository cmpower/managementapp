(function(){
    "use strict";
    // UserRemovalView

    var StoreManager = require('../../../app/storage/store_manager');
    var storeNames = require('../../../app/storage/store_names');
    var ModelInit = require('../../../app/models/model_initiator');
    var ModelConfigNames = require('../../../app/models/model_config_names');
    var dataTable = require('../../../app/utils/data_table/render_data_table');
    var notification = require('../../../app/utils/notification/notification_module');

    module.exports = function(){

        var ViewFunctions = {

            getUserAccessList: function(){
                var model = ModelInit(ModelConfigNames.getUserAccessListModel);
                var userInfo = StoreManager.getStoreData(storeNames.userInfo);
                var selectedDevice = StoreManager.getStoreData(storeNames.currentSelectedDevice);
                model.setModelData({
                    "deviceId": selectedDevice.id,
                    "userId": userInfo.userId,
                    "permissionStatus": "approved"
                });
                model.sendServerCall();
            },
            renderUserAccessList: function(){
                var userAList = StoreManager.getStoreData(storeNames.userAccessList);
                var i, tableData = [], tableOpts = {};
                var requestor;

                for(i = 0; i < userAList.length; i++){

                    requestor =  userAList[i].deviceRequesterDetails;
                    tableData.push({
                        "0" : requestor.name,
                        "1" : "<button data-requestorId='"+userAList[i].deviceRequesterId+"' data-deviceId='"+userAList[i].deviceId+"' " +
                        "class='removeUser btn btn-raised btn-sm btn-danger' type='button'>Revoke</button>"
                    });
                }

                tableOpts = {
                    "paging":   false,
                    "ordering": false,
                    "info":     false,
                    responsive: {
                        details: false
                    },
                    "aoColumns": [
                        { "sClass": "text-left" },
                        { "sClass": "" }
                    ],
                    destroy:true
                };

                this.requestTable = dataTable("userAccessTable", tableOpts, tableData);
                this.requestTable.renderDataTable();
            },
            removeUser: function(event){
                var model = ModelInit(ModelConfigNames.getRevokeAccessModel);
                var userInfo = StoreManager.getStoreData(storeNames.userInfo);
                var requestorId = $(event.currentTarget).attr("data-requestorId");
                var deviceId = $(event.currentTarget).attr("data-deviceId");
                model.setModelData({
                    "userId": userInfo.userId,
                    "deviceId": deviceId,
                    "deviceSubOwnerId": requestorId
                });
                model.sendServerCall();
            }

        };


        return ViewFunctions;

    }();

})();
