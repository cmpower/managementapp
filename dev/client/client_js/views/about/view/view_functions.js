"use strict";

(function () {
    "use strict"
    // AboutView
    ;
    var $ = require("jquery");

    module.exports = (function () {

        var ViewFunctions = {

            initTimePicker: function initTimePicker() {
                $("#timePickerDiv").timePicker();
            }

        };

        return ViewFunctions;
    })();
})();