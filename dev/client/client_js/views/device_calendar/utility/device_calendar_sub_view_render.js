'use strict';

// DEVICE_CALENDAR
(function () {
    "use strict";

    var RenderComponent = require('../../../app/components/render_sub_view_component');
    var _ = require('underscore');
    //var device_calendar_storage = require("./device_calendar_storage");
    var device_calendar_sub_view_instance = require("./device_calendar_sub_view_instance");

    var Sub_View_Render = function Sub_View_Render() {
        //this.( ViewName or RegionName ) = "";
    };

    Sub_View_Render.prototype = {

        /*'setViewNameorRegionName' : function(options){
            this.( ViewName or RegionName ) = options.value;
        },*/

        'renderCalendarView': function renderCalendarView(data) {
            var view = RenderComponent.renderSubView('DateRangeTab', 'getCalendarView', {});
            device_calendar_sub_view_instance.storeSubView("CalendarView", view);
        },
        'renderWeeklyView': function renderWeeklyView(data) {
            var view = RenderComponent.renderSubView('WeeklyTab', 'getWeeklyView', {});
            device_calendar_sub_view_instance.storeSubView("WeeklyView", view);
        },
        'renderHolidayView': function renderHolidayView(data) {
            var view = RenderComponent.renderSubView('HolidaysTab', 'getHolidayView', {});
            device_calendar_sub_view_instance.storeSubView("HolidayView", view);
        }

    };

    module.exports = (function () {
        return new Sub_View_Render();
    })();
})();