(function(){
    "use strict";
    // CalendarView

    module.exports = function(){
        var ui = {
            "multiDateCalendar" : "#multiDateCalendar",
            "calLegendHeader" : "#calLegendHeader",

            "customDayScheduleForm" : "#customDayScheduleForm",
            "customDateModal" : "#customDateModal",
            "pickedCustomDay" : "#pickedCustomDay",
            "fromCustomTimePicker" : "#fromCustomTimePicker",
            "fromCustomTime" : "#fromCustomTime",
            "toCustomTimePicker" : "#toCustomTimePicker",
            "toCustomTime" : "#toCustomTime",
            "setCustomScheduleButton" : "#setCustomScheduleButton",

            "editCustomDayScheduleForm" : "#editCustomDayScheduleForm",
            "editCustomDateModal" : "#editCustomDateModal",
            "editPickedCustomDay" : "#editPickedCustomDay",
            "editFromCustomTimePicker" : "#editFromCustomTimePicker",
            "editFromCustomTime" : "#editFromCustomTime",
            "editToCustomTimePicker" : "#editToCustomTimePicker",
            "editToCustomTime" : "#editToCustomTime",
            "editCustomScheduleButton" : "#editCustomScheduleButton",
            "deleteCustomScheduleButton" : "#deleteCustomScheduleButton",

            "customTimeErrorMsg" : "#customTimeErrorMsg",
            "editCustomTimeErrorMsg" : "#editCustomTimeErrorMsg"


        };

        return (ui);
    }();

})();