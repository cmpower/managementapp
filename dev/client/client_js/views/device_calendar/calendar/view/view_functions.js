(function(){
    "use strict";
    // CalendarView

    var $ = require("jquery");
    var _ = require("underscore");
    var Moment = require("moment");
    var Utils = require("../../../../app/utils/utils");
    var Calendar_Swipe = require('../../../../app/lib_enhancements/calendar_swipe');
    var StoreManager = require('../../../../app/storage/store_manager');
    var storeNames = require('../../../../app/storage/store_names');
    var ModelInit = require('../../../../app/models/model_initiator');
    var ModelConfigNames = require('../../../../app/models/model_config_names');

    module.exports = function(){

        var ViewFunctions = {


            //--------------------------
            getDeviceSchedule: function(){
                var deviceId = StoreManager.getStoreData(storeNames.chosenDeviceId);
                var model = ModelInit(ModelConfigNames.updateCustomScheduleModel);
                model.setModelData({
                    "deviceId": deviceId
                });
                model.sendServerCall();
            },

            renderMultiDateCalendar: function(){
                var scheduleList = StoreManager.getStoreData(storeNames.scheduleList);
                var custom_dates = _.pluck(scheduleList.customSchedule, "dated");
                var holiday_dates = _.pluck(scheduleList.holidaySchedule.scheduleList, "dated");

                /*var custom_dates    = ["08/07/2016","09/07/2016","10/07/2016","11/07/2016","12/07/2016","19/07/2016","20/07/2016","21/07/2016"];
                 var holiday_dates   = ["08/07/2016","09/07/2016","10/07/2016","11/07/2016","12/07/2016","19/07/2016","20/07/2016","21/07/2016"];*/
                this.ui.multiDateCalendar.datepicker('destroy');
                this.ui.multiDateCalendar.datepicker({
                    multidate: true,
                    todayBtn: true,
                    todayHighlight: true,
                    daysOfWeekHighlighted: "0,1,2,3,4,5,6",
                    beforeShowDay: function(date){
                        var d = date;
                        var curr_date = d.getDate();
                        var curr_month = d.getMonth() + 1; //Months are zero based
                        var curr_year = d.getFullYear();
                        var formattedDate = Moment(curr_month + "/" + curr_date + "/" + curr_year, "M/D/YYYY").format("MM/DD/YYYY");
                        var formattedHolidayDate = Moment(curr_month + "/" + curr_date, "M/D").format("MM/DD");

                        if ($.inArray(formattedDate, custom_dates) != -1){
                            return {
                                classes: 'activeDatesCustom'
                            };
                        }
                        if ($.inArray(formattedHolidayDate, holiday_dates) != -1){
                            return {
                                classes: 'activeDatesHoliday'
                            };
                        }
                    }
                });
                Calendar_Swipe.init("multiDateCalendar");
            },
            //--------------------------

            showCustomSchModal: function(dataObj){
                var selectedDate = Moment(dataObj);

                this.ui.setCustomScheduleButton.attr("data-date", selectedDate.format("MM/DD/YYYY"));
                this.ui.pickedCustomDay.html(selectedDate.format("dddd DD MMMM YYYY"));
                this.ui.fromCustomTimePicker.attr("data-day_text", selectedDate.format("dddd"));
                this.ui.toCustomTimePicker.attr("data-day_text", selectedDate.format("dddd"));
                this.ui.fromCustomTime.val("00:00 AM");
                this.ui.toCustomTime.val("00:00 AM");
                this.initCustomTimePicker();
                this.ui.customDateModal.modal("show");
            },

            showEditCustomSchModal: function(dataObj){
                var selectedDate = Moment(dataObj);

                var scheduleList = _.clone(StoreManager.getStoreData(storeNames.scheduleList));
                var customScheduleList = scheduleList.customSchedule;
                var chosenCustomSchedule = _.findWhere(customScheduleList, {"dated": selectedDate.format("MM/DD/YYYY")});

                this.ui.editCustomScheduleButton.attr("data-date", selectedDate.format("MM/DD/YYYY"));
                this.ui.deleteCustomScheduleButton.attr("data-date", selectedDate.format("MM/DD/YYYY"));
                this.ui.editPickedCustomDay.html(selectedDate.format("dddd DD MMMM YYYY"));
                this.ui.editFromCustomTimePicker.attr("data-day_text", selectedDate.format("dddd"));
                this.ui.editToCustomTimePicker.attr("data-day_text", selectedDate.format("dddd"));
                this.ui.editFromCustomTime.val((Moment(chosenCustomSchedule.fromTime, "HH:mm:ss").format("hh:mm a")).toUpperCase());
                this.ui.editToCustomTime.val((Moment(chosenCustomSchedule.toTime, "HH:mm:ss").format("hh:mm a")).toUpperCase());
                this.initCustomTimePicker();
                this.ui.editCustomDateModal.modal("show");
            },

            initCustomTimePicker: function(){
                this.customTimePickerIds = [
                    "#fromCustomTimePicker", "#toCustomTimePicker", "#editFromCustomTimePicker", "#editToCustomTimePicker"
                ];
                var i;

                for(i = 0; i < this.customTimePickerIds.length; i++){
                    $(this.customTimePickerIds[i]).timePicker({
                        'afterDone': function afterDone() {
                            $("#customTimeErrorMsg").hide();
                            $("#editCustomTimeErrorMsg").hide();
                        }
                    });
                }

                $.material.init();
            },
            clearTimePickerOnModalClose: function(){
                var that = this;
                this.ui.customDateModal.on("hide.bs.modal", function(){
                    that.destroyTimePickers();
                });
                this.ui.editCustomDateModal.on("hide.bs.modal", function(){
                    that.destroyTimePickers();
                });
            },
            destroyTimePickers: function(){
                if(typeof this.customTimePickerIds == "undefined"){
                    return true;
                }
                var i;
                for(i = 0; i < this.customTimePickerIds.length; i++){
                    $($(this.customTimePickerIds[i]).timePicker()).timePicker("destroy");
                }
            },

            setCustomSchedule: function(event){
                var fromMoment = Moment(this.ui.fromCustomTime.val().toLowerCase(), "hh:mm a");
                var toMoment = Moment(this.ui.toCustomTime.val().toLowerCase(), "hh:mm a");

                if(toMoment.diff(fromMoment, "seconds") < 1 ){
                    this.ui.customTimeErrorMsg.show();
                    return false;
                }

                var model = ModelInit(ModelConfigNames.scheduleCalendarModel);
                var deviceId = StoreManager.getStoreData(storeNames.chosenDeviceId);
                var selectedDate = $(event.currentTarget).attr("data-date");

                var scheduleList = _.clone(StoreManager.getStoreData(storeNames.scheduleList));
                var customScheduleList = scheduleList.customSchedule;
                var customSchedule = {
                    "dated": selectedDate,
                    "turnTo": "on",
                    "fromTime": Moment(this.ui.fromCustomTime.val().toLowerCase(), "hh:mm a").format("HH:mm:ss"),
                    "toTime": Moment(this.ui.toCustomTime.val().toLowerCase(), "hh:mm a").format("HH:mm:ss")
                };

                customScheduleList.push(customSchedule);

                model.setModelData({
                    "deviceId": deviceId,
                    "scheduleType": "customSchedule",
                    "customSchedule": customScheduleList

                });
                model.sendServerCall();

            },
            editCustomSchedule: function(event){
                var fromMoment = Moment(this.ui.editFromCustomTime.val().toLowerCase(), "hh:mm a");
                var toMoment = Moment(this.ui.editToCustomTime.val().toLowerCase(), "hh:mm a");

                if(toMoment.diff(fromMoment, "seconds") < 1 ){
                    this.ui.editCustomTimeErrorMsg.show();
                    return false;
                }

                var model = ModelInit(ModelConfigNames.scheduleCalendarModel);
                var deviceId = StoreManager.getStoreData(storeNames.chosenDeviceId);
                var selectedDate = $(event.currentTarget).attr("data-date");

                var scheduleList = _.clone(StoreManager.getStoreData(storeNames.scheduleList));
                var customScheduleList = scheduleList.customSchedule;

                var pos;

                for(var i = 0; i < customScheduleList.length; i++){
                    if(customScheduleList[i].dated ==  selectedDate){
                        pos = i;
                        break;
                    }
                }
                customScheduleList[pos].fromTime = Moment(this.ui.editFromCustomTime.val().toLowerCase(), "hh:mm a").format("HH:mm:ss");
                customScheduleList[pos].toTime = Moment(this.ui.editToCustomTime.val().toLowerCase(), "hh:mm a").format("HH:mm:ss");

                model.setModelData({
                    "deviceId": deviceId,
                    "scheduleType": "customSchedule",
                    "customSchedule": customScheduleList

                });
                model.sendServerCall();

            },
            deleteCustomSchedule: function(event){
                var model = ModelInit(ModelConfigNames.scheduleCalendarModel);
                var deviceId = StoreManager.getStoreData(storeNames.chosenDeviceId);
                var selectedDate = $(event.currentTarget).attr("data-date");

                var scheduleList = _.clone(StoreManager.getStoreData(storeNames.scheduleList));
                var customScheduleList = scheduleList.customSchedule;

                var pos;

                for(var i = 0; i < customScheduleList.length; i++){
                    if(customScheduleList[i].dated ==  selectedDate){
                        pos = i;
                        break;
                    }
                }

                customScheduleList.splice(pos,1);

                model.setModelData({
                    "deviceId": deviceId,
                    "scheduleType": "customSchedule",
                    "customSchedule": customScheduleList

                });
                model.sendServerCall();

            },
            setCustomScheduleComplete: function(){
                $(".modal").modal("hide");
                this.destroyTimePickers();
                this.getDeviceSchedule();
            },

            //  ---------------------------------------------------------

            scrollLegend: function(event){
                Utils.scrollElementToTop(event);
            }


        };


        return ViewFunctions;

    }();

})();
