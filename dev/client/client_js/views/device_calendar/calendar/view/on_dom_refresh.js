(function(){
    "use strict";
    // View

    module.exports = function(){

        function on_dom_refresh(){
            this.renderMultiDateCalendar();
            this.clearTimePickerOnModalClose();
        }

        return (on_dom_refresh);
    }();

})();