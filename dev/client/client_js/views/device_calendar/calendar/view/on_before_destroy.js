(function(){
    "use strict";
    // CalendarView

    module.exports = function(){

        function on_before_destroy(){
            this.destroyTimePickers();
        }

        return (on_before_destroy);
    }();

})();