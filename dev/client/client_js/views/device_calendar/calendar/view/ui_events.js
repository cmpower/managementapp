(function(){
    "use strict";
    // CalendarView

    module.exports = function(){
        var ui_events = {
            "click @ui.setCustomScheduleButton" : "setCustomSchedule",
            "click @ui.editCustomScheduleButton" : "editCustomSchedule",
            "click @ui.deleteCustomScheduleButton" : "deleteCustomSchedule",
            "click @ui.calLegendHeader" : "scrollLegend"

        };

        return (ui_events);
    }();

})();