(function(){
    "use strict";
    // DeviceCalendarView

    module.exports = function(){
        var ui_events = {
            "click .calDate" : "dateSelected"
        };

        return (ui_events);
    }();

})();