(function(){
    "use strict";
    // View

    module.exports = function(){

        function on_dom_refresh(){
            this.populateDeviceName();
            this.renderSubViews();
            this.initTabEvents();
        }

        return (on_dom_refresh);
    }();

})();