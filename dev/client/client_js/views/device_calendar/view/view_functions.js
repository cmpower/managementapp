(function(){
    "use strict";
    // DeviceCalendarView

    var $ = require("jquery");
    var Device_Calendar_Sub_View_Render = require("../utility/device_calendar_sub_view_render");
    var Device_Calendar_Sub_View_Instance = require("../utility/device_calendar_sub_view_instance");
    var StoreManager = require('../../../app/storage/store_manager');
    var storeNames = require('../../../app/storage/store_names');
    module.exports = function(){

        var ViewFunctions = {

            //-----------------------------------
            renderMultiDateCalendar: function(){
                var CalendarView = this.getChildView("DateRangeTab");
                CalendarView.renderMultiDateCalendar();
            },
            renderWeeklySchedule: function(){
                var WeeklyView = this.getChildView("WeeklyTab");
                WeeklyView.renderWeeklySchedule();
            },
            renderHolidaySchedule: function(){
                var HolidayView = this.getChildView("HolidaysTab");
                HolidayView.renderHolidaySchedule();
            },
            //-----------------------------------
            populateDeviceName: function(){
                var dName = StoreManager.getStoreData(storeNames.chosenDeviceName);
                this.ui.deviceName.html(dName.deviceName);
                this.ui.deviceLocationName.html(dName.deviceLocationName);
            },
            renderSubViews: function(){
                Device_Calendar_Sub_View_Render.renderCalendarView();
                Device_Calendar_Sub_View_Render.renderWeeklyView();
                Device_Calendar_Sub_View_Render.renderHolidayView();
            },
            //-----------------------------------
            initTabEvents: function(){
                var that = this;
                $("a[href='#DateRangeTab']").on('shown.bs.tab', function () {
                    var CalendarView = that.getChildView("DateRangeTab");
                    CalendarView.getDeviceSchedule();
                });
            },
            //-----------------------------------

            dateSelected: function(event){
                //console.log($(event.currentTarget).closest("div").parent().parent()[0].id);

                var isEdit;
                var date = parseInt($(event.currentTarget).attr("data-date"));
                var month = parseInt($(event.currentTarget).attr("data-month"));
                var year = parseInt($(event.currentTarget).attr("data-year"));
                var datePickerId = $(event.currentTarget).closest("div").parent().parent()[0].id;

                if(datePickerId == "selectHolidayDateDiv"){
                    $('#'+datePickerId+'').datepicker('setDate', new Date(year, month, date));
                }
                if(datePickerId == "multiDateCalendar"){
                    //$('#multiDateCalendar').datepicker('setDate', new Date(year, month, date));
                    var CalendarView = Device_Calendar_Sub_View_Instance.getSubView("CalendarView");
                    isEdit = $(event.currentTarget).parent().hasClass("activeDatesCustom");
                    if(isEdit){
                        CalendarView.showEditCustomSchModal(new Date(year, month, date));
                    }else{
                        CalendarView.showCustomSchModal(new Date(year, month, date));
                    }


                }

                //console.log($('#'+datePickerId+'').datepicker('getDate'));
            },

            setScheduleComplete: function(){
                var responseData = StoreManager.getStoreData(storeNames.setScheduleResponseData);
                var type = (responseData == null)? "default" : responseData.scheduleType;
                var CalendarView = Device_Calendar_Sub_View_Instance.getSubView("CalendarView");
                var WeeklyView = Device_Calendar_Sub_View_Instance.getSubView("WeeklyView");
                var HolidayView = Device_Calendar_Sub_View_Instance.getSubView("HolidayView");

                switch(type){
                    case 'customSchedule' : CalendarView.setCustomScheduleComplete();
                        break;
                    case 'weeklySchedule' : WeeklyView.setWeeklyScheduleComplete();
                        break;
                    case 'holidaySchedule' : HolidayView.setHolidayScheduleComplete();
                        break;
                    default : CalendarView.setCustomScheduleComplete();
                        break;
                }

            }

            //    --------------------------------------



        };


        return ViewFunctions;

    }();

})();
