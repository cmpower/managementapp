(function(){
    "use strict";
    // DeviceCalendarView

    module.exports = function(){
        var regions = {
            "DateRangeTab"  : "#DateRangeTab",
            "WeeklyTab"     : "#WeeklyTab",
            "HolidaysTab"   : "#HolidaysTab"

        };

        return (regions);
    }();

})();