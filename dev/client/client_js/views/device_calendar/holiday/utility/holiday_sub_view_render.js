'use strict';

// holiday
(function () {
    "use strict";

    var RenderComponent = require('../../../../app/components/render_sub_view_component');
    var _ = require('underscore');
    //var holiday_storage = require("./holiday_storage");
    var holiday_sub_view_instance = require("./holiday_sub_view_instance");

    var Sub_View_Render = function Sub_View_Render() {
        //this.( ViewName or RegionName ) = "";
    };

    Sub_View_Render.prototype = {

        /*'setViewNameorRegionName' : function(options){
            this.( ViewName or RegionName ) = options.value;
        },*/

        'renderAddHolidayView': function renderAddHolidayView(region, data) {
            var view = RenderComponent.renderChildrenSubViews(region, 'getAddHolidayView', data);
            holiday_sub_view_instance.storeSubView("AddHolidayView", view);
        },

        'renderHolidayListView': function renderHolidayListView(region, data) {
            var view = RenderComponent.renderChildrenSubViews(region, 'getHolidayListView', data);
            holiday_sub_view_instance.storeSubView("HolidayListView", view);
        }

    };

    module.exports = (function () {
        return new Sub_View_Render();
    })();
})();