// HOLIDAY
(function(){
    "use strict";

    var _ = require('underscore');

    var sub_views = {
        'AddHolidayView'  : {},
        'HolidayListView'    : {}
    };

    var HOLIDAY_SUB_VIEW = function(){
        this.SUB_VIEWS = _.clone(sub_views);
    };

    HOLIDAY_SUB_VIEW.prototype = {

        getSubView : function(subViewName){
            if(typeof this.SUB_VIEWS[subViewName] != "undefined"){
                return (_.clone(this.SUB_VIEWS[subViewName]));
            }else{
                console.error("Sub View doesn't exists");
                return {};
            }

        },
        storeSubView : function(subViewName, view){
            if(typeof this.SUB_VIEWS[subViewName] != "undefined"){
                this.SUB_VIEWS[subViewName] = view;
                return true;
            }else{
                console.error("Sub View doesn't exists");
                return false;
            }
        },
        clearSubViews : function(){
            this.SUB_VIEWS = _.clone(sub_views);
        }
    };


    //************************
    module.exports = function(){
        return (new HOLIDAY_SUB_VIEW());
    }();
    //************************

})();