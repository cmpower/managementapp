(function(){
    "use strict";
    // HolidayView

    module.exports = function(){
        var ui = {
            "showAddHolidayFormButton" : "#showAddHolidayFormButton",
            "holidaySwitch" : "#holidaySwitch",
            "fromHolTime" : "#fromHolTime",
            "toHolTime" : "#toHolTime",

            "holidayTimeErrorMsg" : "#holidayTimeErrorMsg"

        };

        return (ui);
    }();

})();