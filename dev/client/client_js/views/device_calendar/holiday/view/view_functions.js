"use strict";

(function () {
    "use strict";
    // HolidayView

    var Moment = require("moment");
    var $ = require("jquery");
    var Utils = require('../../../../app/utils/utils');
    var notification = require('../../../../app/utils/notification/notification_module');
    var Holiday_Sub_View_Render = require("../utility/holiday_sub_view_render");
     var StoreManager = require('../../../../app/storage/store_manager');
     var storeNames = require('../../../../app/storage/store_names');
     var ModelInit = require('../../../../app/models/model_initiator');
     var ModelConfigNames = require('../../../../app/models/model_config_names');

    module.exports = (function () {

        var ViewFunctions = {

            //--------------------------
            getUpdatedHolSchedule: function(){
                var deviceId = StoreManager.getStoreData(storeNames.chosenDeviceId);
                var model = ModelInit(ModelConfigNames.updateHolidayScheduleModel);
                model.setModelData({
                    "deviceId": deviceId
                });
                model.sendServerCall();
            },
            renderHolidaySchedule: function(){
                var HolidayListView = this.getChildView("holidayListTableRegion");
                HolidayListView.renderHolidayList();
            },

            //--------------------------
            renderSubViews: function renderSubViews() {
                Holiday_Sub_View_Render.renderAddHolidayView(this.getRegion("addHolidayCollapseRegion"), {});
                Holiday_Sub_View_Render.renderHolidayListView(this.getRegion("holidayListTableRegion"), {});
            },
            //--------------------------

            renderHolTimings:function(){
                var scheduleList = StoreManager.getStoreData(storeNames.scheduleList);
                var scheduleCommand = scheduleList.holidaySchedule.scheduleCommand;

                if(scheduleCommand.turnTo == "on"){
                    this.ui.holidaySwitch.prop("checked", true);
                    $("#holidayCollapse").show();
                }else{
                    this.ui.holidaySwitch.prop("checked", false);
                    $("#holidayCollapse").hide();
                }

                this.ui.fromHolTime.val((Moment(scheduleCommand.fromTime, "HH:mm:ss").format("hh:mm a")).toUpperCase());
                this.ui.toHolTime.val((Moment(scheduleCommand.toTime, "HH:mm:ss").format("hh:mm a")).toUpperCase());

                this.initHolTimePicker();
            },
            initHolTimePicker: function initHolTimePicker() {
                this.holTimePickerIds = ["#fromHolTimePicker", "#toHolTimePicker"];
                var i, that = this;

                for (i = 0; i < this.holTimePickerIds.length; i++) {
                    $(this.holTimePickerIds[i]).timePicker({
                        'afterDone': function afterDone() {
                            $("#holidayTimeErrorMsg").hide();
                            that.changeHolidayTime();
                        }
                    });
                }

                this.initMaterialComponents();
            },
            initMaterialComponents: function() {
                $.material.init();
            },
            holidaySwitchOnOff: function(event) {
                var collapseTarget = $(event.currentTarget).attr("data-target");
                if ($(event.currentTarget).is(":checked")) {
                    $("#" + collapseTarget + "").show();
                    this.ui.fromHolTime.val("08:00 AM");
                    this.ui.toHolTime.val("09:00 PM");
                } else {
                    $("#" + collapseTarget + "").hide();
                    this.ui.fromHolTime.val("00:00 AM");
                    this.ui.toHolTime.val("00:00 AM");
                }
                this.changeHolidayTime();
            },
            destroyTimePickers: function() {
                if (typeof this.holTimePickerIds == "undefined") {
                    return true;
                }

                var i;
                for (i = 0; i < this.holTimePickerIds.length; i++) {
                    $($(this.holTimePickerIds[i]).timePicker()).timePicker("destroy");
                }
            },

            changeHolidayTime : function(event){
                var model = ModelInit(ModelConfigNames.scheduleCalendarModel);
                var scheduleList = StoreManager.getStoreData(storeNames.scheduleList);
                var holSchData = scheduleList.holidaySchedule.scheduleList;
                var deviceId = StoreManager.getStoreData(storeNames.chosenDeviceId);

                var fromTime, toTime, turnTo;
                var holidayTimeIsOn = this.ui.holidaySwitch.is(":checked");

                var fromMoment = Moment(this.ui.fromHolTime.val().toLowerCase(), "hh:mm a");
                var toMoment = Moment(this.ui.toHolTime.val().toLowerCase(), "hh:mm a");

                if(holidayTimeIsOn && toMoment.diff(fromMoment, "seconds") < 1){
                    this.ui.holidayTimeErrorMsg.show();
                    return false;
                }

                if(holidayTimeIsOn){
                    fromTime = Moment(this.ui.fromHolTime.val().toLowerCase(), "hh:mm a").format("HH:mm:ss");
                    toTime = Moment(this.ui.toHolTime.val().toLowerCase(), "hh:mm a").format("HH:mm:ss");
                    turnTo = "on";
                }else{
                    fromTime = "00:00:01";
                    toTime = "23:59:59";
                    turnTo = "off";
                }

                model.setModelData({
                    "deviceId": deviceId,
                    "scheduleType": "holidaySchedule",
                    "holidaySchedule": {
                        "scheduleCommand" : {
                            "turnTo": turnTo,
                            "fromTime": fromTime,
                            "toTime": toTime
                        },
                        "scheduleList" : holSchData
                    }
                });
                model.sendServerCall();
            },

            //==============================================

            setHolidayScheduleComplete: function() {
                Utils.scrollIdToTop("#holidayListTBody");
                this.getUpdatedHolSchedule();
            }
        };

        return ViewFunctions;
    })();
})();