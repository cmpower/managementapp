(function(){
    "use strict";
    // HolidayView

    module.exports = function(){
        var ui_events = {
            "change @ui.holidaySwitch" : "holidaySwitchOnOff"

        };

        return (ui_events);
    }();

})();