(function(){
    "use strict";
    // View

    module.exports = function(){

        function on_dom_refresh(){
            this.renderSubViews();
            this.renderHolTimings();
        }

        return (on_dom_refresh);
    }();

})();