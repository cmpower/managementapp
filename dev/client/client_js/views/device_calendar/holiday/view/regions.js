(function(){
    "use strict";
    // HolidayView

    module.exports = function(){
        var regions = {
            "addHolidayCollapseRegion":"#addHolidayCollapseRegion",
            "holidayListTableRegion":"#holidayListTableRegion"

        };

        return (regions);
    }();

})();