(function(){
    "use strict";
    // HolidayListView

    module.exports = function(){
        var ui = {
            "holidayListTBody" : "#holidayListTBody",
            "holidayListTable" : "#holidayListTable",
            "deleteHoliday" : ".deleteHoliday"

        };

        return (ui);
    }();

})();