(function(){
    "use strict";
    // HolidayListView
    var $ = require("jquery");
    var Moment = require("moment");
    var smalltalk = require("../../../../../lib/smalltalk");
    var StoreManager = require('../../../../../app/storage/store_manager');
    var storeNames = require('../../../../../app/storage/store_names');
    var ModelInit = require('../../../../../app/models/model_initiator');
    var ModelConfigNames = require('../../../../../app/models/model_config_names');

    module.exports = function(){

        var ViewFunctions = {

            renderHolidayList: function(){
                var scheduleList = StoreManager.getStoreData(storeNames.scheduleList);
                var holSchData = scheduleList.holidaySchedule.scheduleList;
                var row = "", mmmd, day, holText;
                this.ui.holidayListTBody.html("");
                for(var i = 0; i < holSchData.length; i++){
                    mmmd = Moment(holSchData[i].dated, "MM/DD").format("MMM D");
                    day = Moment(holSchData[i].dated, "MM/DD").format("ddd");
                    holText = holSchData[i].label;
                    row = "<tr><td>"+mmmd+"</td><td>"+day+"</td><td>"+holText+"</td>" +
                        "<td class='text-center deleteHoliday' data-pos='"+i+"' data-dated='"+holSchData[i].dated+"' data-name='"+holSchData[i].label+"'><i class='fa fa-close f-16'></i></td></tr>";
                    this.ui.holidayListTBody.append(row);
                }
            },

            deleteHoliday : function(event){
                var model = ModelInit(ModelConfigNames.scheduleCalendarModel);
                var scheduleList = StoreManager.getStoreData(storeNames.scheduleList);
                var deviceId = StoreManager.getStoreData(storeNames.chosenDeviceId);

                var scheduleCommand = scheduleList.holidaySchedule.scheduleCommand;
                var holSchData = scheduleList.holidaySchedule.scheduleList;
                var dated = $(event.currentTarget).attr("data-dated");
                var pos = $(event.currentTarget).attr("data-pos");
                var holName = $(event.currentTarget).attr("data-name");

                function deleteHoliday(){
                    holSchData.splice(pos,1);
                    model.setModelData({
                        "deviceId": deviceId,
                        "scheduleType": "holidaySchedule",
                        "holidaySchedule": {
                            "scheduleCommand" : scheduleCommand,
                            "scheduleList" : holSchData
                        }

                    });
                    model.sendServerCall();
                }

                smalltalk.confirm('Klario', "Are you sure you want to delete this holiday : "+holName+"?").then(function() {
                    deleteHoliday();
                }, function() {
                    console.log('no');
                });
            }
        };


        return ViewFunctions;

    }();

})();
