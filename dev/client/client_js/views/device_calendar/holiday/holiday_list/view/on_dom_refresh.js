(function(){
    "use strict";
    // View

    module.exports = function(){

        function on_dom_refresh(){
            this.renderHolidayList();
        }

        return (on_dom_refresh);
    }();

})();