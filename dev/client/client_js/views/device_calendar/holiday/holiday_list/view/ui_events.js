(function(){
    "use strict";
    // HolidayListView

    module.exports = function(){
        var ui_events = {
            "click @ui.deleteHoliday" : "deleteHoliday"

        };

        return (ui_events);
    }();

})();