(function(){
    "use strict";
    // AddHolidayView

    var Moment = require("moment");
    var $ = require("jquery");
    var _ = require("underscore");
    var Calendar_Swipe = require('../../../../../app/lib_enhancements/calendar_swipe');
    var notification = require('../../../../../app/utils/notification/notification_module');
    var StoreManager = require('../../../../../app/storage/store_manager');
    var storeNames = require('../../../../../app/storage/store_names');
    var ModelInit = require('../../../../../app/models/model_initiator');
    var ModelConfigNames = require('../../../../../app/models/model_config_names');
    module.exports = function(){

        var ViewFunctions = {
            initAddHolidayCollapseEvents: function(){
                var that = this;
                this.ui.addHolidayCollapse.on('show.bs.collapse', function () {
                    $('#showAddHolidayFormButton').hide();
                });
                this.ui.addHolidayCollapse.on('hide.bs.collapse', function () {
                    $('#showAddHolidayFormButton').show();
                })
            },
            renderSelectHolidayDate: function(){
                var scheduleList = StoreManager.getStoreData(storeNames.scheduleList);
                var holiday_dates = _.pluck(scheduleList.holidaySchedule.scheduleList, "dated");
                $("#selectHolidayDateDiv").datepicker({
                    todayHighlight: true,
                    beforeShowDay: function(date){
                        var d = date;
                        var curr_date = d.getDate();
                        var curr_month = d.getMonth() + 1; //Months are zero based
                        var formattedHolidayDate = Moment(curr_month + "/" + curr_date, "M/D").format("MM/DD");

                        if ($.inArray(formattedHolidayDate, holiday_dates) != -1){
                            return {
                                classes: 'activeDatesHoliday'
                            };
                        }
                    }
                });
                Calendar_Swipe.init("selectHolidayDateDiv");
            },

            addHoliday : function(event){
                var isValid = this.formValidity.validate();
                if($('#selectHolidayDateDiv').datepicker('getDate') == null){
                    notification.warning("Please select a date.");
                    event.preventDefault();
                    return false;
                }
                if(!isValid){
                    event.preventDefault();
                    return false
                }

                var holDate = new Date($('#selectHolidayDateDiv').datepicker('getDate'));
                var momentInput = ""+holDate.getDate()+"/"+(parseInt(holDate.getMonth())+1)+"/"+holDate.getFullYear()+"";
                var selectedDate = Moment(momentInput, "D/M/YYYY");
                var holidayName = this.ui.holidayDesc.val();

                var model = ModelInit(ModelConfigNames.scheduleCalendarModel);
                var scheduleList = StoreManager.getStoreData(storeNames.scheduleList);
                var holScheduleCmd = scheduleList.holidaySchedule.scheduleCommand;
                var holSchList = scheduleList.holidaySchedule.scheduleList;
                var deviceId = StoreManager.getStoreData(storeNames.chosenDeviceId);

                //=========================================================

                if(typeof _.find(holSchList, { "dated" : selectedDate.format("MM/DD")} ) != "undefined"){
                    notification.warning("Holiday already exists.");
                    return false;
                }

                //=========================================================
                holSchList.push({
                    "dated" : selectedDate.format("MM/DD"),
                    "label" : holidayName
                });

                model.setModelData({
                    "deviceId": deviceId,
                    "scheduleType": "holidaySchedule",
                    "holidaySchedule": {
                        "scheduleCommand" : holScheduleCmd,
                        "scheduleList" : holSchList
                    }
                });
                model.sendServerCall();
                //=========================================================

                this.ui.selectHolidayDate.val("");
                this.ui.holidayDesc.val("");

            }

        };


        return ViewFunctions;

    }();

})();
