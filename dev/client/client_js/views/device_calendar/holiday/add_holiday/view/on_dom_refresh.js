"use strict";

(function () {
    "use strict"
    // View
    ;
    var FormValidation = require('../../../../../app/utils/validation/form_validation');
    module.exports = (function () {

        function on_dom_refresh() {
            this.formValidity = FormValidation("addHolidayForm", "addHoliday");
            this.formValidity.initValidator();
            this.renderSelectHolidayDate();
            this.initAddHolidayCollapseEvents();
        }

        return on_dom_refresh;
    })();
})();