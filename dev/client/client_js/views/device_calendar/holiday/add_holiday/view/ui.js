(function(){
    "use strict";
    // AddHolidayView

    module.exports = function(){
        var ui = {
            "addHolidayForm" : "#addHolidayForm",
            "selectHolidayDate" : "#selectHolidayDate",
            "holidayDesc" : "#holidayDesc",
            "addHolidayCollapse" : "#addHolidayCollapse"
        };

        return (ui);
    }();

})();