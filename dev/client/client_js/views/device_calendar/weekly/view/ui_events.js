(function(){
    "use strict";
    // WeeklyView

    module.exports = function(){
        var ui_events = {
            "change .weeklySwitch" : "weeklySwitchOnOff"

        };

        return (ui_events);
    }();

})();