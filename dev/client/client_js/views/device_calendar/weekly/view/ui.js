(function(){
    "use strict";
    // WeeklyView

    module.exports = function(){
        var ui = {

            "weeklySwitch" : ".weeklySwitch",

            "sunWeeklyButton" : "#sunWeeklyButton",
            "monWeeklyButton" : "#monWeeklyButton",
            "tueWeeklyButton" : "#tueWeeklyButton",
            "wedWeeklyButton" : "#wedWeeklyButton",
            "thuWeeklyButton" : "#thuWeeklyButton",
            "friWeeklyButton" : "#friWeeklyButton",
            "satWeeklyButton" : "#satWeeklyButton",

            "fromSunTime" : "#fromSunTime",
            "toSunTime" : "#toSunTime",

            "fromMonTime" : "#fromMonTime",
            "toMonTime" : "#toMonTime",

            "fromTueTime" : "#fromTueTime",
            "toTueTime" : "#toTueTime",

            "fromWedTime" : "#fromWedTime",
            "toWedTime" : "#toWedTime",

            "fromThuTime" : "#fromThuTime",
            "toThuTime" : "#toThuTime",

            "fromFriTime" : "#fromFriTime",
            "toFriTime" : "#toFriTime",

            "fromSatTime" : "#fromSatTime",
            "toSatTime" : "#toSatTime"

        };

        return (ui);
    }();

})();