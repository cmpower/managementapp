"use strict";

(function () {
    "use strict"
        // WeeklyView

    ;
    var $ = require("jquery");
    var _ = require("underscore");
    var Moment = require("moment");
    var Utils = require('../../../../app/utils/utils');
    var StoreManager = require('../../../../app/storage/store_manager');
    var storeNames = require('../../../../app/storage/store_names');
    var ModelInit = require('../../../../app/models/model_initiator');
    var ModelConfigNames = require('../../../../app/models/model_config_names');

    module.exports = (function () {

        var ViewFunctions = {

            //    --------------------------------------
            initWeeklySchedule: function() {
                var scheduleList = StoreManager.getStoreData(storeNames.scheduleList);
                var weeklyScheduleData = scheduleList.weeklySchedule;

                this.ui.fromSunTime.val((Moment(weeklyScheduleData.sunday.fromTime, "HH:mm:ss").format("hh:mm a")).toUpperCase());
                this.ui.toSunTime.val((Moment(weeklyScheduleData.sunday.toTime, "HH:mm:ss").format("hh:mm a")).toUpperCase());

                this.ui.fromMonTime.val((Moment(weeklyScheduleData.monday.fromTime, "HH:mm:ss").format("hh:mm a")).toUpperCase());
                this.ui.toMonTime.val((Moment(weeklyScheduleData.monday.toTime, "HH:mm:ss").format("hh:mm a")).toUpperCase());

                this.ui.fromTueTime.val((Moment(weeklyScheduleData.tuesday.fromTime, "HH:mm:ss").format("hh:mm a")).toUpperCase());
                this.ui.toTueTime.val((Moment(weeklyScheduleData.tuesday.toTime, "HH:mm:ss").format("hh:mm a")).toUpperCase());

                this.ui.fromWedTime.val((Moment(weeklyScheduleData.wednesday.fromTime, "HH:mm:ss").format("hh:mm a")).toUpperCase());
                this.ui.toWedTime.val((Moment(weeklyScheduleData.wednesday.toTime, "HH:mm:ss").format("hh:mm a")).toUpperCase());

                this.ui.fromThuTime.val((Moment(weeklyScheduleData.thursday.fromTime, "HH:mm:ss").format("hh:mm a")).toUpperCase());
                this.ui.toThuTime.val((Moment(weeklyScheduleData.thursday.toTime, "HH:mm:ss").format("hh:mm a")).toUpperCase());

                this.ui.fromFriTime.val((Moment(weeklyScheduleData.friday.fromTime, "HH:mm:ss").format("hh:mm a")).toUpperCase());
                this.ui.toFriTime.val((Moment(weeklyScheduleData.friday.toTime, "HH:mm:ss").format("hh:mm a")).toUpperCase());

                this.ui.fromSatTime.val((Moment(weeklyScheduleData.saturday.fromTime, "HH:mm:ss").format("hh:mm a")).toUpperCase());
                this.ui.toSatTime.val((Moment(weeklyScheduleData.saturday.toTime, "HH:mm:ss").format("hh:mm a")).toUpperCase());

                this.initTimePickers();
                this.initSwitch();
            },

            initSwitch: function() {
                var scheduleList = StoreManager.getStoreData(storeNames.scheduleList);
                var weeklyScheduleData = scheduleList.weeklySchedule;

                var sunTurnTo = weeklyScheduleData.sunday.turnTo;
                var monTurnTo = weeklyScheduleData.monday.turnTo;
                var tueTurnTo = weeklyScheduleData.tuesday.turnTo;
                var wedTurnTo = weeklyScheduleData.wednesday.turnTo;
                var thuTurnTo = weeklyScheduleData.thursday.turnTo;
                var friTurnTo = weeklyScheduleData.friday.turnTo;
                var satTurnTo = weeklyScheduleData.saturday.turnTo;

                if (sunTurnTo == "on") {
                    this.switchOnAndDropdown("sunWeeklyButton");
                }
                if (monTurnTo == "on") {
                    this.switchOnAndDropdown("monWeeklyButton");
                }
                if (tueTurnTo == "on") {
                    this.switchOnAndDropdown("tueWeeklyButton");
                }
                if (wedTurnTo == "on") {
                    this.switchOnAndDropdown("wedWeeklyButton");
                }
                if (thuTurnTo == "on") {
                    this.switchOnAndDropdown("thuWeeklyButton");
                }
                if (friTurnTo == "on") {
                    this.switchOnAndDropdown("friWeeklyButton");
                }
                if (satTurnTo == "on") {
                    this.switchOnAndDropdown("satWeeklyButton");
                }
            },
            switchOnAndDropdown: function(target) {
                var collapseTarget = this.ui[target].attr("data-target");
                this.ui[target].prop("checked", true);
                $("#" + collapseTarget + "").show();
            },

            initTimePickers: function() {
                var that = this,
                    i;

                this.weeklyTimePickerIds = ["#fromSunTimePicker", "#toSunTimePicker", "#fromMonTimePicker", "#toMonTimePicker", "#fromTueTimePicker", "#toTueTimePicker", "#fromWedTimePicker", "#toWedTimePicker", "#fromThuTimePicker", "#toThuTimePicker", "#fromFriTimePicker", "#toFriTimePicker", "#fromSatTimePicker", "#toSatTimePicker"];

                for (i = 0; i < this.weeklyTimePickerIds.length; i++) {
                    $(this.weeklyTimePickerIds[i]).timePicker({
                        'afterDone': function afterDone() {
                            $(".timeErrorMsg").hide();
                            that.setWeeklySchedule();
                        }
                    });
                }

                this.initMaterialComponents();
            },
            destroyTimePickers: function() {
                if (typeof this.weeklyTimePickerIds == "undefined") {
                    return true;
                }

                var i;
                for (i = 0; i < this.weeklyTimePickerIds.length; i++) {
                    $($(this.weeklyTimePickerIds[i]).timePicker()).timePicker("destroy");
                }
            },
            initMaterialComponents: function() {
                $.material.init();
            },

            weeklySwitchOnOff: function(event) {
                var collapseTarget = $(event.currentTarget).attr("data-target");
                var day = $(event.currentTarget).attr("data-day");
                if ($(event.currentTarget).is(":checked")) {
                    $("#" + collapseTarget + "").show();
                    $("#from" + day + "Time").val("08:00 AM");
                    $("#to" + day + "Time").val("09:00 PM");
                } else {
                    $("#" + collapseTarget + "").hide();
                    $("#from" + day + "Time").val("00:00:01");
                    $("#to" + day + "Time").val("23:59:59");
                }

                this.setWeeklySchedule();

                Utils.scrollElementToTop(event);
            },

            //    --------------------------------------
            //    --------------------------------------
            setWeeklySchedule: function() {
                var model = ModelInit(ModelConfigNames.scheduleCalendarModel);
                var deviceId = StoreManager.getStoreData(storeNames.chosenDeviceId);

                //---------------------------------------------------------------------------
                //---------------------------------------------------------------------------
                var sundayIsOn = this.ui.sunWeeklyButton.prop("checked");
                var sunFromMoment = Moment(this.ui.fromSunTime.val().toLowerCase(), "hh:mm a");
                var sunToMoment = Moment(this.ui.toSunTime.val().toLowerCase(), "hh:mm a");
                //---------------------------------------------------------------------------
                var mondayIsOn = this.ui.monWeeklyButton.prop("checked");
                var monFromMoment = Moment(this.ui.fromMonTime.val().toLowerCase(), "hh:mm a");
                var monToMoment = Moment(this.ui.toMonTime.val().toLowerCase(), "hh:mm a");
                //---------------------------------------------------------------------------
                var tuesdayIsOn = this.ui.tueWeeklyButton.prop("checked");
                var tueFromMoment = Moment(this.ui.fromTueTime.val().toLowerCase(), "hh:mm a");
                var tueToMoment = Moment(this.ui.toTueTime.val().toLowerCase(), "hh:mm a");
                //---------------------------------------------------------------------------
                var wednesdayIsOn = this.ui.wedWeeklyButton.prop("checked");
                var wedFromMoment = Moment(this.ui.fromWedTime.val().toLowerCase(), "hh:mm a");
                var wedToMoment = Moment(this.ui.toWedTime.val().toLowerCase(), "hh:mm a");
                //---------------------------------------------------------------------------
                var thursdayIsOn = this.ui.thuWeeklyButton.prop("checked");
                var thuFromMoment = Moment(this.ui.fromThuTime.val().toLowerCase(), "hh:mm a");
                var thuToMoment = Moment(this.ui.toThuTime.val().toLowerCase(), "hh:mm a");
                //---------------------------------------------------------------------------
                var fridayIsOn = this.ui.friWeeklyButton.prop("checked");
                var friFromMoment = Moment(this.ui.fromFriTime.val().toLowerCase(), "hh:mm a");
                var friToMoment = Moment(this.ui.toFriTime.val().toLowerCase(), "hh:mm a");
                //---------------------------------------------------------------------------
                var saturdayIsOn = this.ui.satWeeklyButton.prop("checked");
                var satFromMoment = Moment(this.ui.fromSatTime.val().toLowerCase(), "hh:mm a");
                var satToMoment = Moment(this.ui.toSatTime.val().toLowerCase(), "hh:mm a");

                //---------------------------------------------------------------------------
                //---------------------------------------------------------------------------

                if(sundayIsOn && sunToMoment.diff(sunFromMoment, "seconds") < 1){
                    $("#sunTimeErrorMsg").show();
                    Utils.scrollElementToTopWithId("#sunTimeErrorMsg");
                    return false;
                }
                //---------------------------------------------------------------------------
                if(mondayIsOn && monToMoment.diff(monFromMoment, "seconds") < 1){
                    $("#monTimeErrorMsg").show();
                    Utils.scrollElementToTopWithId("#monTimeErrorMsg");
                    return false;
                }
                //---------------------------------------------------------------------------
                if(tuesdayIsOn && tueToMoment.diff(tueFromMoment, "seconds") < 1){
                    $("#tueTimeErrorMsg").show();
                    Utils.scrollElementToTopWithId("#tueTimeErrorMsg");
                    return false;
                }
                //---------------------------------------------------------------------------
                if(wednesdayIsOn && wedToMoment.diff(wedFromMoment, "seconds") < 1){
                    $("#wedTimeErrorMsg").show();
                    Utils.scrollElementToTopWithId("#wedTimeErrorMsg");
                    return false;
                }
                //---------------------------------------------------------------------------
                if(thursdayIsOn && thuToMoment.diff(thuFromMoment, "seconds") < 1){
                    $("#thuTimeErrorMsg").show();
                    Utils.scrollElementToTopWithId("#thuTimeErrorMsg");
                    return false;
                }
                //---------------------------------------------------------------------------
                if(fridayIsOn && friToMoment.diff(friFromMoment, "seconds") < 1){
                    $("#friTimeErrorMsg").show();
                    Utils.scrollElementToTopWithId("#friTimeErrorMsg");
                    return false;
                }
                //---------------------------------------------------------------------------
                if(saturdayIsOn && satToMoment.diff(satFromMoment, "seconds") < 1){
                    $("#satTimeErrorMsg").show();
                    Utils.scrollElementToTopWithId("#satTimeErrorMsg");
                    return false;
                }

                //===========================================================================

                model.setModelData({
                    "deviceId": deviceId,
                    "scheduleType": "weeklySchedule",
                    "weeklySchedule": {
                        "sunday": {
                            "turnTo": sundayIsOn? "on" : "off",
                            "fromTime": sundayIsOn? sunFromMoment.format("HH:mm:ss") : this.ui.fromSunTime.val(),
                            "toTime": sundayIsOn? sunToMoment.format("HH:mm:ss") : this.ui.toSunTime.val()
                        },
                        "monday": {
                            "turnTo": mondayIsOn? "on" : "off",
                            "fromTime": mondayIsOn? monFromMoment.format("HH:mm:ss") : this.ui.fromMonTime.val(),
                            "toTime": mondayIsOn? monToMoment.format("HH:mm:ss") : this.ui.toMonTime.val()
                        },
                        "tuesday": {
                            "turnTo": tuesdayIsOn? "on" : "off",
                            "fromTime": tuesdayIsOn? tueFromMoment.format("HH:mm:ss") : this.ui.fromTueTime.val(),
                            "toTime": tuesdayIsOn? tueToMoment.format("HH:mm:ss") : this.ui.toTueTime.val()
                        },
                        "wednesday": {
                            "turnTo": wednesdayIsOn? "on" : "off",
                            "fromTime": wednesdayIsOn? wedFromMoment.format("HH:mm:ss") : this.ui.fromWedTime.val(),
                            "toTime": wednesdayIsOn? wedToMoment.format("HH:mm:ss") : this.ui.toWedTime.val()
                        },
                        "thursday": {
                            "turnTo": thursdayIsOn? "on" : "off",
                            "fromTime": thursdayIsOn? thuFromMoment.format("HH:mm:ss") : this.ui.fromThuTime.val(),
                            "toTime": thursdayIsOn? thuToMoment.format("HH:mm:ss") : this.ui.toThuTime.val()
                        },
                        "friday": {
                            "turnTo": fridayIsOn? "on" : "off",
                            "fromTime": fridayIsOn? friFromMoment.format("HH:mm:ss") : this.ui.fromFriTime.val(),
                            "toTime": fridayIsOn? friToMoment.format("HH:mm:ss") : this.ui.toFriTime.val()
                        },
                        "saturday": {
                            "turnTo": saturdayIsOn? "on" : "off",
                            "fromTime": saturdayIsOn? satFromMoment.format("HH:mm:ss") : this.ui.fromSatTime.val(),
                            "toTime": saturdayIsOn? satToMoment.format("HH:mm:ss") : this.ui.toSatTime.val()
                        }
                    }
                });
                model.sendServerCall();
            },
            setWeeklyScheduleComplete: function() {}
            //    --------------------------------------
            //    --------------------------------------

        };

        return ViewFunctions;
    })();
})();