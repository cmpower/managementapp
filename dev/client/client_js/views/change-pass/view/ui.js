(function(){
    "use strict";
    // ChangePassView

    module.exports = function(){
        var ui = {
            "changePasswordForm"    : "#changePasswordForm",
            "password"              : "#password"

        };

        return (ui);
    }();

})();