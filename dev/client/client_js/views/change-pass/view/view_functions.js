(function(){
    "use strict";
    // ChangePassView
    var $ = require('jquery');
    var ModelInit = require('../../../app/models/model_initiator');
    var ModelConfigNames = require('../../../app/models/model_config_names');
    var StoreManager = require('../../../app/storage/store_manager');
    var storeNames = require('../../../app/storage/store_names');
    module.exports = function(){

        var ViewFunctions = {

            hideHeaderFooter:function(){
                $("#HeaderRegion").hide();
                $("#SidebarRegion").hide();
                $("#FooterRegion").hide();
            },

            showHeaderFooter:function(){
                $("#HeaderRegion").show();
                $("#SidebarRegion").show();
                $("#FooterRegion").show();
            },

            changePassword: function(event){
                var isValid = this.formValidity.validate();
                var userInfo = StoreManager.getStoreData(storeNames.userInfo);
                var model = ModelInit(ModelConfigNames.changePasswordModel);

                if(!isValid){
                    event.preventDefault();
                    return false
                }

                model.setModelData({
                    "userId"    : userInfo.userId,
                    "password"  : this.ui.password.val()
                });
                model.sendServerCall();
            }

        };


        return ViewFunctions;

    }();

})();
