(function(){
    "use strict";
    // ChangePassView

    module.exports = function(){
        var ui_events = {
            "submit @ui.changePasswordForm" : "changePassword"

        };

        return (ui_events);
    }();

})();