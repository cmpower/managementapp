(function(){
    "use strict";
    // ChangePassView
    var FormValidation = require('../../../app/utils/validation/form_validation');
    module.exports = function(){

        function on_dom_refresh(){
            this.formValidity = FormValidation("changePasswordForm", "changePassword");
            this.formValidity.initValidator();
            this.hideHeaderFooter();
        }

        return (on_dom_refresh);
    }();

})();