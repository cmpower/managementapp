(function(){
    "use strict";
    // ChangePassView

    module.exports = function(){

        function on_before_destroy(){
            this.showHeaderFooter();
        }

        return (on_before_destroy);
    }();

})();