(function(){
    "use strict";
    // View

    module.exports = function(){

        function on_dom_refresh(){
            this.init();
        }

        return (on_dom_refresh);
    }();

})();