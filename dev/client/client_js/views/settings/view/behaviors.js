"use strict";

(function () {
    "use strict"
    // LoginView

    ;
    module.exports = (function () {
        var behaviors = {
            RippleBehaviour: {
                message: "Ripple Effect"
            },
            MaterializeBehaviour: {
                message: "Materialize HTML Elements"
            }
        };

        return behaviors;
    })();
})();