(function(){
    "use strict";
    // SettingsView

    module.exports = function(){
        var ui_events = {
            "change @ui.notificationSettings":"notificationSettingsFunc",
            "change @ui.domainRadioButton" : "domainRadioButtonFunc"

        };

        return (ui_events);
    }();

})();