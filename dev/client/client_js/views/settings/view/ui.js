(function(){
    "use strict";
    // SettingsView

    module.exports = function(){
        var ui = {
            "notificationSettings" : "#notificationSettings",
            "domainRadioButton" : ".domainRadioButton"

        };

        return (ui);
    }();

})();