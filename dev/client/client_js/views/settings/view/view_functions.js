'use strict';

(function () {
    "use strict"
        // SettingsView

        /*var $ = require("../../../lib/jquery");
         var ModelInit = require('../../../app/models/model_initiator');
         var ModelConfigNames = require('../../../app/models/model_config_names');
         var StoreManager = require('../../../app/storage/store_manager');
         var storeNames = require('../../../app/storage/store_names');*/
        //require('../../../app/behaviors/behaviors');
    ;
    var $ = require("jquery");
    var Push_Notification = require('../../../app/push/push_notification');

    module.exports = (function () {

        var ViewFunctions = {

            init: function init() {
                var Local_Storage = require('../../../app/local_storage/local_storage');
                var Local_Storage_Names = require('../../../app/local_storage/local_storage_list');

                ((Local_Storage.retrieve(Local_Storage_Names.targetUrl)) == "https://www.klario.net") ? $("#klarioNET").prop("checked",true) :  $("#klarioIN").prop("checked",true) ;

                var val = (Local_Storage.retrieve(Local_Storage_Names.isSubscribed) == "true");
                $("#notificationSettings").prop("checked", val);
            },

            notificationSettingsFunc: function notificationSettingsFunc(event) {
                var Local_Storage = require('../../../app/local_storage/local_storage');
                var Local_Storage_Names = require('../../../app/local_storage/local_storage_list');
                Local_Storage.store(Local_Storage_Names.appNotification, event.currentTarget.checked.toString());

                if (event.currentTarget.checked)
                    Push_Notification.registerPush();
                else
                    Push_Notification.unRegisterPush();
            },

            domainRadioButtonFunc: function domainRadioButtonFunc(event) {

                var Local_Storage = require('../../../app/local_storage/local_storage');
                var Local_Storage_Names = require('../../../app/local_storage/local_storage_list');
                var Target_Url = require('../../../app/api/target_url');

                var isSubscribed  = JSON.parse(Local_Storage.retrieve(Local_Storage_Names.isSubscribed));
                if(isSubscribed){
                    Push_Notification.unRegisterPush();
                    (event.currentTarget.id == "klarioIN") ? Local_Storage.store(Local_Storage_Names.targetUrl, Target_Url.klario_in) : Local_Storage.store(Local_Storage_Names.targetUrl, Target_Url.klario_net);
                    Push_Notification.registerPush();
                }else {
                    (event.currentTarget.id == "klarioIN") ? Local_Storage.store(Local_Storage_Names.targetUrl, Target_Url.klario_in) : Local_Storage.store(Local_Storage_Names.targetUrl, Target_Url.klario_net);
                }
            }

        };

        return ViewFunctions;
    })();
})();