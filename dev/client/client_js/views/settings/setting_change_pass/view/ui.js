(function(){
    "use strict";
    // ChangePassView

    module.exports = function(){
        var ui = {
            "settingChangePasswordForm"    : "#settingChangePasswordForm",
            "password"              : "#password"

        };

        return (ui);
    }();

})();