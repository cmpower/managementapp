(function(){
    "use strict";
    // LoginView

    module.exports = function(){
        var behaviors = {
            RippleBehaviour: {
                    message: "Ripple Effect"
                },
            MaterializeBehaviour: {
                    message: "Material Effect"
                }
            };

        return (behaviors);
    }();

})();