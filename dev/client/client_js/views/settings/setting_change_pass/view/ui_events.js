(function(){
    "use strict";
    // ChangePassView

    module.exports = function(){
        var ui_events = {
            "submit @ui.settingChangePasswordForm" : "settingChangePassword",
            "click #closeSettingChangePass" : "closeSettingChangePass"

        };

        return (ui_events);
    }();

})();