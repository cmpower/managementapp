(function(){
    "use strict";
    // SettingChangePassView
    var FormValidation = require('../../../../app/utils/validation/form_validation');
    module.exports = function(){

        function on_dom_refresh(){
            this.formValidity = FormValidation("settingChangePasswordForm", "changePass");
            this.formValidity.initValidator();
        }

        return (on_dom_refresh);
    }();

})();